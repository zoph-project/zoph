<?php
/**
 * Template for 'quicklook', a hover over the map that shows info
 * about a photo or a place.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */

if (!defined("ZOPH")) { die("Illegal call"); }
?>

<div class="quicklook">
    <h2>
        <?php if (isset($tpl_link)): ?>
            <a href="<?= $tpl_link ?>">
                <?= $tpl_title ?: $tpl_file ?>
            </a>
        <?php else: ?>
            <?= $tpl_title ?: $tpl_file ?>
        <?php endif ?>
    </h2>
    <?= $tpl_title ? "" : "<p>" . $tpl_file . "</p>" ?>
    <?= $tpl_thumb ?? "" ?>
    <small>
        <?= $tpl_small ?>
        <?php if (isset($tpl_photographer)): ?>
            <?= translate("by", 0) . $tpl_photographer ?>
        <?php endif ?>
        <?= $tpl_count ?? "" ?>
    </small>
</div>



