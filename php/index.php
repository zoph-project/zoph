<?php
require_once "include.inc.php";

use web\request;
use zoph\app;

$app = new app(request::create());
$app->run();
$app->view?->display();

?>
