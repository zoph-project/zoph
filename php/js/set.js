var set=function() {
    var dragged = null;
    const urlParams=new URLSearchParams(window.location.search);
    const setId = urlParams.get("set_id");
    const action = urlParams.get("_action");

    function init(e=false) {
        if (!setId || action == "download" || action == "edit") {
            return;
        }
        const thumbs = document.getElementsByClassName("thumb");
        const cols = document.getElementById("cols").value;
        let counter = 0;
        for (let thumb of thumbs) {
            let thumbnail = thumb.parentNode;
            if (e) {
                let a = thumb.parentNode;
                thumbnail = a.parentNode;
                thumbnail.removeChild(a);
            }
            let photoId = thumb.dataset["photoId"];

            counter++;

            var nextId = 0;
            if (thumbs[counter]) {
                nextId = thumbs[counter].dataset["photoId"];
            }

            thumbnail.className="dragdrop";
            thumbnail.dataset["photoId"] = photoId;
            thumbnail.insertBefore(makeDropzone(photoId), null);
            thumbnail.insertBefore(thumb, null);
            thumbnail.insertBefore(makeDropzone(nextId), null);

            thumb.addEventListener("dragstart", set.dragStart)
            thumb.addEventListener("dragend", set.dragEnd)

            document.getElementById("sortorder").style.display = "none";
            document.getElementById("updown").style.display = "none";
            document.body.style.minWidth=(cols * 170) + 50 + "px";
        }
    }

    function makeDropzone(photoId, insert) {
        dropzone = document.createElement("div");
        dropzone.className="dropzone";
        dropzone.dataset["photoId"] = photoId;
        dropzone.addEventListener("dragenter", set.dragEnter);
        dropzone.addEventListener("drop", set.handleDrop, false);
        dropzone.addEventListener("dragover", set.dragOver);
        dropzone.addEventListener("dragleave", set.dragLeave);
        return dropzone;
    }
    
    function dragStart(e) {
        e.dataTransfer.dropEffect = "move";
        e.dataTransfer.setDragImage(e.target.cloneNode(), 0,0);
        e.target.style.boxShadow="none";
        e.target.style.opacity="0.2";
        dragged = e.target;
    }

    function dragEnd(e) {
        e.target.style.boxShadow="";
        e.target.style.opacity="1";
    }

    function dragEnter(e) {
        e.preventDefault();
        e.target.classList.add("dropzone-active");
        e.target.parentNode.style.gridColumn="span 2";
    }

    function dragLeave(e) {
        e.target.classList.remove("dropzone-active");
        e.target.parentNode.style.gridColumn="";
    }

    function dragOver(e) {
        e.preventDefault();
    }

    function handleDrop(e) {
        e.preventDefault();
        dragLeave(e);

        let targetId = e.target.dataset["photoId"];
        let photoId = dragged.dataset["photoId"];

        dragged=null;

        movePhoto(setId, photoId, targetId);
    }

    function movePhoto(setId, photoId, targetId) {
        const url = basePath + "set/move?_targetId=" + targetId + "&set_id=" + setId + "&_photoId=" +photoId;
        var http=new XMLHttpRequest();
        http.responseType = 'json';

        http.open("GET", url, true);
        http.send();
        http.onreadystatechange=function() {
            if (http.readyState == XMLHttpRequest.DONE) {
                updateThumbs(http);
            }
        }
    }

    function updateThumbs(http) {
        let order = http.response;
        let thumbs = document.getElementsByClassName("thumbnails")[0];
        let photos = Array();
        for (let photoId of order) {
            photos.push(thumbs.querySelector('div.dragdrop[data-photo-id="' + photoId + '"]'));
        }

        // The photo-id on the dropzones is no longer correct after reshuffle
        // so we delete them and recreate
        dropzones = Array.from(document.getElementsByClassName("dropzone"));
        for (let dz of dropzones) {
            dz.remove();
        }
        
        for (let photo of photos) {
            thumbs.appendChild(photo, null);
        }
        set.init();
    }

    return {
        init:init,
        dragStart:dragStart,
        dragEnd:dragEnd,
        dragEnter:dragEnter,
        dragLeave:dragLeave,
        dragOver:dragOver,
        handleDrop:handleDrop
    };
}();

window.addEventListener("load",set.init,false);

