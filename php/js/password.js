// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var zPass=function() {
    var pwdId;
    var confirmId;
    var errorId;

    function init(pwd, confirm,error) {
        pwdId = pwd;
        confirmId = confirm;
        errorId = error;

        var inputPwd=document.getElementById(pwdId);
        var inputConfirm=document.getElementById(confirmId);
        var errorDiv=document.getElementById(errorId);
        var submit = inputPwd.closest('form').querySelector('input[type="submit"]');

        inputPwd.addEventListener("input", checkMatch);
        inputConfirm.addEventListener("input", checkMatch);
        errorDiv.style.visibility = "hidden";
        submit.disabled = true;

    }

    function checkMatch() {
        var inputPwd=document.getElementById(pwdId);
        var inputConfirm=document.getElementById(confirmId);
        var errorDiv=document.getElementById(errorId);
        var submit = inputPwd.closest('form').querySelector('input[type="submit"]');

        if (inputPwd.value != inputConfirm.value) {
            inputPwd.classList.add("error");
            errorDiv.style.visibility = "visible";
            submit.disabled = true;
        } else {
            inputPwd.classList.remove("error");
            errorDiv.style.visibility = "hidden";
            submit.disabled = false;
        }
    }
    
    return {
        init:init,
        checkMatch:checkMatch
    };
}();
