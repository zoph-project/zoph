<?php
/**
 * View to display albums
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace album\view;

use web\view\viewInterface;
use album;
use app;
use conf\conf;
use organiser\viewTrait as organiserView;
use pageException;
use selection;
use template\block;
use template\actionlink;
use user;
use web\request;

use photoNoSelectionException;

/**
 * Display screen for albumss
 */
class display extends view implements viewInterface {
    use organiserView;

    private const VIEWNAME = "Album View";
    private const PHOTOLINK = "photos?album_id=";
    private const DESCRIPTION = "album_description";

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user = user::getCurrent();
        $id = (int) $this->object->getId();
        $param = array("album_id" => $id);
        $parentParam = array("parent_album_id" => $id);
        $actionlinks=array();

        if ($user->canEditOrganisers()) {
            $actionlinks=array(
                new actionlink("edit", "album/edit", $param),
                new actionlink("new", "album/new", $parentParam),
                new actionlink("delete", "album/delete", $param)
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks[]=new actionlink("unset_coverphoto", "album/unsetcoverphoto", $param);
            }
        }
        return $actionlinks;
    }

    private function getSelection() : ?selection {
        $albumId = array(
            "album_id" => $this->object->getId()
        );

        try {
            $selection=new selection(
                array(
                    new actionlink("coverphoto", "album/coverphoto", $albumId)
                ),
                "album?_qs=album_id=" . $this->object->getId(),
                field: "coverphoto"
            );
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }

        return $selection;
    }

    public function getTitle() : string {
        return $this->object->get("parent_album_id") ? $this->object->get("album") : translate("Albums");
    }
}
