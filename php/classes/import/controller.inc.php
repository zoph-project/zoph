<?php
/**
 * Controller for import
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace import;

use conf\conf;
use generic\controller as genericController;
use user;
use web\request;

/**
 * Controller for web import
 */
class controller extends genericController {
    protected static $viewDisplay   = view\display::class;
    protected static $viewRedirect  = \web\view\redirect::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "process", "retry", "delete", "import", "upload"
    );


    /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        $user = user::getCurrent();
        if ((!conf::get("import.enable")) || (!$user->isAdmin() && !$user->get("import"))) {
            $this->view = new static::$viewRedirect($this->request, $this->object);
            $this->view->setRedirect("zoph/welcome");
        } else {
            parent::doAction($action);
        }
    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'upload'
     */
    public function actionUpload() {
        $files = $this->request->getFiles("file");
        if (conf::get("import.upload") && $files) {
            web::processUpload($files);
        }

    }


    /**
     * Do action 'process'
     */
    public function actionProcess() {
        web::processFile($this->request["file"]);
    }

    /**
     * Do action 'retry'
     */
    public function actionRetry() {
        web::retryFile($this->request["file"]);
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        web::deleteFile($this->request["file"]);
    }

    /**
     * Do action 'import'
     */
    public function actionImport() {
        $files=web::getFileList($this->request["_import_image"]);
        $photos=web::photos($files, $this->request->getRequestVars());
        foreach ($photos as $photo) {
            echo "<b>" . $photo->getName() . "</b> was imported.<br>";
        }
    }

}
