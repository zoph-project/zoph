<?php
/**
 * Common parts for category view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use web\view\viewInterface;
use template\template;
use web\request;
use web\view\view as webView;

use user;
use category;

/**
 * This is a base view for the category page, that contains common parts for all views
 */
abstract class view extends webView implements viewInterface {

    /** * @var array request variables */
    protected $vars;

    /**
     * Create view
     * @param request web request
     * @param category the category that this page is dealing with
     */
    public function __construct(protected request $request, protected category $object) {
        $this->vars=$request->getRequestVars();
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate($this->request->getAction() . " category");
    }

}
