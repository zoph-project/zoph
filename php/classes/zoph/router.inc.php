<?php
/**
 * Router
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph;


use generic\controller;
use router\abstractRouter;
use router\routerInterface;

use controllerException;

/**
 * Router
 */
class router extends abstractRouter implements routerInterface {

    /**
     * This takes the 'controller' part of the url and finds the matching controller class
     */
    protected function getControllerClass(string $controller) : string {
        return match($controller) {
            "admin"         => \admin\controller::class,
            "album"         => \album\controller::class,
            "backup"        => \backup\controller::class,
            "category"      => \category\controller::class,
            "calendar"      => \calendar\controller::class,
            "circle"        => \circle\controller::class,
            "colourscheme"  => \template\colorScheme\controller::class,
            "comment"       => \comment\controller::class,
            "config"        => \conf\controller::class,
            "css"           => \template\controller::class,
            "group"         => \group\controller::class,
            "image"         => \image\controller::class,
            "import"        => \import\controller::class,
            "install"       => \install\controller::class,
            "mail"          => \mail\controller::class,
            "page"          => \page\controller::class,
            "pageset"       => \pageset\controller::class,
            "person"        => \person\controller::class,
            "photo"         => \photo\controller::class,
            "photos"        => \photos\controller::class,
            "place"         => \place\controller::class,
            "permissions"   => \permissions\controller::class,
            "relation"      => \photo\relation\controller::class,
            "search"        => \search\controller::class,
            "service"       => \web\service\controller::class,
            "set"           => \set\controller::class,
            "track"         => \geo\controller::class,
            "upgrade"       => \upgrade\controller::class,
            "user"          => \user\controller::class,
            "zoph"          => \zoph\controller::class,
            ""              => \zoph\controller::class,
            default => throw new controllerException("Unknown controller: " . $controller)
        };
    }
}
