<?php
/**
 * View to show the reports page of Zoph
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph\view;

use web\view\viewInterface;
use album;
use category;
use person;
use place;
use rating;
use template\block;
use web\view\view;

/**
 * Display info screen
 */
class reports extends view implements viewInterface {

    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title" => $this->getTitle()
        ));

        $topAlbums = album::getTopN();
        if ($topAlbums) {
            $blockAlbums=new block("report", array(
                "title"     => translate("Most Populated Albums"),
                "lines"     => $topAlbums
            ));
            $tpl->addBlock($blockAlbums);
        }

        $topCategories = category::getTopN();
        if ($topCategories) {
            $blockCategories=new block("report", array(
                "title"     => translate("Most Populated Categories"),
                "lines"     => $topCategories
            ));
            $tpl->addBlock($blockCategories);
        }

        $topPeople = person::getTopN();
        if ($topPeople) {
            $blockPeople=new block("report", array(
                "title"     => translate("Most Photographed People"),
                "lines"     => $topPeople
            ));
            $tpl->addBlock($blockPeople);

        }

        $topPlaces = place::getTopN();
        if ($topPlaces) {
            $blockPlaces=new block("report", array(
                "title"     => translate("Most Photographed Places"),
                "lines"     => $topPlaces
            ));
            $tpl->addBlock($blockPlaces);
        }

        $graph=new block("graph_bar", array(
            "title"     => translate("photo ratings", 0),
            "class"     => "ratings",
            "value_label" => translate("rating", 0),
            "count_label" => translate("count", 0),
            "rows"      => rating::getGraphArray()
        ));

        $tpl->addBlock($graph);
        return $tpl;
    }


    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("Reports");
    }

}
