<?php
/**
 * App
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph;

use app as baseApp;
use template\menu\item as menuItem;
use user;
use web\request;

/**
 * App
 */
class app extends baseApp {
    protected function getRouter(request $request) : router {
        return new router($request);
    }

    public static function getMenu() : array {
        $menu = array(
            new menuItem("home", ""),
            new menuItem("albums", "album"),
            new menuItem("categories", "category"),
            new menuItem("people", "person/people", "canBrowsePeople"),
            new menuItem("places", "place", "canBrowsePlaces"),
            new menuItem("sets", "set/sets", "isAdmin", config: "feature.sets"),
            new menuItem("photos", "photos"),
            new menuItem("lightbox", "album", "getLightbox", parameter: array("album_id" => "getLightbox")),
            new menuItem("search", "search"),
            new menuItem("import", "import", "canImport", config: "import.enable"),
            new menuItem("admin", "admin", "isAdmin"),
            new menuItem("reports", "zoph/reports"),
            new menuItem("prefs", "user/prefs", "isDefault", negate: true),
            new menuItem("about", "zoph/info"),
            new menuItem("login", "zoph/logout", "isDefault"),
            new menuItem("logout", "zoph/logout", "isDefault", negate: true),
        );

        foreach ($menu as $item) {
            $item->setSelected(app::$page);
        }

        return $menu;
    }
}
