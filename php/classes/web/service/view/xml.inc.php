<?php
/**
 * View to display data as json
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\service\view;

use web\view\viewInterface;
use DomDocument;
use web\request;

/**
 * This view displays xml data
 */
class xml implements viewInterface {

    public function __construct(private request $request, private DomDocument $xml) {
    }

    public function view() : string {
        return $this->xml->SaveXML();
    }

    public function display($template = "xml") : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }
        echo $this->view();
    }

    public function getActionlinks() : ?array {
        return null;
    }

    public function getHeaders() : ?array {
        return array("Content-Type: text/xml");
    }

    public function getTitle() : string {
        return "";
    }

    public function getScripts() : array {
        return array();
    }
}
