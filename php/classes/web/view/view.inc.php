<?php
/**
 * Base class for views
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\view;

use conf\conf;
use template\block;
use template\page;
use template\template;
use web\request;

/**
 * Base view
 */
abstract class view implements viewInterface {

    /** @var string additional style to be added to header of the page */
    private $style;

    /** @var array scripts to include */
    public array $scripts = array();

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request) {
    }

    /**
     * Get headers
     * @return array
     */
    public function getHeaders() : array {
        return array();
    }

    /**
     * Get view
     * @return template view
     */
    abstract public function view() : null|block|string;

   /**
    * Output page
    * @codeCoverageIgnore
    */
    public function display($template = "html") : void {
        $page = new page($template, array(
            "title"     => $this->getTitle(),
            "style"     => $this->getStyle(),
            "scripts"   => $this->getScripts()
        ));

        foreach ($this->getHeaders() as $header) {
            header($header);
        }

        $page->addBlock($this->view());

        echo $page;
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() : string {
        return conf::get("interface.title");
    }

    public function setStyle(string $style) {
        $this->style = $style;
    }

    private function getStyle() {
        return $this->style;
    }

    public function getScripts() : array {

        $scripts = array(
            "js/util.js",
            "js/xml.js",
            "js/thumbview.js",
            "js/translate.js",
            "js/error.js"
        );

        if (conf::get("interface.autocomplete")) {
            $scripts[]="js/autocomplete.js";
        }

        if (conf::get("maps.provider")) {
            $scripts[]="js/leaflet-src.js";
            $scripts[]="js/maps.js";

            if (conf::get("maps.geocode")) {
                $scripts[]="js/geocode.js";
            }
        }


        return $scripts;
    }

}
