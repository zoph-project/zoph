<?php
/**
 * Controller for groups
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group;

use generic\controller as genericController;
use group;
use organiser\addRemoveMembersUpdateActionTrait;
use user;
use web\request;

/**
 * Controller for groups
 */
class controller extends genericController {
    use addRemoveMembersUpdateActionTrait;

    protected static $memberClass   = user::class;

    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewGroups    = view\groups::class;
    protected static $viewNew       = view\update::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected $actions = array("confirm", "delete", "display", "edit", "insert", "new", "update", "groups");

    public function doAction(string $action) : void {
        if (user::getCurrent()->isAdmin()) {
            if ($action != "groups") {
                $group = new group($this->request["group_id"]);
                $group->lookup();
                $this->setObject($group);
            }
            parent::doAction($action);
        } else {
            $this->view = new static::$viewRedirect($this->request);
            $this->view->setRedirect("zoph/welcome");
        }
    }

    protected function actionConfirm() : void {
        parent::actionConfirm();
        $this->view->setRedirect("group/groups");
    }

    protected function actionInsert() : void {
        parent::actionInsert();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

    protected function actionGroups() : void {
        $this->view = new static::$viewGroups($this->request, $this->object);
        $this->view->setObjects(group::getAll("group_name"));
    }

}
