<?php
/**
 * Selection class
 * A photo can be "selected" to be used in another part of Zoph,
 * for example to be set as a coverphoto or to define related photos
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

use template\actionlink;
use template\block;
use web\session;

/**
 * Selection class
 * A photo can be "selected" to be used in another part of Zoph,
 * for example to be set as a coverphoto or to define related photos
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class selection {
    /** @var array Photos in the selection  */
    private $photos=array();

    /**
     * Create a new selection object
     * @param array The links that need to be displayed with each photo
     * @param string return link
     * @param photo|Organiser the currently displayed object
     * @param string field to assign photo id to
     */
    public function __construct(
        private array       $links,
        private string      $return,
        private ?zophTable  $obj = null,
        private string|array|null     $field = "photo_id",
        private ?array       $session = null,
    ) {
        if (!isset($session)) {
            $session = session::getCurrent();
        }

        if (!isset($session["selected_photo"]) || sizeof($session["selected_photo"])===0) {
            throw new photoNoSelectionException("No photos selected");
        }

        foreach ($session["selected_photo"] as $photo_id) {
            $photo=new photo($photo_id);
            $photo->lookup();
            $this->photos[]=$photo;
        }
        if (sizeof($this->photos) === 0) {
            throw new photoNoSelectionException("No photos selected");
        }

    }

    /**
     * Display the selection div
     */
    public function __toString() {
        $photos=array();

        foreach ($this->photos as $photo) {
            $actionlinks=array();

            if (!($this->obj instanceof photo && ($this->obj->getId() == $photo->getId()))) {
                foreach ($this->links as $actionlink) {
                    if (is_array($this->field)) {
                        $field = array_shift($this->field);
                    } else {
                        $field = $this->field;
                    }

                    $actionlink->addParams(array(
                        $field    => $photo->getId(),
                        "_return" => $this->return
                    ));
                    $actionlinks[]=$actionlink;
                }
            }
            $actionlinks[] = new actionlink("x", "photo/deselect", array(
                "photo_id" => (int) $photo->getId(),
                "_return"  => $this->return
            ));

            $tplActionlinks=new block("actionlinks", array(
                "actionlinks"   => $actionlinks
            ));

            $photos[]=array(
                "actionlinks"   => $tplActionlinks,
                "photo"         => $photo
            );

        }

        $tpl=new block("selection", array(
            "count"     => count($this->photos),
            "photos"    => $photos
        ));
        return (string) $tpl;
    }

    public static function select(photo $photo) : void {
        $session = session::getCurrent();
        $selectKey=false;

        if (isset($session["selected_photo"])) {
            if (!is_array($session["selected_photo"])) {
                $session["selected_photo"] = array();
            }
            $selectKey=array_search($photo->getId(), $session["selected_photo"]);
        }
        if ($selectKey === false) {
            $selection = $session["selected_photo"];
            $selection[]=$photo->getId();
            $session["selected_photo"] = $selection;
        }

    }

    public static function deselect(photo $photo) : void {
        $session = session::getCurrent();
        $selectKey=false;

        if (isset($session["selected_photo"])) {
            if (!is_array($session["selected_photo"])) {
                $session["selected_photo"] = array();
            }
            $selectKey=array_search($photo->getId(), $session["selected_photo"]);
        }
        if ($selectKey !== false) {
            $selection = $session["selected_photo"];
            unset($selection[$selectKey]);
            $session["selected_photo"] = $selection;
        }

    }


}
