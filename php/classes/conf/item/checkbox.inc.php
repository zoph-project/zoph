<?php
/**
 * A checkbox configuration item defines a configuration item that can be true or false
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace conf\item;

use template\block;

/**
 * A checkbox configuration item defines a configuration item that can be true or false
 * @package Zoph
 * @author Jeroen Roos
 */
class checkbox extends item {

    /**
     * Set value
     * @param bool value
     */
    public function setValue(bool|string|null $value) : void {
        if ($value === "true") {
            $value = true;
        } else if ($value === "false") {
            $value = false;
        }
        parent::setValue((bool) $value);
    }

    /**
     * Check value
     * check if a specific value is legal for this option
     * @param string value
     * @return bool
     */
    public function checkValue(bool|string $value) : bool {
        return ((bool) $value == $value);
    }

    /**
     * Get value of item for display
     * @return string value
     */
    public function displayValue() : string {
        return ($this->getValue() ? "true": "false");
    }


    /**
     * Get value of item
     * if value is not set, get default
     * @return bool value
     */
    public function getValue() : ?bool {
        if (!isset($this->fields["value"]) || $this->fields["value"]===null || !$this->requirementsMet()) {
            return (bool) $this->getDefault();
        } else {
            return (bool) $this->fields["value"];
        }
    }

    /**
     * Display this option through template
     * @return block template block
     */
    public function display() : ?block {
        if ($this->internal) {
            return null;
        }
        return new block("confItemCheckbox", array(
            "label" => e(translate($this->getLabel(), 0)),
            "name" => e($this->getName()),
            "checked" => $this->getValue() ? "checked" : "",
            "desc" => e(translate($this->getDesc(), 0)),
            "hint" => e(translate($this->getHint(), 0)),
        ));
     }
}
