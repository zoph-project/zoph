<?php
/**
 * This class defines a minimal configuration set, as used during install
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace conf;

use conf\item\text;
use conf\item\checkbox;
use conf\item\number;
use conf\item\select;
use conf\item\salt;

use template\template;

/**
 * conf\minimal is the class that defines config options & their defaults
 * in the database, in a minimal configuration
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class minimal extends conf {
    /**
     * Get default config
     */
    protected static function getConfig() {
        static::getConfigInterface();
    }

    /**
     * Get config collection for interface settings
     */
    private static function getConfigInterface() {
        $interface = new collection();

        $intWidth = new text();
        $intWidth->setName("interface.width");
        $intWidth->setLabel("Screen width");
        $intWidth->setDesc("A number in pixels (\"px\") or percent (\"%\"), the latter " .
            "is a percentage of the user's browser window width.");
        $intWidth->setDefault("1000px");
        $intWidth->setRegex("^[0-9]+(px|%)$");
        $interface[]=$intWidth;
        $intTpl = new select();
        $intTpl->setName("interface.template");
        $intTpl->setLabel("Template");
        $intTpl->setDesc("The template Zoph uses");
        $intTpl->addOptions(array("default"));
        $intTpl->setDefault("default");
        $interface[]=$intTpl;

        $intCookieExpire = new select();
        $intCookieExpire->setName("interface.cookie.expire");
        $intCookieExpire->setLabel("Cookie Expiry Time");
        $intCookieExpire->setDesc("Set the time after which a cookie will expire, that is, " .
            "when a user will need to re-login. \"session\" (default) means: until user " .
            "closes the browser");
        $intCookieExpire->addOptions(array(
            0       => "session",
        ));
        $intCookieExpire->setDefault(0);
        $interface[]=$intCookieExpire;

        conf::addGroup($interface, "interface", "Interface settings",
            "Settings that define how Zoph looks");

    }
}
