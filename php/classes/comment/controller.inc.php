<?php
/**
 * Controller for comments
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment;

use conf\conf;
use comment;
use generic\controller as genericController;
use web\request;
use photo;
use user;

use photoNotAccessibleSecurityException;
use securityException;
use userInsufficientRightsSecurityException;

/**
 * Controller for comment
 */
class controller extends genericController {
    protected static $viewComments  = view\comments::class;
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewUpdate    = view\update::class;

    /** @var photo photo that this comment belongs to */
    private $photo;

    /** @var array objects (only used when displaying all comments) */
    private $objects = array();

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "comments", "delete", "display", "edit", "insert",
        "new", "update"
    );

    /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        try {
            $user=user::getCurrent();
            if (!$user->canLeaveComments()) {
                throw new userInsufficientRightsSecurityException("User has no rights to leave comments");
            }

            if ($action == "comments") {
                $comments=comment::getRecords();
                foreach ($comments as $comment) {
                    $photo=$comment->getPhoto();
                    if ($user->getPhotoPermissions($photo) || $user->canSeeAllPhotos()) {
                        $this->objects[] = $comment;
                    }
                }
                parent::doAction($action);
            } else {
                $comment = $this->getComment();
                $this->setPhotoFromComment($comment);

                if ($this->photo instanceof photo &&
                    ($user->getPhotoPermissions($this->photo) || $user->canSeeAllPhotos())) {
                        $this->object=$comment;
                        parent::doAction($action);
                } else {
                    throw new photoNotAccessibleSecurityException("User has no access to this photo");
                }
            }
        } catch (securityException $e) {
            $this->actionForbidden($e);
        }

    }

    private function getComment() {
        $commentId = (int) $this->request["comment_id"];
        $comment = new comment($commentId);
        if ($commentId !== 0) {
            $comment->lookup();
        }
        return $comment;
    }

    private function setPhotoFromComment(comment $comment) {
        $photo = $comment->getPhoto();
        if (!$photo instanceof photo && $comment->getId() == 0) {
            $photo = new photo((int) $this->request["photo_id"]);
            $photo->lookup();
        }
        $this->photo=$photo;
    }

    protected function actionComments() {
        $this->view = new static::$viewComments($this->request, $this->objects);
    }

    /**
     * Do action 'confirm'
     */
    protected function actionConfirm() {
        $user = user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            parent::actionConfirm();
            $this->view->setRedirect("photo?photo_id=" . $this->photo->getId());
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'delete'
     */
    protected function actionDelete() {
        $user = user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            parent::actionDelete();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'edit'
     */
    protected function actionEdit() {
        $user = user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'update'
     */
    protected function actionUpdate() {
        $user=user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            unset($this->request["photo_id"]);
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'insert'
     */
    protected function actionInsert() {
        $user = user::getCurrent();
        if ($user->canLeaveComments()) {
            $comment = new comment();
            $comment->set("user_id", user::getCurrent()->getId());
            $comment->set("ipaddr", $this->request->getServerVar("REMOTE_ADDR"));
            $this->setObject($comment);
            unset($this->request["photo_id"]);
            parent::actionInsert();
            $comment->addToPhoto($this->photo);
        }
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $this->view->setRedirect("photo?photo_id=" . $this->photo->getId());
    }
}
