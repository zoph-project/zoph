<?php
/**
 * This is a trait to add autocover functionality to organisers
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace organiser;

use db\selectHelper;
use db\select;
use db\clause;
use db\param;
use PDO;
use photo;

/**
 * Autocover for organisers
 *
 * @author Jeroen Roos
 * @package Zoph
 */
trait autocoverTrait {
    /**
     * Get autocover for this organiser
     * @param string how to select a coverphoto: oldest, newest, first, last, random, highest
     * @param bool choose autocover from this organiser AND children
     * @return photo coverphoto
     */
    public function getAutoCover($autocover=null, $children=false) : ?photo {
        $coverphoto=$this->getCoverphoto();

        if (!($coverphoto instanceof photo)) {
            $qry=$this->getAutoCoverQuery($children);

            $qry = selectHelper::expandQueryForUser($qry);

            $qry=selectHelper::getAutoCoverOrder($qry, $autocover);

            $coverphotos=photo::getRecordsFromQuery($qry);
            $coverphoto=array_shift($coverphotos);

            if (!($coverphoto instanceof photo) && !$children) {
                // No photos found in this organiser, let's retry, but also looking in sub-organisers
                return $this->getAutoCover($autocover, true);
            }
        }

        return $coverphoto;
    }

    /**
     * Get query to select coverphoto for this organiser.
     * @param bool choose autocover from this organiser AND children
     * @return select SQL query
     */
    protected function getAutoCoverBaseQuery($id, $children) : select {
        $qry=new select(array("p" => "photos"));
        $qry->addFunction(array("photo_id" => "DISTINCT ar.photo_id"));
        $qry->join(array("ar" => "view_photo_avg_rating"), "p.photo_id = ar.photo_id");

        if ($children) {
            $ids=new param(":ids", $this->getBranchIdArray(), PDO::PARAM_INT);
            $qry->addParam($ids);
            $where=clause::InClause($id, $ids);
        } else {
            $where=new clause($id . "=:id");
            $qry->addParam(new param(":id", $this->getId(), PDO::PARAM_INT));
        }
        $qry->where($where);


        return $qry;
    }
}
