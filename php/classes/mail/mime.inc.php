<?php
/**
 * HTML MIME Mail composer class
 *
 *  @copyright 2002-2003  Richard Heyes
 *  @copyright 2003-2005  The PHP Group
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  o Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  o Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  o The names of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Tobias Ratschiller <tobias@dnet.it>
 * @author Sascha Schumann <sascha@schumann.cx>
 * @author Richard Heyes <richard@phpguru.org>
 * @author Tomas V.V.Cox <cox@idecnet.com>
 *
 * @author Jeroen Roos
 *
 * @package Zoph
 */

namespace mail;

/**
 * Mime mail composer class. Can handle: text and html bodies, embedded html
 * images and attachments.
 * @author Tobias Ratschiller <tobias@dnet.it>
 * @author Sascha Schumann <sascha@schumann.cx>
 * @author Richard Heyes <richard@phpguru.org>
 * @author Tomas V.V.Cox <cox@idecnet.com>
 *
 * @author Jeroen Roos
 *
 * @package Zoph
 */
class mime {
    /** @var string Contains the plain text part of the email */
    private $txtbody;
    /** @var string Contains the html part of the email */
    private $htmlbody;
    /** @var array list of the attached images */
    private $htmlImages = array();
    /** @var array list of the attachments */
    private $parts = array();
    /** @var array Build parameters
     *    text_encoding  -  What encoding to use for plain text
     *                      Default is 7bit
     *    html_encoding  -  What encoding to use for html
     *                      Default is quoted-printable
     *    7bit_wrap      -  Number of characters before text is
     *                      wrapped in 7bit encoding
     *                      Default is 998
     *    html_charset   -  The character set to use for html.
     *                      Default is utf-8
     *    text_charset   -  The character set to use for text.
     *                      Default is utf-8
     *    head_charset   -  The character set to use for headers.
     *                      Default is utf-8
     */
    private $buildParams = array(
        'text_encoding' => '7bit',
        'html_encoding' => 'quoted-printable',
        '7bit_wrap'     => 998,
        'html_charset'  => 'utf-8',
        'text_charset'  => 'utf-8',
        'head_charset'  => 'utf-8'
    );
    /** @var array Headers for the mail */
    private $headers = array();

    /**
     * Accessor function to set the body text. Body text is used if
     * it's not an html mail being sent or else is used to fill the
     * text/plain part that emails clients who don't support
     * html should show.
     */
    public function setTXTBody(string $data) : void {
        $this->txtbody = $data;
    }

    /**
     * Adds a html part to the mail
     */
    public function setHTMLBody($data) : void {
        $this->htmlbody = $data;
    }

    /**
     * Adds an image to the list of embedded images. The source is a string containing the image.
     *
     * @paramstring The image data.
     * @param string The file name
     * @param string The content type
     * @return bool true
     */
    public function addHTMLImageFromString($filedata, $filename, $cType='application/octet-stream') {
        $filename = basename($filename);
        $this->htmlImages[] = array(
            'body'   => $filedata,
            'name'   => $filename,
            'cType'  => $cType,
            'cid'    => md5(uniqid(time()))
        );
        return true;
    }

    /**
     * Adds an image to the list of embedded images. The source is a file on disk.
     *
     * @param string The file to be used as attachment
     * @param string The content type
     * @param string encoding.
     */
    public function addHTMLImageFromFile($file, $cType='application/octet-stream') {
        $filedata = $this->fileToString($file);
        return $this->addHTMLImageFromString($filedata, $file, $cType);
    }

    /**
     * Adds a file to the list of attachments. The source is a string containing the
     * contents of the file.
     *
     * @param string The file data to use as attachment
     * @param string The content type
     * @param string The filename of the attachment.
     * @param string encoding.
     * @throws mail\exception
     */
    public function addAttachmentFromString($filedata, $filename, $cType = 'application/octet-stream', $encoding = 'base64') {
        if (empty($filename)) {
            throw new exception("The supplied filename for the attachment can\'t be empty");
        }
        $filename = basename($filename);
        $this->parts[] = array(
            'body'     => $filedata,
            'name'     => $filename,
            'cType'    => $cType,
            'encoding' => $encoding
        );
    }

    /**
     * Adds a file to the list of attachments. The source is a file on disk.
     *
     * @param string The file to be used as attachment
     * @param string The content type
     * @param string encoding.
     */
    public function addAttachmentFromFile($file, $cType = 'application/octet-stream', $encoding = 'base64') {
        $filedata=$this->fileToString($file);
        $this->addAttachmentFromString($filedata, $file, $cType, $encoding);
    }


    /**
     * Get the contents of the given file name as string
     *
     * @param  string  path of file to process
     * @return string  contents of $filename
     * @throws mail\exception
     */
    private function fileToString($filename) {
        if (!is_readable($filename)) {
            throw new exception('File is not readable ' . $filename);
        }
        if (!$fd = fopen($filename, 'rb')) {
            throw new exception('Could not open ' . $filename);
        }
        $filesize = filesize($filename);
        if ($filesize == 0) {
            $cont =  "";
        } else {
            $cont = fread($fd, $filesize);
        }
        fclose($fd);
        return $cont;
    }

    /**
     * Adds a text subpart to the mail\mimePart object and
     * returns it during the build process.
     *
     * @param mixed The object to add the part to, or null if a new object is to be created.
     * @param string   The text to add.
     * @return mail\mimePart The text mail\mimePart object
     */
    private function addTextPart($obj, $text) {
        $params['content_type'] = 'text/plain';
        $params['encoding']     = $this->buildParams['text_encoding'];
        $params['charset']      = $this->buildParams['text_charset'];
        if (is_object($obj)) {
            return $obj->addSubpart($text, $params);
        } else {
            return new mimePart($text, $params);
        }
    }

    /**
     * Adds a html subpart to the mail\mimePart object and
     * returns it during the build process.
     *
     * @param  mixed   The object to add the part to, or null if a new object is to be created.
     * @return mail\mimePart The html mail\mimePart object
     */
    private function addHtmlPart($obj = null) {
        $params['content_type'] = 'text/html';
        $params['encoding']     = $this->buildParams['html_encoding'];
        $params['charset']      = $this->buildParams['html_charset'];
        if (is_object($obj)) {
            return $obj->addSubpart($this->htmlbody, $params);
        } else {
            return new mimePart($this->htmlbody, $params);
        }
    }

    /**
     * Creates a new mail\mimePart object, using multipart/mixed as
     * the initial content-type and returns it during the
     * build process.
     *
     * @return mail\mimePart  The multipart/mixed mail\mimePart object
     */
    private function addMixedPart() {
        $params['content_type'] = 'multipart/mixed';
        return new mimePart('', $params);
    }

    /**
     * Adds a multipart/alternative part to a mail\mimePart
     * object (or creates one), and returns it during
     * the build process.
     *
     * @param  mixed   The object to add the part to, or
     *                 null if a new object is to be created.
     * @return mail\mimePart  The multipart/mixed mail\mimePart object
     */
    private function addAlternativePart($obj = null) {
        $params['content_type'] = 'multipart/alternative';
        if (is_object($obj)) {
            return $obj->addSubpart('', $params);
        } else {
            return new mimePart('', $params);
        }
    }

    /**
     * Adds a multipart/related part to a mail\mimePart
     * object (or creates one), and returns it during
     * the build process.
     *
     * @param mixed    The object to add the part to, or
     *                 null if a new object is to be created
     * @return mail\mimePart  The multipart/mixed mail\mimePart object
     */
    private function addRelatedPart($obj = null) {
        $params['content_type'] = 'multipart/related';
        if (is_object($obj)) {
            return $obj->addSubpart('', $params);
        } else {
            return new mimePart('', $params);
        }
    }

    /**
     * Adds an html image subpart to a mail\mimePart object
     * and returns it during the build process.
     *
     * @param  mail\mimePart The mail\mimePart to add the image to
     * @param  array   The image information
     * @return mail\mimePart  The image mail\mimePart object
     */
    private function addHtmlImagePart(mimePart $obj, $value) {
        $params['content_type'] = $value['cType'];
        $params['encoding']     = 'base64';
        $params['disposition']  = 'inline';
        $params['dfilename']    = $value['name'];
        $params['cid']          = $value['cid'];
        $obj->addSubpart($value['body'], $params);
    }

    /**
     * Adds an attachment subpart to a mail\mimePart object
     * and returns it during the build process.
     *
     * @param  mail\mimePart  The mail\mimePart to add the image to
     * @param  array   The attachment information
     * @return mail\mimePart  The image mail\mimePart object
     */
    private function addAttachmentPart(mimePart $obj, $value) {
        $params['content_type'] = $value['cType'];
        $params['encoding']     = $value['encoding'];
        $params['disposition']  = 'attachment';
        $params['dfilename']    = $value['name'];
        $obj->addSubpart($value['body'], $params);
    }

    /**
     * Builds the multipart message from the list ($this->parts) and
     * returns the mime content.
     * @return string The mime content
     */
    public function get() {
        if (!empty($this->htmlImages) && isset($this->htmlbody)) {
            foreach ($this->htmlImages as $value) {
                $regex = '#(\s)((?i)src|background|href(?-i))\s*=\s*(["\']?)' .  preg_quote($value['name'], '#') .  '\3#';
                $rep = '\1\2=\3cid:' . $value['cid'] .'\3';
                $this->htmlbody = preg_replace($regex, $rep, $this->htmlbody);
            }
        }

        $hasAttachments = !empty($this->parts);
        $hasHtmlImages  = !empty($this->htmlImages);
        $hasHtmlBody    = !empty($this->htmlbody);
        $hasTextBody    = (!$hasHtmlBody && !empty($this->txtbody));
        if ($hasTextBody) {
            $message = $this->getText($hasAttachments);
        } else if (!$hasHtmlBody && $hasAttachments) {
            $message = $this->getAttachments();
        } else {
            $message = $this->getHTML($hasAttachments, $hasHtmlImages);
        }

        if (isset($message)) {
            $output = $message->encode();
            $this->headers = array_merge($this->headers, $output['headers']);
            return $output['body'];
        } else {
            return false;
        }
    }

    private function getText(bool $hasAttachments) {
        if ($hasAttachments) {
            $message = $this->addMixedPart();
            $this->addTextPart($message, $this->txtbody);
            foreach ($this->parts as $part) {
                $this->addAttachmentPart($message, $part);
            }
        } else {
            $message = $this->addTextPart(null, $this->txtbody);
        }
        return $message;
    }

    private function getAttachments() {
        $message = $this->addMixedPart();
        foreach ($this->parts as $part) {
            $this->addAttachmentPart($message, $part);
        }
        return $message;
    }

    private function getHTML(bool $hasAttachments, bool $hasHtmlImages) {
        if ($hasAttachments) {
            return $this->getHTMLAttachments($hasHtmlImages);
        } else {
            return $this->getHTMLNoAttachments($hasHtmlImages);
        }
    }

    private function getHTMLNoAttachments(bool $hasHtmlImages) {
        if ($hasHtmlImages) {
            if (isset($this->txtbody)) {
                $message =$this->addAlternativePart();
                $this->addTextPart($message, $this->txtbody);
                $related = $this->addRelatedPart($message);
            } else {
                $message = $this->addRelatedPart();
                $related = $message;
            }
            $this->addHtmlPart($related);
            foreach ($this->htmlImages as $img) {
                $this->addHtmlImagePart($related, $img);
            }
        } else {
            if (isset($this->txtbody)) {
                $message = $this->addAlternativePart();
                $this->addTextPart($message, $this->txtbody);
                $this->addHtmlPart($message);
            } else {
                $message = $this->addHtmlPart();
            }

        }
        return $message;
    }

    private function getHTMLAttachments(bool $hasHtmlImages) {
        if ($hasHtmlImages) {
            $message = $this->addMixedPart();
            if (isset($this->txtbody)) {
                $alt = $this->addAlternativePart($message);
                $this->addTextPart($alt, $this->txtbody);
                $rel = $this->addRelatedPart($alt);
            } else {
                $rel = $this->addRelatedPart($message);
            }
            $this->addHtmlPart($rel);
            foreach ($this->htmlImages as $img) {
                $this->addHtmlImagePart($rel, $img);
            }
            foreach ($this->parts as $part) {
                $this->addAttachmentPart($message, $part);
            }
        } else {
            $message = $this->addMixedPart();
            if (isset($this->txtbody)) {
                $alt = $this->addAlternativePart($message);
                $this->addTextPart($alt, $this->txtbody);
                $this->addHtmlPart($alt);
            } else {
                $this->addHtmlPart($message);
            }
            foreach ($this->parts as $part) {
                $this->addAttachmentPart($message, $part);
            }
        }
        return $message;
    }

    /**
     * Returns an array with the headers needed to prepend to the email
     * (MIME-Version and Content-Type). Format of argument is:
     * $array['header-name'] = 'header-value';
     *
     * @param  array Assoc array with any extra headers.  Optional.
     * @return array Assoc array with the mime headers
     */
    public function headers(array $extraHeaders = null) {
        // Content-Type header should already be present,
        // So just add mime version header
        $headers['MIME-Version'] = '1.0';
        if (isset($extraHeaders)) {
            $headers = array_merge($headers, $extraHeaders);
        }
        $this->headers = array_merge($headers, $this->headers);

        return $this->encodeHeaders($this->headers);
    }

    /**
     * Get the text version of the headers
     * (useful if you want to use the PHP mail() function)
     *
     * @param  array headers Assoc array with any extra headers. Optional.
     * @return string  Plain text headers
     */
    public function txtHeaders(array $extraHeaders = null) {
        $headers = $this->headers($extraHeaders);
        $ret = '';
        foreach ($headers as $key => $val) {
            $ret .= "$key: $val" . PHP_EOL;
        }
        return $ret;
    }

    /**
     * Sets the Subject header
     *
     * @param  string $subject String to set the subject to
     */
    public function setSubject($subject) {
        $this->headers['Subject'] = $subject;
    }

    /**
     * Set an email to the From (the sender) header
     *
     * @param  string $email The email address to add
     */
    public function setFrom($email) {
        $this->headers['From'] = $email;
    }

    /**
     * Add an email to the Cc (carbon copy) header
     * (multiple calls to this method are allowed)
     *
     * @param  string The email address to add
     */
    public function addCc($email) {
        if (isset($this->headers['Cc'])) {
            $this->headers['Cc'] .= ", $email";
        } else {
            $this->headers['Cc'] = $email;
        }
    }

    /**
     * Add an email to the Bcc (blind carbon copy) header
     * (multiple calls to this method are allowed)
     *
     * @param  string The email address to add
     */
    public function addBcc($email) {
        if (isset($this->headers['Bcc'])) {
            $this->headers['Bcc'] .= ", $email";
        } else {
            $this->headers['Bcc'] = $email;
        }
    }

    /**
     * Encodes a header as per RFC2047
     *
     * @param  string  The header data to encode
     * @return string  Encoded data
     */
    private function encodeHeaders($input) {
        foreach ($input as $header => $value) {
            preg_match_all('/(\w*[\x80-\xFF]+\w*)/', $value, $matches);
            foreach ($matches[1] as $value) {
                $replacement = preg_replace('/([\x80-\xFF])/e', '"=" .  strtoupper(dechex(ord("\1")))', $value);
                $value = str_replace($value, '=?' .  $this->buildParams['head_charset'] .  '?Q?' . $replacement . '?=', $value);
            }
            $input[$header] = $value;
        }

        return $input;
    }

} // End of class
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
?>
