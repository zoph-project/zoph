<?php
/**
 * View for display mail compose page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace mail\view;

use web\view\viewInterface;
use conf\conf;
use template\block;
use template\form;
use template\template;
use user;

/**
 * This view displays a from to create a mail to a Zoph user
 */
class compose extends view implements viewInterface {

    /**
     * Output view
     */
    public function view() : block {
        $msg="";
        $subject = sprintf(translate("A Photo from %s"), conf::get("interface.title")) . ": ".  $this->object->get("name");


        foreach ($this->object->getEmailArray() as $name => $value) {
            if ($name && $value) {
                $msg .= "$name: $value\r\n";
            }
        }

        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
            "error"             => $this->error,
            "warning"           => $this->warning,
            "success"           => $this->success,
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $form = $this->getMailForm("mail", $msg);

        $form->addInputHidden("photo_id", $this->object->getId());
        $form->addDropdown("html", template::createYesNoDropdown("html", $this->request["html"] ?? "1"), translate("send as html"));
        $form->addInputText("subject", $subject, translate("subject"), sprintf(translate("%s chars max"), "40"), 40);
        $form->addDropdown("_size",
            template::createDropdown("_size", $this->request["_size"] ?? "mid", array(
                "full" => translate("Yes", 0),
                "mid" => translate("No", 0)
            )), translate("send fullsize"));
        $form->addDropdown("includeurl", template::createYesNoDropdown("includeurl", $this->request["includeurl"] ?? "1"), translate("include URL"));
        $form->addBlock($this->object->getImageTag(MID_PREFIX));

        $tpl->addBlock($form);

        return $tpl;
    }
}
