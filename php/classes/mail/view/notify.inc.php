<?php
/**
 * View for display notify mail compose page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace mail\view;

use web\view\viewInterface;
use conf\conf;
use template\block;
use template\form;
use template\template;
use user;
use web\url;

/**
 * This view displays a form to notify a Zoph user of new albums he or she gained access to
 */
class notify extends view implements viewInterface {

    protected $user;
    private $albums = array();

    /**
     * Output view
     */
    public function view() : block {
        $subject = translate("New Albums on") . " " . conf::get("interface.title");

        $msg = $this->getAlbumList();

        if ($this->request["showusername"]) {
            $msg .= translate("user name", 0) . ": " .  $this->user->get('user_name') . "\n";
        }

        $form = $this->getMailForm("mailnotify", $msg);
        if (!empty($this->albums)) {
            $form->addInputHidden("setlastmodified", 1);
        }
        $form->addInputHidden("user_id", $this->user->getId());
        $form->addInputText("subject", $subject, translate("subject"), sprintf(translate("%s chars max"), "40"), 40);



        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
            "error"             => $this->error,
            "warning"           => $this->warning,
            "success"           => $this->success,
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock($form);

        return $tpl;
    }

    private function getAlbumList() : string {
        if (empty($this->albums)) {
            return "";
        }

        $albumList = array();
        foreach ($this->albums as $album) {
            $albumPath = '';
            $ancestors = $album->getAncestors();
            if ($ancestors) {
                while ($parent = array_pop($ancestors)) {
                    $albumPath .= $parent->get("album") .  " > ";
                }
            }
            $albumPath .= $album->get("album");
            $albumList[] = $albumPath;
        }

        sort($albumList);
        reset($albumList);

        $body = translate("I have enabled access to the following albums for you:", 0) . "\n\n";

        $body .= implode("\n", $albumList) . "\n\n";

        $body .= translate("For accessing these Albums you have to use this URL:", 0);
        $body .= " " . url::get() . "\n";

        return $body;

    }


    public function setUser(user $user = null) : void {
        $this->user = $user;
    }

    public function setAlbums(array $albums) : void {
        $this->albums = $albums;
    }

    public function getTitle() : string {
        return translate("Notify user");
    }

}
