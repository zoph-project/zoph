<?php
/**
 * View to display slidewhow
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use web\view\viewInterface;
use conf\conf;
use template\block;
use web\request;
use web\view\view;
use user;

/**
 * This view displays a slideshow
 */
class slideshow extends view implements viewInterface {
    /** * @var array request variables */
    private $vars;

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request) {
        $this->vars=$request->getRequestVars();
    }

    /**
     * Output view
     */
    public function view() : block {
        $user = user::getCurrent();

        return new block("slideshow", array(
            "javascript" => "var duration=" . (int) $user->prefs->get("slideshow_time") . ";"
        ));
    }

    public function getTitle() : string {
        return "Slideshow";
    }

    public function getScripts() : array {
        $scripts = parent::getScripts();
        $scripts[]="js/json.js";
        $scripts[]="js/slideshow.js";
        return $scripts;
    }


}
