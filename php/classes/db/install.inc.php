<?php
/**
 * Create Database & set access rights
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

use template\result;

/**
 * Database creation
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class install {

    /** @var array store database login details */
    private $db;

    private $results = array();
    private $errors = array();

    private $host;

    public function __construct(string $root, string $rootpw) {
        $this->db = db::getLoginDetails();

        db::setLoginDetails($this->db["host"], "mysql", $root, $rootpw, $this->db["prefix"]);

        $this->host = $this->getHost();
    }

    public function create() :void {
        try {
            db::SQL("CREATE DATABASE IF NOT EXISTS " . $this->db["dbname"] .
                " CHARACTER SET utf8 COLLATE utf8_general_ci;");
            $this->results["create"] = result::SUCCESS;
        } catch (exception $e) {
            $this->results["create"] = result::ERROR;
            $this->errors["create"] = $e->getMessage();
        }
    }

    public function user() : void {
        if ($this->getUser() == $this->db["user"]) {
            $this->results["user"] = result::WARNING;
            $this->errors["user"] = "The user already exists in the database and was not created.";
            return;
        }

        // @codeCoverageIgnoreStart
        try {
            db::SQL("CREATE USER IF NOT EXISTS " . $this->db["user"] . "@" . $this->host .
                " IDENTIFIED BY \"" . $this->db["pass"] . "\";");
            $this->results["user"] = result::SUCCESS;
        } catch (exception $e) {
            $this->results["user"] = result::ERROR;
            $this->errors["user"] = $e->getMessage();
        }
        // @codeCoverageIgnoreEnd
    }

    public function grant() : void {
        if ($this->getUser() == $this->db["user"]) {
            $this->results["grant"] = result::WARNING;
            $this->errors["grant"] = "The user already exists in the database, GRANTs were not executed.";
            return;
        }

        // @codeCoverageIgnoreStart
        try {
            $rows=db::SQL("SELECT USER();")->fetch();

            $host=explode("@", ($rows[0]))[1];

            $qry = "GRANT ALL ON " . $this->db["dbname"] . ".* TO " . $this->db["user"] . "@" . $host . ";";
            db::SQL($qry);
            $this->results["grant"] = result::SUCCESS;
        } catch (exception $e) {
            $this->results["grant"] = result::ERROR;
            $this->errors["grant"] = "The GRANT operation failed with the following error:";
            $this->errors["grant"] .= "<pre>";
            $this->errors["grant"] .= $e->getMessage();
            $this->errors["grant"] .= "</pre>";
            $this->errors["grant"] .= "You may need to execute the query <tt>" . $qry . "</tt> manually.";
        }
        // @codeCoverageIgnoreEnd
    }

    public function getHost() {
        try {
            $rows=db::SQL("SELECT USER();")->fetch();
            $this->results["host"] = result::SUCCESS;
            return explode("@", ($rows[0]))[1];
        } catch (exception $e) {
            $this->results["get host"] = result::ERROR;
            $this->errors["get host"] = $e->getMessage();
        }
    }

    private function getUser() {
        $rows=db::SQL("SELECT USER();")->fetch();
        return explode("@", ($rows[0]))[0];
    }

    public function getResults() : array {
        $results = array();
        foreach ($this->results as $key => $result) {
            $results[] = new result($result, $key, $this->errors[$key] ?? "");
        }
        return $results;
    }

}
