<?php
/**
 * Database CREATE VIEW query
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

/**
 * Database CREATE VIEW query
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class view extends query {

    /** @var string name of the table */
    private $name;

    /** @var select query to use for view  */
    private $query=array();

    /** @var bool create view with IF NOT EXISTS statement  */
    private $ifNotExists=false;

    /**
     * Create a new view
     * @param string name
     */
    public function __construct(string $name) {
        $this->name=$name;
    }

    public function setQuery(select $qry) {
        $this->query=$qry;
    }

    public function ifNotExists($if = true) : void {
        if (db::getServerType() != db::MYSQL) {
            // MySQL does not support IF NOT EXIST on CREATE VIEW
            // For now, we silently ignore.
            $this->ifNotExists = $if;
        }
    }

    /**
     * Build the query
     * @return string create view query
     */
    public function __toString() {
        return "CREATE VIEW " . ($this->ifNotExists ? "IF NOT EXISTS " : "") .
            db::getPrefix() . $this->name . " AS\n" . (string) $this->query;
    }
}
