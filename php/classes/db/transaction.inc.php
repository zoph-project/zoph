<?php
/**
 * Database TRANSACTION support
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

use log;

/**
 * Database TRANSACTIONs
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class transaction {

    /**
     * Create a new TRANSACTION
     * @param string table name (ignored)
     */
    public function __construct() {
        db::beginTransaction();
    }

    public function doQuery(query $qry) {
        try {
            return $qry->execute();
        } catch (\Exception $e) {
            log::msg("Transaction failed, rollback.", log::ERROR, log::DB);
            log::msg($e->getMessage(), log::DEBUG, log::DB);
            $this->rollback();
            throw $e;
        }
    }

    public function commit() {
        db::commit();
    }

    public function rollback() {
        db::rollback();
    }
}
