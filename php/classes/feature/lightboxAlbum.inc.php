<?php
/**
 * Feature class for lightbox album
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace feature;

use db\clause;
use db\param;
use db\select;
use photo\collection;
use template\block;
use user;
use DateInterval;
use PDO;
use zoph\app;

/**
 * Feature class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class lightboxAlbum extends base {
    protected $type = "lightbox";
    protected $title = "Lightbox Album";
    protected $width = THUMB_SIZE + 10;

    public function getData(int $count = 10) {
        $photos = collection::createFromVars(array(
            "album_id"         => (user::getCurrent())->getLightbox()->getId(),
            "_order"        => "date",
            "_dir"          => "desc"
        ))->subset(0, $count)->toArray();
        return parent::getPhotoData($photos);
    }

    public function getMore() : ?block {
        return new block("link", array(
            "href"  => app::getBasePath() . "photos?album_id=" . (user::getCurrent())->getLightbox()->getId(),
            "link"  => translate("See all photos in lightbox album"))
        );
    }
}
