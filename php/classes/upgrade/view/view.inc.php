<?php
/**
 * Base View for migrations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade\view;

use web\view\viewInterface;
use upgrade\migrations;
use template\template;
use web\request;
use web\view\view as webView;

/**
 * base view for migrations
 */
abstract class view extends webView implements viewInterface {

    /** @var migrations migrations */
    protected $migrations;

    /** @var string title */
    protected $title;

    /**
     * Create view
     * @param request web request
     * @param migrations to be displayed
     */
    public function __construct(request $request, migrations $migrations = null) {
        $this->migrations = $migrations;
        if ($migrations && sizeof($migrations->get()) == 1) {
            $this->title=$migrations->get()[0]->getDesc();
        }
    }

    /**
     * Get Actionlinks
     * @return array
     */
    protected function getActionlinks() : array {
        return array();
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() : string {
        return $this->title ?: translate("Zoph Upgrade");
    }
}
