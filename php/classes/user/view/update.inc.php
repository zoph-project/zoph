<?php
/**
 * View to update user
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user\view;

use album;
use conf\conf;
use person;
use user;
use template\actionlink;
use template\block;
use template\form;
use template\template;
use web\request;
use zoph\app;

/**
 * Update screen for user
 */
class update extends view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $param = array(
            "user_id" => (int) $this->object->getId()
        );
        if ($this->request->getAction() == "new") {
            return array(
                new actionlink("return", "user/users"),
            );
        } else {
            return array(
                new actionlink("delete", "user/delete", $param),
                new actionlink("change password", "user/password", $param),
                new actionlink("new", "user/new"),
                new actionlink("return", "user", $param)
            );
        }
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $action = $this->request->getAction() == "new" ? "insert" : "update";

        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
            "obj"               => $this->object,
        ));

        $tpl->addActionlinks($this->getActionlinks());

        if (isset($this->msg)) {
            $tpl->addblock($this->msg->view());
        }

        $form=new form("form", array(
            "formAction"        => app::getBasePAth() . "user/" . $action,
            "class"             => "user",
            "onsubmit"          => null,
            "submit"            => translate("submit")
        ));

        $personDropdown=template::createDropdown("person_id",
            ($action == "insert" ? "1" : $this->object->get("person_id")),
            person::getSelectArray());
        $userClassDropdown=template::createDropdown("user_class", $this->object->get("user_class"),
            array("1" => translate("User", 0), "0" => translate("Admin", 0)));

        $form->addInputHidden("user_id", $this->object->getId());
        $form->addInputText("user_name", $this->object->getName(), translate("user name"),
            sprintf(translate("%s chars max"), 16), 16);
        $form->addDropdown("person_id", $personDropdown, translate("person"));

        if ($this->request->getAction() == "new") {
            $form->addInputPassword("password", translate("password"), 32, sprintf(translate("%s chars max"), 32));
        }

        $form->addDropdown("user_class", $userClassDropdown, translate("class"));

        $desc=user::getAccessRightsDescription();

        foreach ($this->object->getAccessRightsArray() as $field => $value) {
            $dropdown=template::createDropdown($field, $value, array(
                "1" => translate("Yes"),
                "0" => translate("No")
            ));
            $form->addDropdown($field, $dropdown, $desc[$field]);
        }

        $lightboxArray = album::getSelectArray();
        $lightboxArray["null"] = "[none]";
        $lightbox=template::createDropdown("lightbox_id", $this->object->get("lightbox_id"), $lightboxArray);
        $form->addDropdown("lightboxid", $lightbox, translate("lightbox album"));

        $tpl->addBlock($form);

        return $tpl;
    }

    public function getTitle() : string {
        if ($this->request->getAction() == "new") {
            return translate("New user");
        } else {
            return parent::getTitle();
        }

    }
}
