<?php
/**
 * Controller for user
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user;

use auth\validator;
use conf\conf;
use generic\controller as genericController;
use securityException;
use template\message;
use web\request;
use user;

/**
 * Controller for user
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewPassword  = view\password::class;
    protected static $viewPrefs     = view\prefs::class;
    protected static $viewRedirect  = \web\view\redirect::class;
    protected static $viewUpdate    = view\update::class;
    protected static $viewUsers     = view\users::class;

    /** @var array Actions that can be used in this controller */
    protected $actions = array("confirm", "delete", "display", "edit", "insert", "new", "prefs", "update", "updateprefs", "users", "password");

    public function doAction(string $action) : void {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $this->setUserAdmin($user, $action);
        } else {
            $this->setUserNonAdmin($user, $action);
        }
        parent::doAction($action);
    }

    private function setUserAdmin(user $user, string $action) : void {
        if (!in_array($action, array("users", "new"))) {
            if (isset($this->request["user_id"])) {
                $user = new user($this->request["user_id"]);
            }
            $user->lookup();
            $user->lookupPrefs();

            if ($user->getId() == -1) {
                $prefs = new prefs(-1);
                if (!$prefs->lookup()) {
                    $prefs->insert();
                    $prefs->load(1);
                }

                $user->set("user_name", translate("Default preferences"));
                $user->prefs=$prefs;
            }

            $this->setObject($user);
        } else if ($action == "new") {
            $this->setObject(new user());
        }
    }

    private function setUserNonAdmin(user $user, string $action) : void {
        if ($user->isDefault()) {
            throw new securityException("Default user cannot change password or preferences");
        } else if (in_array($action, array("password", "update", "prefs", "updateprefs"))) {
            $this->setObject($user);
        } else {
            throw new securityException("Admin user required");
        }
    }

    protected function actionConfirm() {
        parent::actionConfirm();
        $this->view->setRedirect("user/users");
    }

    protected function actionPrefs() {
        $this->view = new static::$viewPrefs($this->request, $this->object);
    }

    /**
     * Action: update
     * The update action processes a form as generated after the "edit" action.
     * The subsequently called view displays the object.
     */
    protected function actionUpdate() {
        $msg=null;
        $password = $this->request["_password"];
        if (!empty($password)) {
            $this->object->set("password", validator::hashPassword($password));
            $this->object->update();
            $msg = new message(message::SUCCESS, translate("The password has been changed"));
        }


        if (user::getCurrent()->isAdmin()) {
            parent::actionUpdate();
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewPassword($this->request, $this->object);
            if (empty($password)) {
                $msg = new message(message::ERROR, translate("The password may not be null"));
            }
        }
        $this->view->setMessage($msg);
    }

    /**
     * Action: update prefs
     */
    protected function actionUpdatePrefs() {
        $prefs = $this->object->prefs;
        $prefs->setFields($this->request->getRequestVars());
        $prefs->update();

        $this->view = new static::$viewPrefs($this->request, $this->object);
        $prefs->load();
        user::getCurrent()->loadLanguage(1);
    }

    protected function actionUsers() {
        $this->view = new static::$viewUsers($this->request);
        $this->view->setObjects(user::getAll("user_name"));
    }

    protected function actionPassword() {
        $this->view = new static::$viewPassword($this->request, $this->object);
    }

}
