<?php
/**
 * Common parts for person view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place\view;

use web\view\viewInterface;
use conf\conf;
use place;
use template\block;
use user;
use web\view\view as webView;
use web\request;

/**
 * This is a base view for the place page, that contains common parts for all views
 */
abstract class view extends webView implements viewInterface {

    /** * @var array request variables */
    protected $vars;

    /**
     * Create view
     */
    public function __construct(protected request $request, protected place $object) {
        $this->vars=$request->getRequestVars();
    }

    public function getScripts() : array {
        $scripts = parent::getScripts();
        $scripts[]="js/json.js";
        $scripts[]="js/locationLookup.js";
        return $scripts;
    }


    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        $editactions = array("settzchilderen", "insert", "update");
        if (in_array($this->request->getAction(), $editactions)) {
            return translate("edit place");
        } else {
            return translate($this->request->getAction() . " place");
        }
    }

}
