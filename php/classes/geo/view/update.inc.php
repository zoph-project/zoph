<?php
/**
 * View for edit tracks
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace geo\view;

use web\view\viewInterface;
use conf\conf;
use geo\track;
use template\actionlink;
use template\block;
use template\form;
use user;

/**
 * This view displays the track page when editing
 */
class update extends view implements viewInterface {

    protected function getActionLinks() : array {
        $param = array("track_id" => (int) $this->object->getId());

        $actionlinks = parent::getActionLinks();
        $actionlinks[] = new actionlink("display", "track", $param);

        return $actionlinks;
    }
    /**
     * Output the view
     */
    public function view() : block {
        if (in_array($this->request->getAction(), array("edit", "update"))) {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = "display";
        }

        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock(new block("track_form", array(
            "action"    => $action,
            "track_id"  => $this->object->getId(),
            "name"      => $this->object->get("name")
        )));

        return $tpl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        if ($this->object->getId()==0) {
            return translate("Add track");
        } else if (trim($this->object->get("subject"))=="") {
            return translate("track");
        } else {
            return $this->object->get("subject");
        }
    }

}
