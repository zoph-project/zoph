<?php
/**
 * Set some basic configuration options during setup
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use app;
use conf\conf;
use template\block;
use template\form;
use template\template;
use web\request;

/**
 * Set configuration options
 */
class config extends view {
    public array $scripts = array("js/conf.js");
    protected array $javascripts = array("window.addEventListener('load', zConf.genSaltAll)");

    /** @var string error to display instead of form */
    private string $error;

    /**
     * View for install
     * @return template\block
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));

        $tpl->addBlock($this->getIcons());


        $form=new form("form", array(
            "class"         => "config",
            "formAction"    => app::getBasePath() . "install/finish",
            "submit"        => "next",
            "onsubmit"      => null
        ));

        $form->addInputHidden("token", $this->request["token"]);

        $items = array(
            "interface.title",
            "path.images",
            "maps.provider",
            "import.enable",
            "import.upload",
            "share.salt.full",
            "share.salt.mid",
            "feature.comments",
            "date.tz",
            "date.guesstz"
        );
        conf::loadFromDB();
        foreach ($items as $item) {
            $config = conf::getItemByName($item);
            $form->addBlock($config->display());
        }


        $tpl->addBlock($form);
        return $tpl;
    }

    public function setError(string $error) {
        $this->error = $error;
    }
}
