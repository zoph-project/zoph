<?php
/**
 * View that shows the generated ini file
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use app;
use conf\conf;
use template\block;
use template\form;
use web\request;

/**
 * Show INI file
 */
class inifile extends view {
    /** @var string ini file */
    private $inifile;

    /**
     * View for install
     * @return template\template
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $tpl->addBlock(new block("text", array(
            "class"     => "install",
            "title"     => "INI file",
            "text"      => array(
                "Please copy the INI file below to <tt>" . INI_FILE . "</tt>. " .
                "If you do not have access to <tt>" . INI_FILE . "</tt>, you can change the location " .
                "by editing <tt>config.inc.php</tt> and modifying the line ",
                "<tt>define('INI_FILE', \"" . INI_FILE . "\");</tt> ",
                "Make sure you <b>do not</b> put this file anywhere within the web root or " .
                "the entire Internet will know your database passwords."
            )
        )));

        $form=new form("form", array(
            "class"         => "inifile",
            "formAction"    => app::getBasePath() . "install/dbcheck",
            "submit"        => "next",
            "onsubmit"      => null
        ));

        $form->addInputHidden("token", $this->request["token"]);
        $form->addTextarea("inifile", $this->inifile, "INI file", 60, 10);

        $tpl->addBlock($form);

        return $tpl;
    }

    public function setInifile(string $inifile) {
        $this->inifile = $inifile;
    }

}
