<?php
/**
 * View for display start of installation
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use app;
use template\block;

/**
 * Display pending migrations
 */
class display extends view {

    /**
     * View for install
     * @return template\template
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $text = new block("text", array(
            "title"     => $this->getTitle(),
            "text"      => $this->getText(),
            "class"     => "install"
        ));

        $text->addBlock(new block("button", array(
            "button"        => "next",
            "button_class"  => "install",
            "button_url"    => app::getBasePath() . "install/requirements?token=" . app::getToken()
        )));
        $tpl->addBlock($text);

        return $tpl;
    }

    private function getText() {
        return array(
            translate("Zoph could not find a working configuration and has therefore started the installation process. If this is incorrect, please verify the configuration and database credentials; otherwise, please start the installation process by clicking next.", false)
        );
    }

    public function getTitle() : string {
        return translate("Welcome to Zoph", false);
    }
}
