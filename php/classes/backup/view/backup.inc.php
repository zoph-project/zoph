<?php
/**
 * View for backup: this sends the backup to the browser
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace backup\view;

use web\view\viewInterface;
use conf\conf;
use template\block;
use web\request;
use web\view\view;

/**
 * Send headers to direct the browser to download the file and send gzip data
 */
class backup extends view implements viewInterface {

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request, private string $object) {
    }

    public function getHeaders() : array {
        $size = strlen($this->object);
        $filename = "backup.sql.gz";
        return array(
            "Content-Description: File Transfer",
            "Content-Type: application/gzip",
            "Content-Disposition: attachment; filename=\"" . $filename . "\"",
            "Content-Transfer-Encoding: binary",
            "Expires: 0",
            "Cache-Control: no-cache",
            "Content-Length: " . $size
        );
    }

    /**
     * Output view
     */
    public function view() : string {
        return $this->object;
    }

    public function display($template = null) : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }
        echo $this->view();
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("Create backup");
    }

}
