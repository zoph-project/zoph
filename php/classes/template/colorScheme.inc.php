<?php
/**
 * A class corresponding to the color_themes table.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

namespace template;

use zophTable;

/**
 * A class corresponding to the color_schemes table.
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */
class colorScheme extends zophTable {

    /** @var string The name of the database table */
    protected static $tableName="color_schemes";
    /** @var array List of primary keys */
    protected static $primaryKeys=array("color_scheme_id");
    /** @var array Fields that may not be empty */
    protected static $notNull=array("name");
    /** @var bool keep keys with insert. In most cases the keys are set by
                  the db with auto_increment */
    protected static $keepKeys = false;
    /** @var string URL for this class */
    protected static $url="colourscheme?color_scheme_id=";

    /** @var colorScheme the currently loaded scheme */
    private static $current=null;


    public function __construct($id=0) {
        parent::__construct($id);
        if (!$id) {
            foreach (self::getDefaults() as $name => $value) {
                $this->set($name, $value);
            }
            $this->removeOctothorpe();
        }
    }

   /**
    * Insert the color scheme in the db
    */
    public function insert() {
        $this->removeOctothorpe();
        parent::insert();
    }

   /**
    * Update the color scheme in the db
    */
    public function update() {
        $this->removeOctothorpe();
        parent::update();
    }

    public function getName() {
        return $this->get("name");
    }

    public function getDisplayArray() {
        $da = parent::getDisplayArray();
        unset($da["Name"]);
        return $da;
    }

    /**
     * Get color from current color scheme
     * or fall back to default
     * @param string Name of color to retrieve
     * @return string #xxxxxx HTML color code
     */
    public static function getColor($color) {
        if (!is_null(static::$current)) {
            return "#" . static::$current->get($color);
        } else {
            return static::getDefault($color);
        }
    }

    /**
     * Get all colours from the current scheme
     * @return array of name => value pairs
     */
    public function getColors() {
        $this->lookup();
        $colors=array();
        foreach ($this->fields as $field => $value) {
            if ($this->isKey($field) || $field=="name") {
                continue;
            }
            $colors[$field]=$this->fields[$field];
        }
        return $colors;
    }


    private function removeOctothorpe() {
        foreach ($this->fields as $field => $value) {
            $this->set($field, str_replace("#", "", $value ?? ""));
        }
    }

    /**
     * Get the default value for a specific  colour
     */
    private static function getDefault(string $colour) : string {
        $cs = self::getDefaults();
        if (array_key_exists($colour, $cs)) {
            return $cs[$colour];
        } else {
            throw new undefinedColourException("Undefined colour: " . e($colour));
        }
    }

    /**
     * Define a default for each colour
     * for now, this is a fallback for whenever no colour scheme has been loaded,
     * e.g. when the user is not logged in yet. Eventually, it will be possible
     * to define a "default" colour scheme, and then this will only be used in
     * a worst case fall back (for example when an admin deletes *all* colour
     * schemes.
     * @return array of name => #xxxxxx HTML color code pairs
     */
    public static function getDefaults() : array {
        return array(
            "page_bg_color"             => "#445577",
            "text_color"                => "#000000",
            "link_color"                => "#111111",
            "vlink_color"               => "#444444",
            "table_bg_color"            => "#eeeeee",
            "table_border_color"        => "#000000",
            "breadcrumb_bg_color"       => "#eeeeee",
            "title_bg_color"            => "#bbbbcc",
            "title_font_color"          => "#333333",
            "tab_bg_color"              => "#667799",
            "tab_font_color"            => "#000000",
            "selected_tab_bg_color"     => "#bbbbcc",
            "selected_tab_font_color"   => "#000000"
        );
    }

    /**
     * Set current color scheme
     * @param colorScheme the color scheme to use
     */
    public static function setCurrent(colorScheme $cs) {
        static::$current=$cs;
    }
}
?>
