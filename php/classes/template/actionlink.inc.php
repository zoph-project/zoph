<?php
/**
 * Actionlink - display a link that does an action (display, edit, etc.)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template;

use app;
use conf\conf;
use exception as templateException;

/**
 * Actionlink template
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class actionlink {

    /**
     * Create actionlink object
     * Needs to be called with either controller, action and parameters set, or with url set.
     * @param string title
     * @param string base url
     * @param array array of additional parameters
     */
    public function __construct(
        private string  $title,
        private ?string $link = null,
        private ?array  $parameters = array(),
        private ?string $url = null,
        private ?array  $titleParams = array()
    ) {
        if (is_null($this->link) && is_null($this->url)) {
            throw new templateException("Actionlink: either link or url must be specified");
        }

    }

    /**
     * Return block
     * @return block template
     */
    public function view() : block {
        if (is_null($this->url)) {
            $this->url = app::getBasePath() . $this->link;
            if (!empty($this->parameters)) {
                $this->url .= "?" . http_build_query($this->parameters);
            }

        } else {
            $this->url = app::getBasePath() . $this->url;
        }

        if (!empty($this->titleParams)) {
            $title = vsprintf(translate($this->title), $this->titleParams);
        } else {
            $title = translate($this->title);
        }

        return new block("actionlink", array(
            "link" => $this->url,
            "text" => $title
        ));
    }

    /**
     * Display message
     * @return string template output
     */
    public function __toString() : string {
        return (string) $this->view();
    }

    public function addParams(array $params) {
        $this->parameters = array_merge($this->parameters, $params);
    }
}
