<?php
/**
 * Controller for template
 * now only used to display CSS, but may be extended to other
 * template-related assets as well.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template;

use conf\conf;
use web\request;
use zoph\app;
use generic\controllerInterface;

/**
 * Controller for template
 */
class controller implements controllerInterface {

    protected static $viewCSS       = view\css::class;
    protected static $viewDefault   = view\view::class;
    protected static $viewVariables = view\variables::class;
    protected static $viewColours   = view\colours::class;

    private $view;
    /**
     * Create a new controller from a web request
     * @param request Request to proces
     */
    public function __construct(protected request $request) {


    }

    /**
     * Do the action as set in the request
     */
    public function doAction(string $action) : void {
        $this->view = match($action) {
            ""              => new static::$viewDefault($this->request, $action),
            "colours.css"   => new static::$viewColours($this->request, $action),
            "variables.css" => new static::$viewVariables($this->request, $action),
            default         => new static::$viewCSS($this->request, $action)
        };
    }

    public function getView() {
        return $this->view;
    }
}
