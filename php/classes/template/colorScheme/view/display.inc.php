<?php
/**
 * View for display colorScheme page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme\view;

use web\view\viewInterface;
use conf\conf;
use template\actionlink;
use template\block;
use template\colorScheme;
use user;
use web\request;

/**
 * This view displays the "colorScheme" page
 */
class display extends view implements viewInterface {
    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        if (user::getCurrent()->isAdmin()) {
            $param = array("color_scheme_id" => (int) $this->object->getId());
            return array(
                new actionlink("edit", "colourscheme/edit", $param),
                new actionlink("copy", "colourscheme/copy", $param),
                new actionlink("delete", "colourscheme/delete", $param),
                new actionlink("new", "colourscheme/new"),
                new actionlink("return", "colourscheme/displayAll")
            );
        } else {
            return array();
        }
    }

    /**
     * Output view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title" =>  $this->getTitle()
        ));

        $tpl->addBlock(new block("displayColorScheme", array(
            "colors"    =>  $this->object->getDisplayArray()
        )));

        $tpl->addActionlinks($this->getActionlinks());

        return $tpl;

    }
}
