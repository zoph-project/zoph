<?php
/**
 * Message, display an informational or error message
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template;

use conf\conf;

/**
 * Message template
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class message {

    /** @var int message::SUCCESS - this message shows a succesful action  */
    public const SUCCESS=0;
    /** @var int message::INFO - this message is informational  */
    public const INFO=1;
    /** @var int message::WARNING - this message gives a warning */
    public const WARNING=2;
    /** @var int message::ERROR - this message displays an error */
    public const ERROR=3;
    /** @var int message::UNDEFINED - this message's status is unknown */
    public const UNDEFINED=99;

    /** @var int store message status */
    private $status=99;
    /** @var string store message text */
    private $msg = "";
    /** @var string store message class */
    private $class = "unknown";
    /** @var string store message title */
    private $title = null;

    /**
     * Create message object
     * @param int status: message::SUCCESS, message::INFO, message::WARNING or message::ERROR
     * @param string message
     */
    public function __construct(int $status, string $msg, string $title = null) {
        $this->status   = $status;
        $this->msg      = $msg;
        $this->title    = $title;
        $this->processStatus();
    }

    /**
     * Return block
     * @return block template
     */
    public function view() : block {
        return new block("message", array(
            "class" => $this->class,
            "title" => $this->title,
            "text"   => $this->msg,
        ));
    }

    /**
     * Display message
     * @return string template output
     */
    public function __toString() : string {
        return (string) $this->view();
    }

    /**
     * Process status, set class
     * this will determine colour and icon
     */
    private function processStatus() : void {
        switch ($this->status) {
            case self::SUCCESS:
                $this->class = "success";
                break;
            case self::INFO:
                $this->class = "info";
                break;
            case self::WARNING:
                $this->class = "warning";
                break;
            case self::ERROR:
                $this->class = "error";
                break;
            default:
                $this->class = "unknown";
                break;
        }
    }

}
