<?php
/**
 * View for css files
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\view;

use settings;
use template\block;
use template\template;
use web\view\viewInterface;
use web\request;
use web\view\view;

use fileNotFoundException;

/**
 * This view displays CSS files
 */
class css extends view implements viewInterface {
    private string $file;

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request, string $action) {
        $this->file = settings::$phpLocation . template::getFile("css", $action);

        if (!file_exists($this->file)) {
            throw new fileNotFoundException("File not found: " . $this->file);
        }
    }

    /**
     * Output view
     */
    public function view() : string {
        return file_get_contents($this->file);
    }

    public function display($template = null) : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }

        echo $this->view();
    }

    public function getHeaders() : array {
        $headers = array();

        $file = $this->file;

        $mtime = filemtime($file);
        $filesize = filesize($file);
        $gmtMtime = gmdate('D, d M Y H:i:s', $mtime) . ' GMT';


        $mod = $this->request->getServerVar("HTTP_IF_MODIFIED_SINCE") ?? "";
        if ($mod == $gmtMtime) {
            $headers[]="HTTP/1.1 304 Not Modified";
        } else {
            $headers[] = "Content-Length: " . $filesize;
            $headers[] = "Content-Disposition: inline; filename=" . $file;
            $headers[] = "Last-Modified: " . $gmtMtime;
            $headers[] = "Content-Type: text/css";
        }
        return $headers;
    }


}
