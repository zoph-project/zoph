<?php
/**
 * Results - display status table for actions
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template;

use conf\conf;

/**
 * Result template
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class result {

    /** @var int result::TODO - this action is pending */
    public const TODO=-1;
    /** @var int result::SUCCESS - this action was executed succesfully  */
    public const SUCCESS=0;
    /** @var int result::INFO - this action was executed succesfully, but gave some log info  */
    public const INFO=1;
    /** @var int result::WARNING - this action was executed succesfully, but gave a warning */
    public const WARNING=2;
    /** @var int result::ERROR - this action encountered an error */
    public const ERROR=3;
    /** @var int result::UNDEFINED - this action's status is unknown */
    public const UNDEFINED=99;

    /** @var int store result */
    private $result=99;
    /** @var string store result message */
    private $msg = "";
    /** @var string store result error */
    private $error = "";
    /** @var string store result icon */
    private $icon = "";
    /** @var string store result status */
    private $status = "unknown";

    /**
     * Create result object
     * @param int result: result::TODO, result::SUCCESS, result::INFO, result::WARNING or result::ERROR
     * @param string result message
     * @param string|null result error
     */
    public function __construct(int $result, string $msg, string $error=null) {
        $this->result = $result;
        $this->msg = $msg;
        $this->error = $error;
        $this->processResult();
    }

    /**
     * Set icon
     * override standard icon
     * @param string icon path - recommend to use template::getImage() for this
     */
    public function setIcon(string $icon) : void {
        $this->icon = $icon;
    }

    /**
     * Display result object
     * @return string template output
     */
    public function __toString() : string {
        return (string) new block("result", array(
            "status"=> $this->status,
            "icon"  => $this->icon,
            "msg"   => $this->msg,
            "error" => $this->error
        ));
    }

    /**
     * Process results, set status and icon
     */
    private function processResult() : void {
        switch ($this->result) {
            case self::TODO:
                $this->status = "todo";
                $this->icon = template::getImage("icons/todo.png");
                break;
            case self::SUCCESS:
                $this->status = "ok";
                $this->icon = template::getImage("icons/ok.png");
                break;
            case self::INFO:
                $this->status = "info";
                $this->icon = template::getImage("icons/info.png");
                break;
            case self::WARNING:
                $this->status = "warning";
                $this->icon = template::getImage("icons/warning.png");
                break;
            case self::ERROR:
                $this->status = "error";
                $this->icon = template::getImage("icons/error.png");
                break;
            default:
                $this->status = "unknown";
                $this->icon = template::getImage("icons/unknown.png");
                break;
        }
    }

    public function getResult() {
        return $this->result;
    }
}
