<?php
/**
 * Generic Controller
 * Handles basic form actions, such as confirm, delete, edit, insert, new and update
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 * @author Jason Geiger
 */

namespace generic;

use breadcrumb;
use exception;
use web\request;
use zophTable;
use app;

/**
 * Generic Controller
 * Handles basic form actions, such as confirm, delete, edit, insert, new and update
 */
abstract class controller implements controllerInterface {

    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewRedirect  = \web\view\redirect::class;
    protected static $viewUpdate    = view\update::class;
    protected static $viewForbidden = view\forbidden::class;

    /** @var zophTable holds object to operate on */
    protected   $object;
    /** @var string where to redirect after action */
    public      $redirect   = "zoph/welcome";
    /** @var array Actions that can be used in this controller */
    protected   $actions    = array("confirm", "delete", "display", "edit", "insert", "new", "update");
    /** @var string view to call after action */
    protected   $view       = null;
    /** @var string action */
    public string $action = "display";

    /**
     * Create a new controller from a web request
     * @param request Request to proces
     */
    public function __construct(protected request $request) {
        if (isset($this->request["_return"])) {
            $this->redirect=$this->request["_return"] . "?" . $this->request->getReturnQueryString();
        }
    }

    /**
     * Set the object to operate on
     * @param zophTable object to operate on
     */
    public function setObject(zophTable $obj) {
        $this->object=$obj;
    }

    /**
     * Do the action as set in the request
     * in the current mode of operation, no authorization checking is needed,
     * because currently, the authorization checking is done inside the actions
     * however, it would be nice to do some checking here as a first line of defense
     * @todo This needs more authorization checking
     */
    public function doAction(string $action) {
        if (in_array($action, $this->actions)) {
            $function = "action" . ucwords($action);
            $this->$function();
        } else {
            $this->actionDisplay();
        }
        if ($this->view?->getTitle() && !app::getMode(app::TEST)) {
            breadcrumb::create($this->view->getTitle(), $this->request);
        }
    }

    /**
     * Action: edit
     * The edit action calls a view that will allow the user to update the
     * current object.
     */
    protected function actionEdit() {
        $this->view = new static::$viewUpdate($this->request, $this->object);

    }

    /**
     * Action: forbidden
     * This action can be called when a user does something that he is not allowed to do
     * this should only happen if a user is, for example, altering the URL manually
     * because in normal program flow no links to "forbidden" parts should be shown
     */
    protected function actionForbidden(exception $e) {
        $this->view = new static::$viewForbidden($this->request, $e);
    }

    /**
     * Action: update
     * The update action processes a form as generated after the "edit" action.
     * The subsequently called view displays the object.
     */
    protected function actionUpdate() {
        $this->object->setFields($this->request->getRequestVars());
        $this->object->update();
        $this->view = new static::$viewUpdate($this->request, $this->object);

    }

    /**
     * Action: new
     * The new action calls a view that displays a form that allows the user
     * to create a new object.
     */
    protected function actionNew() {
        $this->object->setFields($this->request->getRequestVars());
        $this->view = new static::$viewNew($this->request, $this->object);
    }

    /**
     * Action: insert
     * The insert action processes a form as generated after the "new" action.
     * The subsequently called view displays the object.
     */
    protected function actionInsert() {
        $this->object->setFields($this->request->getRequestVars());
        $this->object->insert();
        $this->view = new static::$viewDisplay($this->request, $this->object);

    }

    /**
     * Action: delete
     * The delete action asks for confirmation of a delete of the current object
     */
    protected function actionDelete() {
        $this->view = new static::$viewConfirm($this->request, $this->object);

    }

    /**
     * Action: confirm
     * The confirm action is called when the user confirms the delete
     * this deletes the object and then redirects the user back the the
     * last page he visited before the delete.
     */
    protected function actionConfirm() {
        $this->object->delete();

        breadcrumb::eat();
        $crumb = breadcrumb::getLast();
        if ($crumb instanceof breadcrumb) {
            $this->redirect=urldecode(html_entity_decode($crumb->getURL()));
        }

        $this->view = new static::$viewRedirect($this->request);
    }

    /**
     * The display action displays the object
     */
    protected function actionDisplay() {
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * get View
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Get the object to operate on
     */
    public function getObject() {
        return $this->object;
    }
}
