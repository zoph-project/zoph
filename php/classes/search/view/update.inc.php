<?php
/**
 * View for search page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace search\view;

use web\view\viewInterface;
use search;
use template\actionlink;
use template\block;
use template\form;
use template\template;
use user;
use zoph\app;

/**
 * This view displays the update search page
 */
class update extends view implements viewInterface {

    /**
     * Output view
     */
    public function view() : block {

        $action = match($this->request->getAction()) {
            "edit"  => "update",
            "new"   => "insert",
            default => "display"
        };


        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks(array(
            new actionlink("return", "search")
        ));

        $form=new form("form", array(
            "formAction"        => app::getBasePath() . "search/" . $action,
            "submit"            => translate("submit")
        ));

        $form->addInputHidden("search_id", $this->object->getId());
        $form->addInputHidden("search", $this->object->get("search"));
        $form->addInputText("name", $this->object->getName(), translate("Name"),
            sprintf(translate("%s chars max"), 64), 40);
        if (user::getCurrent()->isAdmin()) {
            $form->addDropdown(
                "owner",
                template::createDropdown("owner", $this->object->get("owner"),
                    template::createSelectArray(user::getRecords("user_name"),
                    array("user_name"))),
                translate("Owner")
            );
            $form->addDropdown(
                "public",
                template::createYesNoDropdown("public", $this->object->get("public")),
                translate("Public")
            );
        }

        $tpl->addBlock($form);
        return $tpl;
    }

}
