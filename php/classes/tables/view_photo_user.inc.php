<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use db\select;
use db\table;
use db\view;

/**
 * This is a class to generate a view in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class view_photo_user extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : view {
        $view = new view("view_photo_user");
        $view->ifNotExists();

        $qry = new select(array("p" => "photos"));
        $qry->addFields(array(
            "photo_id"  => "p.photo_id",
            "user_id"   => "gu.user_id"
        ));
        $qry->join(array("pa" => "photo_albums"), "pa.photo_id = p.photo_id")
            ->join(array("gp" => "group_permissions"), "pa.album_id = gp.album_id")
            ->join(array("gu" => "groups_users"), "gp.group_id = gu.group_id");

        $view->setQuery($qry);

        return $view;
    }
}
