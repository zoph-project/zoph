<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use db\column;
use db\create;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class photo_relations extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("photo_relations");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("photo_id_1"))->int()->notNull()->setPrimaryKey()->default(0),
            (new column("photo_id_2"))->int()->notNull()->setPrimaryKey()->default(0),
            (new column("desc_1"))->char(128)->default("NULL"),
            (new column("desc_2"))->char(128)->default("NULL")
        ));

        return $table;
    }
}
