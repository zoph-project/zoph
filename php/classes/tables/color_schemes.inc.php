<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use PDO;
use db\clause;
use db\column;
use db\create;
use db\insert;
use db\param;
use db\select;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class color_schemes extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("color_schemes");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("color_scheme_id"))->int()->notNull()->autoIncrement()->setPrimaryKey(),
            (new column("name"))->varchar(64)->notNull()->default(''),
            (new column("page_bg_color"))->varchar(6)->default("NULL"),
            (new column("text_color"))->varchar(6)->default("NULL"),
            (new column("link_color"))->varchar(6)->default("NULL"),
            (new column("vlink_color"))->varchar(6)->default("NULL"),
            (new column("table_bg_color"))->varchar(6)->default("NULL"),
            (new column("table_border_color"))->varchar(6)->default("NULL"),
            (new column("breadcrumb_bg_color"))->varchar(6)->default("NULL"),
            (new column("title_bg_color"))->varchar(6)->default("NULL"),
            (new column("tab_bg_color"))->varchar(6)->default("NULL"),
            (new column("tab_font_color"))->varchar(6)->default("NULL"),
            (new column("selected_tab_bg_color"))->varchar(6)->default("NULL"),
            (new column("selected_tab_font_color"))->varchar(6)->default("NULL"),
            (new column("title_font_color"))->varchar(6)->default("NULL")
        ));

        return $table;
    }

    protected function data() : array {
        $return = array();

        $fields = array("name", "page_bg_color", "text_color", "link_color", "vlink_color", "table_bg_color", "table_border_color", "breadcrumb_bg_color", "title_bg_color", "tab_bg_color", "tab_font_color", "selected_tab_bg_color", "selected_tab_font_color", "title_font_color");

        $colourSchemes = array(
            array('default','445577','000000','111111','444444','eeeeee','000000','eeeeee','bbbbcc','333333','667799','000000','bbbbcc','000000'),
            array('blugram','909090','000000','111111','333333','eef0f0','000000','cce0e0','dde0cc','ccd0bb','000000','bbd0d0','000000','000000'),
            array('dow','444444','000000','000055','000033','cccccc','000000','aaaaaa','2222aa','2222aa','ffffff','cccccc','000000','ffffff'),
            array('hoenig','FFEFD6','5C1F00','330000','330000','FFFBF5','000000','FFF7EB','FFE7C2','FFE7C2','5C1F00','FFD799','000000','993300'),
            array('forest','336633','000000','000000','000000','99CC99','000000','669966','663300','663300','E0E0E0','996633','FFFFFF','99CC99'),
            array('black','000000','FFFFFF','FFFFFF','FFFFFF','000000','FFFFFF','000000','666666','666666','FFFFFF','999999','FFFFFF','FFFFFF'),
            array('beach','646D7E','000000','000000','000000','F9EEE2','000000','9AADC7','C6DEFF','617C58','D0D0D0','8BB381','000000','646D7E'),
            array('steel','cccccc','000000','0000aa','bb0000','eeeeef','222222','6699dd','dddddf','99bbdd','444444','0000bb','bbbbbb','555555'),
            array('happy','16c0ff','333333','444444','444444','ffff77','333333','ffff77','e43f7d','e43f7d','333333','e43f7d','333333','333333'),
            array('grey','eeeeee','111111','222222','555555','f4f4f4','111111','dddddd','dfdfdf','aaaaaa','222222','888888','111111','131313'),
            array('grey - blue','eeeeee','111111','222222','555555','f4f4f4','000000','dddddd','dfdfdf','0000ff','eeeeee','000077','ffffff','131313'),
            array('grey - green','eeeeee','111111','222222','555555','f4f4f4','000000','dddddd','dfdfdf','00dd00','eeeeee','007700','ffffff','131313'),
            array('grey - red','eeeeee','111111','222222','555555','f4f4f4','000000','dddddd','dfdfdf','ff0000','eeeeee','770000','ffffff','131313'),
        );

        foreach ($colourSchemes as $cs) {
            $qry = new select("color_schemes");
            $qry->addFunction(array("count" => "COUNT(*)"));
            $qry->where(new clause("name=:name"));
            $qry->addParam(new param(":name", $cs[0], PDO::PARAM_STR));

            if ($qry->getCount() > 0) {
                continue;
            }
            $cs = array_combine($fields, $cs);
            $qry = new insert("color_schemes");
            foreach ($cs as $field => $value) {
                $qry->addParam(new param(":" . $field, $value, PDO::PARAM_STR));
            }
            $return[] = $qry;
        }

        return $return;
    }
}
