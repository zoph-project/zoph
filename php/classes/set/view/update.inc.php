<?php
/**
 * View for edit set page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use conf\conf;
use template\actionlink;
use template\block;
use template\form;
use web\request;

use set\model as set;
use user;

/**
 * This view displays the set page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {
        $param = array("set_id" => $this->object->getId());

        if ($this->object->getId() != 0) {
            $actionlinks = array(
                new actionlink("return", "set", $param)
            );
        } else {
            $actionlinks = array(
                new actionlink("return", "set/sets")
            );
        }

        if ($this->request->getAction() != "new") {
            $actionlinks[] = new actionlink("new", "set/new");
            $actionlinks[] = new actionlink("delete", "set/delete", $param);
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        if ($this->request->getAction() == "new") {
            $action = "insert";
        } else if (in_array($this->request->getAction(), array("edit", "update", "insert"))) {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = "display";
        }
        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $form = new form("form", array(
            "formAction"    => $this->request->getBasePath() . "set/" . $action,
            "class"         => "set",
            "onsubmit"      => null,
            "submit"        => translate($action, 0)
        ));


        $form->addInputHidden("set_id", $this->object->getId());
        $form->addInputText("name", $this->object->get("name"), translate("name"));

        $tpl->addBlock($form);

        return $tpl;

    }

}
