<?php
/**
 * View for display set
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use web\view\viewInterface;
use selection;
use template\template;
use template\actionlink;
use template\block;
use set\model as set;
use user;
use photoNoSelectionException;
use zoph\app;

/**
 * This view displays set
 */
class display extends view implements viewInterface {

    public function getActionlinks() : array {
        $param = array("set_id" => (int) $this->object->getId());
        return array(
            new actionlink("new", "set/new"),
            new actionlink("edit", "set/edit", $param),
            new actionlink("delete", "set/delete", $param),
            new actionlink("return", "set/sets")
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
            "selection"         => $this->getSelection()
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock(new block("definitionlist", array(
            "class" => "display set",
            "dl"    => $this->object->getDisplayArray()
        )));

        $pc = $this->object->getPhotoCount();
        $sort = "&_order=set_order";
        $tpl->addBlock(new block("photoCount", array(
            "photolinks" => array(array(
                "icon"  => template::getImage("icons/photobig.png"),
                "count" => $pc,
                "url"   => app::getBasePath() . "photos?set_id=" . $this->object->getId() . $sort,
                "desc"  => translate("photo" . ($pc == 1 ? "" :"s"))
            ))
        )));


        return $tpl;
    }

    private function getSelection() : ?selection {
        $selection=null;

        $setId = array(
            "set_id" => $this->object->getId()
        );

        if (user::getCurrent()->canEditOrganisers()) {
            try {
                $selection=new selection(
                    array(
                        new actionlink("coverphoto", "set/coverphoto", $setId),
                        new actionlink("add to set", "set/addphoto", $setId)
                    ),
                    "set?_qs=set_id=" . $this->object->getId(),
                    field: array("coverphoto", "photo_id")
                );

            } catch (photoNoSelectionException $e) {
                $selection=null;
            }
        }

        return $selection;
    }

}
