<?php
/**
 * View for edit page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace page\view;

use web\view\viewInterface;
use conf\conf;
use template\actionlink;
use template\block;
use template\form;
use web\request;
use zophCode\smiley;
use zoph\app;

/**
 * This view displays the page page when editing
 */
class update extends view implements viewInterface {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        if ($this->object->getId() != 0) {
            $param = array("page_id" => $this->object->getId());
            $actionlinks=array(
                new actionlink("return", "page", $param),
                new actionlink("delete", "page/delete", $param)

            );
        } else {
            $actionlinks=array(
                new actionlink("return", "page/pages")
            );
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        if ($this->request->getAction() == "new") {
            $action = "insert";
        } else if (in_array($this->request->getAction(), array("edit", "update"))) {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = "display";
        }

        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $form = new form("form", array(
            "formAction"    => app::getBasePath() . "page/" . $action,
            "class"         => "page",
            "onsubmit"      => null,
            "submit"        => translate($action, 0)
        ));


        $form->addInputHidden("page_id", $this->object->getId());
        $form->addInputText("title", $this->object->get("title"), translate("title"));
        $form->addTextarea("text", trim($this->object->get("text")), translate("text"));

        $tpl->addBlock($form);

        $tpl->addBlock(smiley::getOverview(true));

        return $tpl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        if ($this->object->getId()==0) {
            return translate("Add page");
        } else if (trim($this->object->get("title"))=="") {
            return translate("page");
        } else {
            return $this->object->get("title");
        }
    }
}
