<?php
/**
 * View to display people
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use web\view\viewInterface;
use circle;
use conf\conf;
use person;
use photoNoSelectionException;
use selection;
use template\actionlink;
use template\block;
use web\request;
use web\view\view as webView;
use user;

/**
 * Display screen for circle
 */
class people extends webView implements viewInterface {

    public function __construct(protected request $request, protected ?circle $object = null) {
    }


    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user = user::getCurrent();


        if ($user->canEditOrganisers()) {
            $actionlinks = array(
                new actionlink("new circle", "circle/new"),
                new actionlink("new person", "person/new"),
            );

            if (isset($this->object)) {
                $param = array("circle_id" => $this->object->getId());
                $actionlinks[] = new actionlink("edit circle", "circle/edit", $param);
                $actionlinks[] = new actionlink("delete circle", "circle/delete", $param);


                if ($this->object?->get("coverphoto")) {
                    $param["coverphoto"] = "NULL";
                    $actionlinks[] = new actionlink("unsetcoverphoto", "circle/update", $param);
                }
            }

        } else {
            $actionlinks = array();
        }

        if (!isset($circle) && ($user->canSeeHiddenCircles())) {
            if ($this->request["_showhidden"]) {
                $actionlinks[] = new actionlink("hide hidden", "person/people", array("_showhidden" => 0));
            } else {
                $actionlinks[] = new actionlink("show hidden", "person/people", array("_showhidden" => 1));
            }
        }

        $actionlinks[]= new actionlink("return", "person/people");
        return $actionlinks;
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $user = user::getCurrent();
        $hidden = array(
            "_action"    => "people",
            "_showhidden" => (bool) $this->request["_showhidden"]
        );
        if ($this->object) {
            $hidden["circle_id"] = $this->object->getId();
        }

        $tpl=new block("organiser", array(
            "pageTop"       => false,
            "pageBottom"    => false,
            "showMain"      => true,
            "title"         => strtolower($this->getTitle()),
            "ancLinks"      => null,
            "coverphoto"    => null,
            "description"   => null,
            "selection"     => $this->getSelection(),
            "view"          => $this->request["_view"],
            "view_name"     => "People view",
            "view_hidden"   => $hidden,
            "autothumb"     => $this->request["_autothumb"] ?? $user->prefs->get("autothumb")
        ));

        $tpl->addActionlinks($this->getActionlinks());
        $tpl->addBlock(new block("people_letters", array(
            "l"    => $this->request["_l"] ?? "all"
        )));

        if (!isset($this->object)) {
            $circles = $this->getCircles();
            if ($circles) {
                $tpl->addBlock($this->getPeopleCircleBlock("circles", $circles));
            }
        }

        $people = $this->getPeople();
        if ($people) {
            $tpl->addBlock($this->getPeopleCircleBlock("people", $people));
        }

        if (!$people && !isset($circles) && !isset($this->object)) {
            $tpl->addBlock(new block("message", array(
                "class" => "error",
                "text"  => $this->getErrorMsg()
            )));
        }

        return $tpl;
    }

    private function getPeopleCircleBlock(string $id, array $items) : block {
        $user = user::getCurrent();
        return new block("view_" . ($this->request["_view"] ?? $user->prefs->get("view")), array(
            "id" => $id,
            "items" => $items,
            "autothumb" => $this->request["_autothumb"],
            "links" => array(
                translate("photos of") => "photos?person_id=",
                translate("photos by") => "photos?photographer_id="
            )
        ));
    }


    private function getSelection() : ?selection {
        if (!$this->object) {
            return null;
        }
        $circleId = array(
            "circle_id" => $this->object->getId()
        );

        try {
            $selection=new selection(
                array(new actionlink("coverphoto", "circle/update", $circleId)),
                "circle?_qs=circle_id=" . $this->object->getId(),
                field: "coverphoto"
            );
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }
        return $selection;
    }

    public function getTitle() : string {
        return $this->object?->getName() ?? translate("People");
    }

    private function getFirstLetter() : ?string {
        $letter = $this->request["_l"] ?? "all";
        return match($letter) {
            "all"           => null,
            "no last name"  => "",
            default         => $letter
        };
    }

    private function getErrorMsg() : ?string {
        $letter = $this->request["_l"] ?? "all";
        return match($letter) {
            "all"           => translate("No people were found"),
            "no last name"  => translate("No people with no last name were found"),
            default         => sprintf(translate("No people were found with a last name beginning with '%s'."), htmlentities($letter))
        };
    }

    private function getCircles() : ?array {
        if (!$this->getFirstLetter()) {
            return circle::getAll($this->request["_showhidden"]);
        }
        return null;
    }

    private function getPeople() : array {
        if (isset($this->object)) {
            $people=array();
            $members=$this->object->getMembers();
            foreach ($members as $member) {
                $member->lookup();
                $people[]=$member;
            }
            return $people;
        } else if (!$this->getFirstLetter()) {
            return person::getAllNoCircle();
        } else {
            return person::getAllPeopleAndPhotographers($this->getFirstLetter());
        }
    }
}
