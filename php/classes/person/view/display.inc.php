<?php
/**
 * View for display photo page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use conf\conf;
use person;
use selection;
use template\template;
use template\actionlink;
use template\block;
use user;
use web\request;
use zoph\app;

use photoNoSelectionException;
use pageException;

/**
 * This view displays the "person" page
 */
class display extends view {

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();

        if ($user->canEditOrganisers()) {
            $param = array("person_id" => (int) $this->object->getId());
            $actionlinks=array(
                new actionlink("edit", "person/edit", $param),
                new actionlink("delete", "person/delete", $param),
                new actionlink("new", "person/new")
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks[] = new actionlink("unset coverphoto", "person/unsetcoverphoto", $param);
            }
        }
        return $actionlinks ?? array();

    }

    /**
     * Output view
     */
    public function view() : block {
        $user=user::getCurrent();


        $selection=$this->getSelection();


        try {
            $page=$this->object->getPage($this->request, $this->request["pagenum"]);
            $showOrig=$this->object->showOrig($this->request["pagenum"]);
        } catch (pageException $e) {
            $showOrig=true;
            $page=null;
        }

        $mainActionlinks=array();

        $tpl=new block("display", array(
            "title"             => $this->getTitle(),
            "obj"               => $this->object,
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => $mainActionlinks,
            "selection"         => $selection,
            "page"              => $page,
            "pageTop"           => $this->object->showPageOnTop(),
            "pageBottom"        => $this->object->showPageOnBottom(),
            "showMain"          => $showOrig
        ));

        $tpl->addBlock($this->getPhotoCount());

        if ($user->canSeePeopleDetails()) {
            $dl=$this->object->getDisplayArray();

            $dl = array_merge($dl, $this->getLocationLinks());
        }
        if ($this->object->get("notes")) {
            $dl[translate("notes")]=$this->object->get("notes");
        }

        $dl = array_merge($dl ?? array(), $this->getCircles());
        $tpl->addBlock(new block("definitionlist", array(
            "dl"    => $dl,
            "class" => ""
        )));
        return $tpl;
    }

    private function getPhotoCount() : block {
        $sortorder = $this->object->get("sortorder");
        $sort = $sortorder ? "&_order=" . $sortorder : "";
        $photosOf = $this->object->getPhotoCount();
        $photosBy = $this->object->getPhotographer()->getPhotoCount();

        return new block("photoCount", array(
            "photolinks"    => array(
                array(
                    "icon"  => template::getImage("icons/photobig.png"),
                    "count" => $photosOf,
                    "url"   => app::getBasePath() . "photos?person_id=" . $this->object->getId() . $sort,
                    "desc"  => translate("photos of")
                ), array(
                    "icon"  => template::getImage("icons/camera.png"),
                    "count" => $photosBy,
                    "url"   => app::getBasePath() . "photos?photographer_id=" . $this->object->getId() . $sort,
                    "desc"  => translate("photos by")
                )
            )
        ));
    }

    private function getSelection() : ?selection {
        $selection=null;
        if (user::getCurrent()->canEditOrganisers()) {
            $personId = array(
                "person_id" => $this->object->getId()
            );

            try {
                $selection=new selection(
                    array(
                        new actionlink("coverphoto", "person/coverphoto", $personId)
                    ),
                    "person?_qs=person_id=" . $this->object->getId(),
                    field: "coverphoto"
                );

            } catch (photoNoSelectionException $e) {
                $selection=null;
            }
        }

        return $selection;
    }

    private function getCircles() : array {
        $circles=$this->object->getCircles();
        $circleLinks=array();
        if ($circles) {
            foreach ($circles as $circle) {
                $circle->lookup();
                $circleLinks[]= new block("link", array(
                                    "href"      => $circle->getURL(),
                                    "target"    => "",
                                    "link"      => $circle->getName()
                                ));
            }
        }
        if (!empty($circleLinks)) {
            return array(
                translate("circles") => implode(", ", $circleLinks)
            );
        } else {
            return array();
        }
    }

    /**
     * Get links to each of the locations
     * @todo All the link blocks here could be generated by the objects themselves
     *       saving a huge amount of more-or-less duplicate code
     */
    private function getLocationLinks() : array {
        $dl = array();
        if ($this->object->getEmail()) {
            $mail=new block("link", array(
                "href"      => "mailto:" . e($this->object->getEmail()),
                "target"    =>  "",
                "link"      => e($this->object->getEmail())
            ));
            $dl[translate("email")]=$mail;
        }

        if ($this->object->home) {
            $home=new block("link", array(
                "href"      => app::getBasePath() . "place?place_id=" . $this->object->get("home_id"),
                "target"    => "",
                "link"      => $this->object->home->get("title")
            ));
            $dl[translate("home location")]=$home;
        }

        if ($this->object->work) {
            $home=new block("link", array(
                "href"      => app::getBasePath() . "place?place_id=" . $this->object->get("work_id"),
                "target"    => "",
                "link"      => $this->object->work->get("title")
            ));
            $dl[translate("work location")]=$home;
        }
        return $dl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return $this->object->getName();
    }

}
