<?php
/**
 * View for edit photo page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use album;
use category;
use conf\conf;
use geo\map;
use pageset;
use person;
use photographer;
use place;
use template\actionlink;
use template\block;
use template\template;
use user;
use web\request;
use zoph\app;

/**
 * This view displays the person page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : ?array {
        if ($this->request->getAction() == "new") {
            $actionlinks = array(
                new actionlink("return", "person/people")
            );
        } else {
            $actionlinks = array(
                new actionlink("new", "person/new"),
                new actionlink("return", "person", array("person_id" => $this->object->getId()))
            );
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        if ($this->request->getAction() == "new") {
            $submit = "insert";
        } else if ($this->request->getAction() == "edit") {
            $submit = "update";
        }
        $action = app::getBasePath() . "person/" . $submit;

        $circles = new block("formFieldsetAddRemove", array(
            "legend"    => translate("circles"),
            "objects"   => $this->object->getCircles(),
            "dropdown"  => $this->object->getCircleDropdown("_circle")
        ));

        return new block("editPerson", array(
            "actionlinks"   => $this->getActionlinks(),
            "action"    => $action,
            "person"    => $this->object,
            "title"     => $this->getTitle(),
            "circles"   => $circles,
            "pagesets"  => template::createSelectArray(pageset::getAll("title"), array("title"), true),
            "submit"    => translate($submit, 0)
        ));
    }

}
