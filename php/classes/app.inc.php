<?php
/**
 * App class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

use auth\web as auth;
use generic\controllerInterface;
use requirements\check;
use router\routerInterface;
use web\request;
use web\session;
use web\view\viewInterface;

use db\exception as databaseException;

/**
 * App class
 */
abstract class app {
    private auth $auth;
    public ?viewInterface $view;
    public controllerInterface $controller;
    private ?user $user;
    protected static app $app;
    protected static session $session;
    private static routerInterface $router;
    protected static int $mode = self::RUN;
    public static string $page;
    public const RUN = 0;
    public const CLI = 1;
    public const LOGIN = 2;
    public const INSTALL = 4;
    public const TEST = 8;

    protected static $viewFailedRequirements    = \requirements\view\failed::class;
    protected static $viewRedirect              = \web\view\redirect::class;


   /**
    * Create app
    */
    public function __construct(private request $request) {
        static::$router = $this->getRouter($this->request);
        static::$page = $request->getController();

        $action = $request->getAction();
        if (!empty($action)) {
            static::$page .= "/" . $action;
        }

        $this->controller = static::$router->controller;
        if ($this->controller instanceof install\controller) {
            self::setMode(self::INSTALL);
        }
        if ($this->controller instanceof zoph\controller && static::$router->action == "login") {
            self::setMode(self::LOGIN);
        }
        if (self::getMode(self::TEST)) {
            return;
        }

       /**
        * This code takes care of logging in or redirecting to install
        * this cannot be executed by PHP Unit, hence the return above
        * the code called by these lines is tested elsewhere
        */
        // @codeCoverageIgnoreStart

        try {
            self::$session = new session();
            $this->loadSettings();
            self::$session->start();
            $this->auth();
            $this->breadcrumbs();
        } catch (configurationException | databaseException $e) {
            if (!self::getMode(self::INSTALL)) {
                self::setMode(self::INSTALL);
                if (!$this->controller instanceof template\controller) {
                    $this->view = new static::$viewRedirect($this->request);
                    $this->view->setRedirect("install", "Run install script");
                }
            }
        }

        if (!$this->controller instanceof template\controller) {
            $this->checkRequirements();
        }

        // @codeCoverageIgnoreEnd

    }

   /**
    * Get the current execution mode
    * return whether a mode is set
    */
    public static function getMode(int $mode) : bool {
        return (bool) (static::$mode & $mode);
    }

   /**
    * Set the current execution mode
    * multiple modes can be active at the same time, so logically combine the
    * new mode with the existing modes.
    */
    public static function setMode(int $mode) {
        static::$mode |= $mode;
    }

   /**
    * Unset an execution mode
    * multiple modes can be active at the same time, so logically substract the
    * given mode from the existing modes.
    */
    public static function unsetMode(int $mode) : void {
        static::$mode &= ~$mode;
    }

   /**
    * Start authentication process.
    * @codeCoverageIgnore
    */
    private function auth() {
        $this->auth = new auth(self::$session, $this->request);
        $this->user = $this->auth->getUser();
        if ($this->auth->getRedirect() && self::$mode == self::RUN) {
            $this->view = new static::$viewRedirect($this->request);
            $this->view->setRedirect($this->auth->getRedirect());
        }
        if (!$this->user) {
            static::setMode(self::LOGIN);
        }
    }

   /**
    * Run the application
    */
    public function run() {
        if (!isset($this->view)) {
            $this->controller->doAction(static::$router->action);
            $this->view = $this->controller->getView();
        }
    }

   /**
    * Start logout process.
    * @codeCoverageIgnore
    */
    public static function logout(request $request) : void {
        $auth = new auth(self::$session, $request);
        $auth->logout();
    }

   /**
    * Get the basepath, needed if Zoph is installed in a subdir
    */
    public static function getBasePath() : string {
        return static::$router->basepath;
    }

   /**
    * Register breadcrumb, handle clearing or 'eat'ing breadcrumbs
    * @codeCoverageIgnore
    */
    private function breadcrumbs() : void {
        breadcrumb::init($this->request);

        if ($this->request["_clear_crumbs"]) {
            breadcrumb::eat(0);
        } else if ($this->request["_crumb"]) {
            breadcrumb::eat($this->request["_crumb"]);
        }
    }


    private function loadSettings() : void {
        if (!static::getMode(self::CLI)) {
            $i=settings::loadINI();
            settings::parseINI($i);
        }
    }

   /**
    * Load settings for unittest
    * This cannot use autodetection of PHP location
    * because the code is executed from PHPUnit context
    */
    public static function loadSettingsTest(string $instance) : void {
        $i=settings::loadINI($instance);
        settings::parseINI($i, test: true);
    }

    public function checkRequirements() {
        $requirements = new check();
        $requirements->doChecks();
        if (!$requirements->getStatus()) {
            $this->view=new static::$viewFailedRequirements($this->request);
            $this->view->addRequirements($requirements);
        }

    }

    /**
     * Set a token that can be used to prevent CSRF attacks
     * @return the token
     */
    public static function setToken() : string {
        $session = session::getCurrent();
        $session["token"] = hash("sha256", random_bytes(64));
        return $session["token"];
    }

    /**
     * Get the token, if it's not set, return a random token
     */
    public static function getToken() : string {
        $session = session::getCurrent();
        return $session["token"] ?? hash("sha256", random_bytes(64));
    }

    /**
     * Check a given token against the token that is set in the session cookie
     */
    public static function checkToken(string $token) : bool {
        return (!empty($token) && (self::getToken() == $token));
    }


    abstract protected function getRouter(request $request) : routerInterface;
}
