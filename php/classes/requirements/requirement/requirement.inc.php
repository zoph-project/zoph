<?php
/**
 * Requirement class (abstract)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use template\block;
use template\template;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
abstract class requirement {

    protected const NAME = "";
    protected const DESCRIPTION = "";

    protected const PASS = true;
    protected const FAIL = false;
    protected const WARNING = null;

    protected const MSG_PASS = "OK";
    protected const MSG_FAIL = "FAIL";
    protected const MSG_WARNING = "FAIL";
    protected const MSG_UNKNOWN = "An error has occured while checking the requirement. This is probably a bug.";

    protected $result = self::FAIL;

    abstract protected function check();

    final public function doCheck() {
        $this->result = $this->check();
    }

    final public function getResult() {
        return $this->result;
    }

    /**
     * Display the requirements overview
     */
    final public function __toString() {
        if ($this->result === self::PASS) {
            $icon = template::getImage("icons/ok.png");
            $status = "reqpass";
            $msg = static::MSG_PASS;
        } else if ($this->result ===  self::FAIL) {
            $icon = template::getImage("icons/error.png");
            $status = "reqfail";
            $msg = static::MSG_FAIL;
        } else if ($this->result ===  self::WARNING) {
            $icon = template::getImage("icons/warning.png");
            $status = "reqwarn";
            $msg = static::MSG_WARNING;
        } else {
            $icon = template::getImage("icons/unknown.png");
            $status = "requnknown";
            $msg = static::MSG_UNKNOWN;
        }

        $tpl=new block("requirement", array(
            "status"    => $status,
            "icon"      => $icon,
            "name"      => static::NAME,
            "desc"      => static::DESCRIPTION,
            "msg"       => $msg
        ));
        return (string) $tpl;
    }

}


