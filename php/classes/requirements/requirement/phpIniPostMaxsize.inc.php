<?php
/**
 * Requirement class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use conf\conf;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class phpIniPostMaxsize extends phpIniCheck {

    protected const NAME = "php.ini: post_max_size";
    protected const DESCRIPTION = "Maximum size to for POST requests (relevant for uploads)";
    protected const MSG_PASS = "Max POST size sufficient.";
    protected const MSG_FAIL = "post_max_size should be 8M or more (16M or more recommended) and be larger than upload_max_filesize";
    protected const MSG_WARNING = "post_max_size is set to less than 16M, this may not be sufficient when uploading large photos or archives.";

    protected function check() {
        $postsize = $this->toBytes(ini_get("post_max_size"));
        $filesize = $this->toBytes(ini_get("upload_max_filesize"));
        if ($postsize < (8 * 1024 * 1024) || $postsize < $filesize) {
            return static::FAIL;
        } else if ($postsize < (16 * 1024 * 1024)) {
            return static::WARNING;
        } else {
            return static::PASS;
        }
    }

}
?>
