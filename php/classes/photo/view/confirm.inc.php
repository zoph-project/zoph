<?php
/**
 * View for confirm delete photo
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use web\view\viewInterface;
use photo;
use template\actionlink;
use template\block;
use user;
use web\request;

/**
 * This view displays the "confirm delete photo" page
 */
class confirm extends view implements viewInterface {

    public function getActionlinks() : array {
        $qs = $this->request->getReturnQueryString();
        $confirm = array(
            "photo_id"  => $this->photo->getId(),
            "_qs"       => urlencode($qs)
        );
        $cancel = "photo";
        if (!empty($qs)) {
            $cancel .= "?" . $qs;
        }
        return array(
            new actionlink("confirm", "photo/confirm", $confirm),
            new actionlink("cancel", $cancel)
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"             =>  translate("delete photo"),
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => null,
            "obj"               => $this->photo,
            "image"             => $this->photo->getImageTag(MID_PREFIX)
        ));

    }

    public function getTitle() : string {
        return translate("delete photo");
    }
}
