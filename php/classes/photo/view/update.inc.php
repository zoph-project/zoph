<?php
/**
 * View for edit photo page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use album;
use category;
use conf\conf;
use geo\map;
use person;
use photographer;
use place;
use template\actionlink;
use template\block;
use template\template;
use user;
use web\request;

/**
 * This view displays the "photo" page when editing a photo
 */
class update extends view {

    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {
        $actionlinks=parent::getActionLinks();
        $user=user::getCurrent();
        unset($actionlinks["edit"]);

        $url = "photo";
        $qs = $this->request->getReturnQueryString();
        if (!empty($qs)) {
            $url .= "?" . $qs;
        }

        if (!$user->prefs->get("auto_edit")) {
            $actionlinks[] = new actionlink("return", $url);
        } else {
            $actionlinks[] = new actionlink("display", $url);
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        $user = user::getCurrent();
        $photo = $this->photo;
        $warning = "";
        $autocomppeople = "true";

        if (!person::getAutocompPref()) {
            $warning = new block("message", array(
                "class" => "warning",
                "text" => translate("Autocomplete for people is needed to add people to a photo")
            ));
            $autocomppeople = "false";
        }

        $this->setLinks();

        if (conf::get("maps.provider")) {
            $map=new map();
            $map->setEditable();
            $map->setCenterAndZoomFromObj($photo);
            $map->addMarkers(array($photo), $user);
        }

        $prevnext = new block("photoPrevNext", array(
            "prev"          => $this->prevURL,
            "up"            => $this->upURL,
            "next"          => $this->nextURL
        ));

        return new block("editPhoto", array(
            "warning"           => $warning,
            "autocomppeople"    => $autocomppeople,
            "photo"             => $photo,
            "title"             => $this->getTitle(),
            "selection"         => $this->getSelection(),
            "admin"             => (bool) $user->isAdmin(),
            "actionlinks"       => $this->getActionlinks(),
            "return_qs"         => $this->request->getReturnQueryString(),
            "rotate"            => conf::get("rotate.enable"),
            "prevnext"          => $prevnext,
            "full"              => $photo->getFullsizeLink($photo->get("name")),
            "size"              => template::getHumanReadableBytes((int) $photo->get("size")),
            "share"             => $this->getShare(),
            "image"             => $photo->getFullsizeLink($photo->getImageTag(MID_PREFIX)),
            "albums"            => $photo->getAlbums($user),
            "categories"        => $photo->getCategories($user),
            "locDropdown"       => place::createDropdown("location_id", $photo->get("location_id")),
            "pgDropdown"        => photographer::createDropdown("photographer_id", $photo->get("photographer_id")),
            "albumDropdown"     => album::createDropdown("_album_id[0]"),
            "catDropdown"       => category::createDropdown("_category_id[0]", ""),
            "zoomDropdown"      => place::createZoomDropdown($photo->get("mapzoom")),
            "show"              => $this->request["_show"],
            "map"               => $map ?? null
        ));
    }
}
