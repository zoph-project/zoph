<?php
/**
 * Process EXIF information
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jason Geiger
 * @author Jeroen Roos
 * @author Matthias Wandel
 *
 * @package Zoph
 */

namespace photo;

use file;
use photo;
use template\block;

/**
 * Uses PHP's read_exif_data() function to parse EXIF headers.
 *
 * I've tried to match some of the normalizations/conversions done by jhead
 * (some code borrowed from exif.c). (License from jhead explicitly allows this).
 *
 * @author Jason Geiger
 * @author Jeroen Roos
 * @author Matthias Wandel
 */
class exif {
    private file $file;
    public array $data = array();
    private array $exif = array();

    public function __construct(file|photo $file) {
        $exif = false;

        if ($file instanceof photo) {
            $this->file = new file($file->getFilePath());
        } else {
            $this->file = $file;
        }

        if ($this->file->getMime() == "image/jpeg") {
            getimagesize($this->file, $info);
            if (isset($info["APP1"]) && substr($info["APP1"], 0, 4) == "Exif") {
                $exif = exif_read_data($this->file);
            }
        }
        if ($exif !== false) {
            $this->exif = $exif;
        }
    }

    public function process() : void {
        if (empty($this->exif)) {
            echo "<b>" . basename($this->file) . "</b>" . ": ";
            echo translate("No EXIF header found.") . "<br>\n";

            // Set date and time to file date/time
            list($this->data["date"], $this->data["time"])= explode(" ", date("Y-m-d H:i:s", filemtime($this->file)));
        } else {
            $this->processDateTime();
            $this->processMakeModel();
            $this->processFlash();
            $this->processFocalLength();
            $this->processExposure();
            $this->processAperture();
            $this->processMeteringMode();
            $this->processISO();
            $this->processCompression();
            $this->processComment();


            if (isset($this->exif["GPSLatitudeRef"]) && isset($this->exif["GPSLatitude"]) &&
                isset($this->exif["GPSLongitudeRef"]) && isset($this->exif["GPSLongitude"])) {
                    $this->processGPSData();
            }
        }
    }

    /**
     * Process Date/Time information from the EXIF header,
     * or, when it's not available, the file date/time
     */
    private function processDateTime() : void {
        if (isset($this->exif["DateTimeOriginal"])) {
            $datetime = $this->exif["DateTimeOriginal"];
        } else if (isset($this->exif["DateTimeDigitized"])) {
            $datetime = $this->exif["DateTimeDigitized"];
        } else if (isset($this->exif["DateTime"])) {
            $datetime = $this->exif["DateTime"];
        }

        if (!isset($datetime)) {
            $datetime = date("Y-m-d H:i:s", filemtime($this->file));
        }
        list($date, $time) = explode(' ', $datetime);
        $this->data["date"] = str_replace(':', '-', $date);
        $this->data["time"] = $time;
    }

    /**
     * Get the make and model from the EXIF data
     */
    private function processMakeModel() : void {
        if (isset($this->exif["Make"])) {
            $this->data["camera_make"] = ucwords(strtolower($this->exif["Make"]));
        }

        if (isset($this->exif["Model"])) {
            $this->data["camera_model"] = ucwords(strtolower($this->exif["Model"]));
        }
    }

    /**
     * Determine usage of flash from the EXIF data
     */
    private function processFlash() : void {
        $this->data["flash_used"]="N";
        if (isset($this->exif["Flash"])) {
            $this->data["flash_used"] = match ($this->exif["Flash"]) {
                0, 16       => "N",
                9           => "Y",
                default     => "Y"
            };
        }
    }

    /**
     * Determine focal length and focus distance from the EXIF data
     */
    private function processFocalLength() : void {
        if (isset($this->exif["FocalLength"])) {
            list($a, $b) = explode('/', $this->exif["FocalLength"]);
            if ($b>0) {
                $this->data["focal_length"] = sprintf("%.1fmm", $a / $b);
            }
        }
        if (isset($this->exif["FocusDistance"])) {
            $this->data["focus_dist"] = $this->exif["FocusDistance"];
        }
    }

    /**
     * Determine exposure & used exposure program from the EXIF data
     */
    private function processExposure() : void {
        $this->data["exposure"]="";
        if (isset($this->exif["ExposureTime"])) {
            list($a, $b) = explode('/', $this->exif["ExposureTime"]);
            if ($b>0) {
                $val = $a / $b;
                $this->data["exposure"] = sprintf("%.3f s", $val);
                if ($val !== 0 && $val <= 0.5) {
                    $this->data["exposure"] .= sprintf("  (1/%d)", (int)(0.5 + 1 / $val));
                }
            }
        }

        if (isset($this->exif["ExposureProgram"])) {
            $this->data["exposure"] .= match($this->exif["ExposureProgram"]) {
                2       => " [program (auto)]",
                3       => " [aperture priority (semi-auto)]",
                4       => " [shutter priority (semi-auto)]",
                default => ""
            };
        }
    }

    /**
     * Determine aperture / f-number from the EXIF data
     */
    private function processAperture() : void {
        if (isset($this->exif["FNumber"])) {
            list($a, $b) = explode('/', $this->exif["FNumber"]);
            if ($b>0) {
                $this->data["aperture"] = sprintf("f/%.1f", $a / $b);
            }
        } else if (isset($this->exif["ApertureValue"])) {
            list($a, $b) = explode('/', $this->exif["ApertureValue"]);
            if ($b>0) {
                $this->data["aperture"] = sprintf("f/%.1f", pow(2, ($a / $b)/2));
            }
        } else if (isset($this->exif["MaxApertureValue"])) {
            list($a, $b) = explode('/', $this->exif["MaxApertureValue"]);
            if ($b>0) {
                $this->data["aperture"] = sprintf("f/%.1f", pow(2, ($a / $b)/2));
            }
        }
    }

    /**
     * Determine metering mode from the EXIF data
     */
    private function processMeteringMode() : void {
        if (isset($this->exif["MeteringMode"])) {
            $this->data["metering_mode"] = match ($this->exif["MeteringMode"]) {
                2       => "center weight",
                3       => "spot",
                5       => "matrix",
                default => ""
            };
        }
    }

    /**
     * Determine ISO from the EXIF data
     */
    private function processISO() : void {
        if (isset($this->exif["ISOSpeedRatings"])) {
            $a = $this->exif["ISOSpeedRatings"];
            if ($a < 50) { $a *= 200; }
            $this->data["iso_equiv"] = $a;
        }
    }

    /**
     * Get used compression from the EXIF data
     */
    private function processCompression() : void {
        if (isset($this->exif["CompressedBitsPerPixel"])) {
            list($a, $b) = explode('/', $this->exif["CompressedBitsPerPixel"]);
            if ($b>0) {
                $val = round($a / $b);
                $this->data["compression"] = match ($val) {
                    1       => "jpeg quality: basic",
                    2       => "jpeg quality: normal",
                    4       => "jpeg quality: fine",
                    default => "unknown"
                };
            }
        }
    }

    /**
     * Get comment from the EXIF data
     */
    private function processComment() : void {
        if (isset($this->exif["Comment"])) {
            $this->data["comment"] = $this->exif["Comment"];
        }
    }

    /**
     * Get the GPS Data from the EXIF Data
     * the GPS Data is stored in an array that looks like this
     * array(3) {
     *      [0]=>string(5) "150/1"  (degrees)
     *      [1]=>string(4) "47/1"   (minutes)
     *      [2]=>string(8) "1239/100" (seconds)
     * }
     */
    private function processGPSData() : void {
        $latarray=$this->exif["GPSLatitude"];

        $latdegarray=explode("/", $latarray[0]);
        $latminarray=explode("/", $latarray[1]);
        $latsecarray=explode("/", $latarray[2]);

        $latdeg=$latdegarray[0] / $latdegarray[1];
        $latmin=$latminarray[0] / $latminarray[1];
        $latsec=$latsecarray[0] / $latsecarray[1];

        $lat=$latdeg + ($latmin / 60) + ($latsec / 3600);

        if ($this->exif["GPSLatitudeRef"] == "S") {
            $lat = $lat * -1;
        }
        $this->data["lat"]=$lat;

        $lonarray=$this->exif["GPSLongitude"];

        $londegarray=explode("/", $lonarray[0]);
        $lonminarray=explode("/", $lonarray[1]);
        $lonsecarray=explode("/", $lonarray[2]);

        $londeg=$londegarray[0] / $londegarray[1];
        $lonmin=$lonminarray[0] / $lonminarray[1];
        $lonsec=$lonsecarray[0] / $lonsecarray[1];

        $lon=$londeg + ($lonmin / 60) + ($lonsec / 3600);

        if ($this->exif["GPSLongitudeRef"] == "W") {
            $lon = $lon * -1;
        }
        $this->data["lon"]=$lon;
        /*
        // No alt in db yet
        if (isset($this->exif["GPSAltitude"])) {
            $altarray=explode("/", $this->exif["GPSAltitude"]);
            $alt=$altarray[0] / $altarray[1];
            $this->data["alt"]=$alt;
        }
        */
    }

    /**
     * Some of the 'Undefined' Tags are arrays, here we convert them to a list of values
     */
    private function processUndefined() : array {
        $return = array();
        foreach ($this->exif as $label => $value) {
            if (is_array($value)) {
                $return[$label] = implode(", ", $value);
            } else {
                $return[$label] = $value;
            }
        }
        return $return;
    }

    /**
     * Returns full EXIF information
     */
    public function getFull() : ?block {
        $exif = $this->processUndefined();
        if (!empty($exif)) {
            return new block("exifdetails", array(
                "exif"  => preg_replace("/[^[:print:]]/", "", $exif)
            ));
        }
        return null;
    }

}
?>
