<?php
/**
 * Controller for photo
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo;

use album;
use breadcrumb;
use conf\conf;
use generic\controller as genericController;
use log;
use photo;
use rating;
use selection;
use user;
use web\request;

use photoNotAccessibleSecurityException;

/**
 * Controller for photo
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "delrate", "deselect", "display", "edit",
        "lightbox", "rate", "select", "update", "unlightbox"
    );

    /** @var int total number of photos return by the query */
    public $photocount = 0;
    /** @var int offset in the query for the current photo */
    public $offset = 0;

    /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        try {
            $photo=$this->getPhotoFromRequest();
        } catch (photoNotAccessibleSecurityException $e) {
            log::msg($e->getMessage(), log::WARN, log::SECURITY);
            $photo=null;
        }
        if ($photo instanceof photo) {
            $this->setObject($photo);
            parent::doAction($action);
        } else {
            $this->view = new static::$viewNotfound($this->request);
        }
    }

    /**
     * Get the photo based on the query in the request
     * @throws photoNotAccessibleSecurityException
     */
    private function getPhotoFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["photo_id"])) {
            $photo = new photo($this->request["photo_id"]);
            $photo->lookup();
        } else {
            $offset = $this->request["_off"] ?? 0;
            $photoCollection = collection::createFromRequest($this->request);

            $this->photocount=sizeof($photoCollection);
            $this->offset=$offset;

            $photos=$photoCollection->subset($offset, 1);
            if (sizeof($photos) > 0) {
                $photo = $photos->shift();
                $photo->lookup();
            } else {
                $photo = null;
            }
        }
        if ($user->isAdmin() || $user->getPhotoPermissions($photo)) {
            return $photo;
        }
        throw new photoNotAccessibleSecurityException(
            "Security Exception: photo " . $photo->getId() .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionConfirm();

        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionDelete();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }


    /**
     * Do action 'delrate'
     */
    public function actionDelrate() {
        if (user::getCurrent()->isAdmin()) {
            $ratingId=$this->request["_rating_id"];
            $rating=new rating((int) $ratingId);
            $rating->delete();

            $breadcrumb = breadcrumb::getLast();
            if ($breadcrumb) {
                $link = html_entity_decode($breadcrumb->getURL());
            } else {
                $link = "zoph/welcome";
            }
            $this->view = new static::$viewRedirect($this->request, $this->object);
            $this->view->setRedirect($link);
        }
    }

    /**
     * Do action 'deselect'
     */
    public function actionDeselect() {
        selection::deselect($this->object);
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $this->view->setRedirect($this->request["_return"] . "?" . $this->request->getPassedQueryString());

    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $user = user::getCurrent();
        if (
            $user->prefs->get("auto_edit")  &&
            (!isset($this->request["_action"]) || $this->request["_action"] == "search") &&
            $this->object->isWritableBy($user)) {

            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
        $this->view->setOffset($this->offset, $this->photocount);
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
        $this->view->setOffset($this->offset, $this->photocount);
    }

    /*
     * Do action 'lightbox'
     */
    public function actionLightbox() {
        $this->object->addTo(new album(user::getCurrent()->get("lightbox_id")));
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $qs=$this->request->cleanQueryString(array("/_return=\w+&?/"));
        $this->view->setRedirect($this->request["_return"] . "?" . $qs);
    }

    /**
     * Do action 'unlightbox'
     * Remove from lightbox
     */
    public function actionUnlightbox() {
        $this->object->removeFrom(new album(user::getCurrent()->get("lightbox_id")));
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $qs=$this->request->getPassedQueryString();
        $this->view->setRedirect($this->request["_return"] . "?" . urldecode($qs));
    }

    /**
     * Do action 'rate'
     */
    public function actionRate() {
        $user=user::getCurrent();
        if (conf::get("feature.rating") && ($user->isAdmin() || $user->get("allow_rating"))) {
            $rating = $this->request->getPostVar("rating");
            $this->object->rate($rating);
        }

        $this->view = new static::$viewRedirect($this->request, $this->object);
        $qs=$this->request->getPassedQueryString();
        $this->view->setRedirect("photo?" . urldecode($qs));

    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        $this->view = new static::$viewDisplay($this->request, $this->object);
        if ($this->object->isWritableBy($user)) {
            $deg = (int) $this->request["_deg"];
            if (conf::get("rotate.enable") && $deg != 0) {
                $this->object->lookup();
                try {
                    $this->object->rotate($deg);
                } catch (\Exception $e) {
                    die($e->getMessage());
                }
            }
            if ($this->request["_thumbnail"]) {
                $this->object->thumbnail();
            }

            $this->object->setFields($this->request->getRequestVars());
            // pass again for add people, categories, etc
            $this->object->updateRelations($this->request->getRequestVars(), "_id");
            $this->object->update();

            if (!empty($this->request->getPassedQueryString())) {
                $this->view = new static::$viewRedirect($this->request, $this->object);
                $this->view->setRedirect("photo?" . $this->request->getPassedQueryString());
            }
        }
    }

    /**
     * Do action 'select'
     * Add a photo to the selection, first check if it's not already selected.
     */
    public function actionSelect() {
        selection::select($this->object);
        $this->view = new static::$viewDisplay($this->request, $this->object);
        $this->view->setOffset($this->offset, $this->photocount);
    }
}
