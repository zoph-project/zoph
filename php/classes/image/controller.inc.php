<?php
/**
 * Controller for image
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace image;

use album;
use conf\conf;
use file;
use generic\controller as genericController;
use photo;
use photo\watermarked;
use settings;
use user;
use web\request;
use zoph\router;

use photoNotAccessibleSecurityException;
use photoNotFoundException;

/**
 * Controller for image
 */
class controller extends genericController {
    protected static $viewDisplay       = view\display::class;
    protected static $viewBackground    = \web\view\redirect::class;
    protected static $viewNotFound      = \web\view\notfound::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "background", "display", "mid", "thumb", "importThumb", "importMid"
    );

    protected array $objects = array();

    protected function actionImportMid() : void {
        $this->object = $this->getImportPhotoFromMD5();
        $this->view = new static::$viewDisplay($this->request, $this->object, "mid");
    }

    protected function actionImportThumb() : void {
        $this->object = $this->getImportPhotoFromMD5();
        $this->view = new static::$viewDisplay($this->request, $this->object, "thumb");
    }

    protected function actionDisplay() {
        $hash = $this->request["hash"];

        if (conf::get("share.enable") && !empty($hash)) {
            $this->setPhotoFromHash($hash);
        } else {
            $photoId = $this->request["photo_id"];
            $this->object = new photo($photoId);
            $this->object->lookup();
            $this->watermarkPhoto();
            $this->setView("full");
        }
    }

    protected function actionMid() {
        $photoId = $this->request["photo_id"];
        $this->object = new photo($photoId);
        $this->setView("mid");
    }

    protected function actionThumb() {
        $photoId = $this->request["photo_id"];
        $this->object = new photo($photoId);
        $this->setView("thumb");
    }

    private function setView(string $type) : void {
        if ($this->object->lookup()) {
            $this->view = new static::$viewDisplay($this->request, $this->object, $type);
        } else {
            $this->view = new static::$viewNotFound($this->request);
            $this->view->setMessage(translate("Photo not found"));
        }
    }

    protected function actionBackground() {
        $this->view = new static::$viewRedirect($this->request);
        if (conf::get("interface.logon.background.album")) {
            $album=new album(conf::get("interface.logon.background.album"));
            $this->objects=$album->getPhotos();
            $this->object=$this->objects[array_rand($this->objects)];
            $this->object->lookup();
            $this->view->setRedirect("image?hash=" . $this->object->getHash("full"));
        } else {
            $templates=array(
                conf::get("interface.template"),
                "default"
            );
            foreach ($templates as $template) {
                $bgs=glob(settings::$phpLocation . "/templates/" . $template . "/images/backgrounds/*.[jJ][pP][gG]");
                if (!empty($bgs)) {
                    $image=$bgs[array_rand($bgs)];
                    $this->view->setRedirect("templates/" . $template . "/images/backgrounds/" . basename($image));
                }
            }
        }
    }

    private function watermarkPhoto() : void {
        $user = user::getCurrent();
        if (!$user->isAdmin() && conf::get("watermark.enable")) {
            $permissions = $user->getPhotoPermissions($this->object);
            $watermark = $permissions->get("watermark_level");
            $photoLevel=$this->object->get("level");

            if ($photoLevel > $watermark) {
                $this->object=new watermarked($this->object->getId());
                $this->object->lookup();
            }
        }
    }

    private function getImportPhotoFromMD5() : photo {
        $md5 = $this->request["file"];
        $file = file::getFromMD5(conf::get("path.images") . "/" . conf::get("path.upload"), $md5);

        $this->object = new photo();
        $this->object->set("name", basename($file));
        $this->object->set("path", conf::get("path.upload"));
        return $this->object;
    }

    private function setPhotoFromHash(string $hash) : void {
        try {
            $this->object=photo::getFromHash($hash, "full");
            $this->object->lookup();
            $this->view = new static::$viewDisplay($this->request, $this->object, "full");
        } catch (photoNotFoundException $e) {
            try {
                $this->object=photo::getFromHash($hash, "mid");
                $this->object->lookup();
                $this->view = new static::$viewDisplay($this->request, $this->object, "mid");
            } catch (photoNotFoundException $e) {
                $this->view = new static::$viewNotFound($this->request);
                $this->view->setMessage($e->getMessage());
            }
        }
    }
}
