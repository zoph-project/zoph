<?php
/**
 * View to update circle
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace circle\view;

use web\view\viewInterface;
use conf\conf;
use circle;
use template\actionlink;
use template\block;
use template\form;
use web\request;
use zoph\app;

/**
 * Update screen for circle
 */
class update extends view implements viewInterface {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $param = array("circle_id" => (int) $this->object->getId());

        if ($this->request->getAction() == "new") {
            return array(
                new actionlink("return", "person/people"),
            );
        } else {
            $actionlinks = array(
                new actionlink("delete", "circle/delete", $param),
                new actionlink("new", "circle/new")
            );

            if ($this->object->getId() != 0) {
                $actionlinks[] = new actionlink("return", "circle", $param);
            } else {
                $actionlinks[] = new actionlink("return", "person/people");
            }

            return $actionlinks;
        }
    }


    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $action = $this->request->getAction();

        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $form=new form("form", array(
            "formAction"        => app::getBasePath() . "circle/" . ($action == "new" ? "insert" : "update"),
            "onsubmit"          => null,
            "submit"            => translate("submit")
        ));

        $form->addInputHidden("circle_id", $this->object->getId());

        $form->addInputText("circle_name", $this->object->getName(), translate("Name"),
            sprintf(translate("%s chars max"), 32), 32);

        $form->addTextArea("description", $this->object->get("description"), translate("Description"), 40, 4);

        $members=new block("formFieldsetAddRemove", array(
            "legend"    => translate("members"),
            "objects"   => $this->object->getMembers(),
            "dropdown"  => $this->object->getNewMemberDropdown("_member")
        ));
        $form->addBlock($members);

        $tpl->addBlock($form);

        return $tpl;
    }

    public function getTitle() : string {
        if ($this->request->getAction() == "new") {
            return translate("New circle");
        } else {
            return parent::getTitle();
        }
    }
}
