<?php
/**
 * View to display circle
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace circle\view;

use web\view\viewInterface;
use conf\conf;
use circle;
use photoNoSelectionException;
use selection;
use template\actionlink;
use template\block;
use web\request;
use user;

/**
 * Display screen for circle
 */
class display extends view implements viewInterface {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $actionlinks = array();

        if (user::getCurrent()->canEditOrganisers()) {
            $param = array("circle_id" => (int) $this->object->getId());
            $actionlinks = array(
                new actionlink("edit", "circle/edit", $param),
                new actionlink("delete", "circle/delete", $param),
                new actionlink("new", "circle/new")
            );

            $param["coverphoto"]="NULL";

            if ($this->object->get("coverphoto")) {
                $actionlinks[]= new actionlink("unset coverphoto", "circle/update", $param);
            }
        }
        $actionlinks[]= new actionlink("return", "person/people");
        return $actionlinks;
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $user = user::getCurrent();
        $tpl = new block("display", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => null,
            "obj"               => $this->object,
            "fields"            => $this->object->getDisplayArray(),
            "selection"         => $this->getSelection(),
            "pageTop"           => null,
            "pageBottom"        => null,
            "page"              => null,
            "showMain"          => true
        ));

        if ($user->canSeePeopleDetails()) {
            $tpl->addBlock(new block("definitionlist", array(
                "class" => "display circle",
                "dl"    => $this->object->getDisplayArray()
            )));
        }
        return $tpl;
    }

    private function getSelection() : ?selection {

        $circleId = array(
            "circle_id" => $this->object->getId()
        );

        try {
            $selection=new selection(
                array(
                    new actionlink("coverphoto", "circle/coverphoto", $circleId)
                ),
                "circle?_qs=circle_id=" . $this->object->getId(),
                field: "coverphoto"
            );
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }
        return $selection;
    }
}
