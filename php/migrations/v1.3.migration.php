<?php
/**
 * Database migrations for v1.3
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use db\alter;
use db\column;

if (!defined("MIGRATIONS")) {
    throw new \exception("error");
}

/**
 * Make database changes for v0.1.3
 *
 * @codeCoverageIgnore
 */
class zoph1030 extends migration {
    protected const DESC="Upgrade to v1.3";
    protected const FROM="v01.02.00.00";
    protected const TO="v01.03.00.000";

    public function __construct() {
        $qry = new alter("people");
        $qry->addColumn((new column("display_name"))->varchar(64)->default("NULL"))->after("called");
        $this->addStep(self::UP, $qry, "Add display name column to people table.");
        $qry = new alter("people");
        $qry->addColumn((new column("full_name"))->varchar(64)->default("NULL"))->after("display_name");
        $this->addStep(self::UP, $qry, "Add full name column to people table.");
        $qry = new alter("people");
        $qry->addColumn((new column("last_name_prefix"))->varchar(32)->default("NULL"))->after("first_name");
        $this->addStep(self::UP, $qry, "Add last name prefix column to people table.");
        $qry = new alter("people");
        $qry->addColumn((new column("last_name_suffix"))->varchar(32)->default("NULL"))->after("last_name");
        $this->addStep(self::UP, $qry, "Add last name suffix column to people table.");
        $qry = new alter("people");
        $qry->addColumn((new column("short_name"))->varchar(32)->default("NULL"))->after("full_name");
        $this->addStep(self::UP, $qry, "Add short name column to people table.");
        $qry = new alter("people");
        $qry->dropColumn("sortname", ifExists: true);
        $this->addStep(self::UP, $qry, "Remove unused 'sortname' column from people table.");
    }
}

return new zoph1030();
