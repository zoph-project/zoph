<?php
/**
 * Database migrations for v0.9.24
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use tables\view_albums_details;

if (!defined("MIGRATIONS")) {
    throw new \exception("error");
}

/**
 * Make database changes for v0.9.24
 *
 * @codeCoverageIgnore
 */
class zoph0924 extends migration {
    protected const DESC="Upgrade to v0.9.24";
    protected const FROM="v00.09.23.000";
    protected const TO="v00.09.24.000";

    public function __construct() {
        $view = new view_albums_details();

        $this->addStep(self::UP, $view->getStructure(), "Add view for album details");
    }
}

return new zoph0924();
