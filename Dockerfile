FROM php:8.3-apache
RUN apt update && apt install -y zlib1g-dev libpng-dev libjpeg-dev libzip-dev libxml2-dev mariadb-client && \
	docker-php-ext-configure gd --with-jpeg && \
	docker-php-ext-install pdo_mysql exif gd zip dom && \
	rm -rf /var/lib/apt/lists/*
WORKDIR /var/www
COPY . .
COPY build/entrypoint.sh /
RUN chmod +x /entrypoint.sh && \
  a2enmod rewrite && \
	rmdir html && \
 	mv php html && \
	chown -R www-data:www-data /var/www/html && \
	echo "upload_max_filesize = 16M\npost_max_size = 16M\n" > /usr/local/etc/php/conf.d/zoph.ini && \
	sed "s/usr\/bin\/php/usr\/local\/bin\/php/" /var/www/cli/zoph && \
	ln -s /var/www/cli/zoph /usr/bin
ENTRYPOINT /entrypoint.sh
