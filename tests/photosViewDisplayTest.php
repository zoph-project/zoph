<?php
/**
 * Photos View Display test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photos\view\display;
use photos\params;
use photo\collection;
use PHPUnit\Framework\TestCase;
use web\request;
use conf\conf;

/**
 * Test the photos view display class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photosViewDisplayTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);
        $lbPhotos = $params->getLightBox();

        $display = new display($request, $params);
        $collection = collection::createFromRequest($request);
        $display->setPhotos($collection);
        $display->setDisplay($collection);
        $display->setLightBox($lbPhotos);

        $this->assertInstanceOf(photos\view\display::class, $display);
        return $display;
    }

    /**
     * Test view
     *
     * @depends testCreate
     */
    public function testView(display $display) {
        $template=$display->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("up1.gif", (string) $template);
        $this->assertStringContainsString("<a href=\"/photo?_off=0\" target=\"\">", (string) $template);

        // Check if scripts are included correctly
        conf::set("interface.autocomplete", false);
        conf::set("maps.provider", null);
        conf::set("maps.geocode", null);
        $scripts = $display->getScripts();

        $this->assertContains("js/set.js", $scripts);
        $this->assertContains("js/util.js", $scripts);
        $this->assertNotContains("js/autocomplete.js", $scripts);
        $this->assertNotContains("js/leaflet-src.js", $scripts);
        $this->assertNotContains("js/geocode.js", $scripts);


        conf::set("interface.autocomplete", true);
        conf::set("maps.provider", "osm");
        conf::set("maps.geocode", "geonames");
        $scripts = $display->getScripts();

        $this->assertContains("js/set.js", $scripts);
        $this->assertContains("js/util.js", $scripts);
        $this->assertContains("js/autocomplete.js", $scripts);
        $this->assertContains("js/leaflet-src.js", $scripts);
        $this->assertContains("js/geocode.js", $scripts);
    }

    /**
     * Test create view with photos
     */
    public function testCreateWithPhotos() {
        $request=new request(array(
            "GET"   => array(
                "album_id" => 5,
                "_action"   => "display",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);
        $lbPhotos = $params->getLightBox();

        $collection = collection::createFromRequest($request);


        $display = new display($request, $params);
        $display->setPhotos($collection);
        $display->setDisplay($collection);
        $display->setLightBox($lbPhotos);

        $this->assertInstanceOf(photos\view\display::class, $display);
        return $display;
    }

    /**
     * Test view with photos
     *
     * @depends testCreateWithPhotos
     */
    public function testViewWithPhotos(display $display) {
        $tpl=$display->view();
        $this->assertInstanceOf(template\block::class, $tpl);

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        // Checking for a few strings in the template, to make sure it built correctly:
        $this->assertStringContainsString('<input type="hidden" name="album_id" value="5">', $act);
        $this->assertStringContainsString('photos 1 to 3 of 3', $act);
    }
}
