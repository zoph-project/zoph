<?php
/**
 * Album controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use album\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

use conf\conf;
/**
 * Test the album controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class albumControllerTest extends TestCase {

    protected function setUp() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("album/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test the "display" action
     * a redirect to albums.
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("album/display", get: array(
            "album_id"  => 2
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(album\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Album 1", (string) $output);
        $this->assertStringContainsString("album/edit?album_id=2", (string) $output);
        $this->assertStringContainsString("<a href=\"/photos?album_id=2,3,4\">" .
            "<img alt=\"icon\" src=\"/templates/default/images/icons/folderphoto.png\">" .
            "<span class=\"photocount\">6 photos</span></a>", (string) $output);
        $this->assertStringContainsString("album?album_id=3", (string) $output);
    }

    /**
     * Test the "display" action - unauthorised
     */
    public function testUnauthorisedDisplayAction() {
        user::setCurrent(new user(6));
        $app = new app(mockupRequest::createRequest("album/display", get: array(
            "album_id"  => 1
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(album\view\notfound::class, $view);

        $this->assertEquals("Album not found", $view->getTitle());

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Album not found", (string) $output);
    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("album/delete", get: array(
            "album_id"  => 1
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(album\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete album", (string) $template);
        $this->assertStringContainsString("/album/confirm?album_id=1", (string) $template);
    }

    /**
     * Create album in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("album/insert", post: array(
            "parent_album_id"   => "12",
            "album"             => "In Concert 2021"
        )));
        $app->run();

        $view = $app->view;
        $album=$app->controller->getObject();

        $this->assertInstanceOf(album\view\update::class, $view);
        $this->assertEquals("In Concert 2021", $album->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/album?album_id=" . $album->getId(), (string) $template); // return link
        $this->assertStringContainsString("<input type=\"hidden\" name=\"album_id\" value=\"" . $album->getId() . "\">", (string) $template);
        $this->assertStringContainsString("In Concert 2021", (string) $template);

        return $album;
    }

    /**
     * Update album in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(album $album) {
        $app = new app(mockupRequest::createRequest("album/update", post: array(
            "album_id"      => $album->getId(),
            "album"         => "In Concert 2022",
        )));
        $app->run();
        $view = $app->view;

        $album=$app->controller->getObject();

        $this->assertInstanceOf(album\view\update::class, $view);

        $this->assertEquals("In Concert 2022", $album->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/album?album_id=" . $album->getId(), (string) $template);
        $this->assertStringContainsString("In Concert 2022", (string) $template);
        $this->assertStringNotContainsString("In Concert 2021", (string) $template);

        return $album;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("album/new", get: array(
            "parent_album_id"   => "12",
        )));
        $app->run();
        $view = $app->view;
        $album=$app->controller->getObject();

        $this->assertInstanceOf(album::class, $album);
        $this->assertEquals(0, $album->getId());

        $this->assertInstanceOf(album\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/album?album_id=12", (string) $template);
        $this->assertStringContainsString("new album", (string) $template);
        $this->assertStringContainsString("new album", $view->getTitle());
    }

    /**
     * Test confirm (delete) action
     * @depends testUpdateAction
     */
    public function testConfirmAction(album $album) {
        $id=$album->getId();
        $app = new app(mockupRequest::createRequest("album/confirm", post: array(
            "album_id"      => $id,
        )));
        $app->run();

        $view = $app->view;
        $album=$app->controller->getObject();
        $albums=album::getAll();
        $ids=array();
        foreach ($albums as $album) {
            $ids[]=$album->getId();
        }
        $this->assertNotContains($id, $ids);


        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /album?album_id=12"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());
    }

    /**
     * Test create set coverphoto
     */
    public function testCoverPhotoAction() {
        $album = new album(12);
        $album->lookup();
        $this->assertEquals($album->get("coverphoto"), null);

        unset($album);

        $app = new app(mockupRequest::createRequest("album/coverphoto", post: array(
            "album_id"   => "12",
            "coverphoto" => "1",
        )));
        $app->run();

        $view = $app->view;
        $album=$app->controller->getObject();

        $this->assertInstanceOf(album::class, $album);
        $this->assertEquals($album->get("coverphoto"), 1);

        $this->assertInstanceOf(album\view\display::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<img src=\"/image/thumb?photo_id=1\" class=\"thumb\"", (string) $template);
        return $album;
    }

    /**
     * Test unset coverphoto action
     * @depends testCoverPhotoAction
     */
    public function testUnsetCoverphotoAction(album $album) {
        $id=$album->getId();

        $app = new app(mockupRequest::createRequest("album/unsetcoverphoto", post: array(
            "album_id"   => $id,
        )));
        $app->run();

        $view = $app->view;
        $album=$app->controller->getObject();

        $this->assertInstanceOf(album::class, $album);
        $this->assertEquals($album->get("coverphoto"), null);

        $this->assertInstanceOf(album\view\display::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringNotContainsString("<img src=\"/image/thumb?photo_id=1\" class=\"thumb\"", (string) $template);
    }

    public function getActions() {
        return array(
            array("display", album\view\display::class),
            array("new", album\view\update::class),
            array("edit", album\view\update::class),
            array("delete", album\view\confirm::class),
            array("nonexistant", album\view\display::class)
        );
    }
}
