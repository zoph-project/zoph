<?php
/**
 * Mockup Request class
 * actually not really a mockup as it uses the real object
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

use web\request;

/**
 * Create Fake request object
 */
class mockupRequest {

    public static function createRequest(string $url, ?array $get = array(), ?array $post= array(), ?array $server= array()) : request {

        $query = http_build_query($get);
        $reqUri = $url;
        if (strlen($query) > 0) {
            $reqUri .= "?" . $query;
        }
        $default = array(
            "PHP_SELF"      => "/index.php/" . $url,
            "PATH_INFO"     => "/" . $url,
            "SCRIPT_NAME"   => "/index.php",
            "REQUEST_URI"   => $reqUri,
            "QUERY_STRING"  => $query
        );

        foreach (array("PHP_SELF", "PATH_INFO", "SCRIPT_NAME", "REQUEST_URI", "QUERY_STRING") as $key) {
            $server[$key] = $server[$key] ?? $default[$key];
        }

        $request = new request(array(
            "GET"       => $get,
            "POST"      => $post,
            "SERVER"    => $server
        ));

        return $request;
    }
}

?>
