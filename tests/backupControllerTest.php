<?php
/**
 * Backup controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use backup\controller;
use backup\exception as backupException;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the database backup controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class backupControllerTest extends TestCase {

    /**
     * Test action display
     */
    public function testActionDisplay() {
        $app = new app(mockupRequest::createRequest("backup/display"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf('\backup\view\display', $view);
        $this->assertStringContainsString("start backup", (string) $view->view());
        $this->assertEquals("Create backup", $view->getTitle());
    }

    /**
     * Test action backup
     */
    public function testActionBackup() {
        $app = new app(mockupRequest::createRequest("backup/backup"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf('\backup\view\backup', $view);
        $output = (string) $view->view();
        $uncompressed = zlib_decode($output);
        $this->assertStringContainsString('Table structure for table', $uncompressed);

        $headers = $view->getHeaders();

        $this->assertIsArray($headers);
        $this->assertContains("Content-Type: application/gzip", $headers);
        $this->assertContains("Content-Description: File Transfer", $headers);
    }

    /**
     * Test action backup - with root password set (wrong)
     */
    public function testActionBackupWithRootPasswordError() {
        $app = new app(mockupRequest::createRequest("backup/backup", post: array(
            "rootpwd" => "NotMyRootPassWord"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf('\backup\view\error', $view);
        $this->assertStringContainsString("Access denied for user 'root'", $view->view());
    }
}

