<?php
/**
 * Setup unit test environment
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

use conf\conf;
use zoph\app;

set_include_path(get_include_path() . PATH_SEPARATOR . getcwd() . "/php");
set_include_path(get_include_path() . PATH_SEPARATOR . getcwd() . "/php/classes");

require_once "classes/autoloader.inc.php";
$autoloader = new autoloader();
$autoloader->register();
autoloader::addPath(getcwd() . "/php");

require_once "exception.inc.php";
require_once "config.inc.php";
require_once "functions.inc.php";

app::setMode(app::TEST);
app::loadSettingsTest("zophgit");

define("CLI_API", 9);

require_once "testHelpers.inc.php";
require_once "mockup/mockupRequest.inc.php";
user::setCurrent(new user(1));
conf::set("path.images", getcwd() . "/.images")->update();
?>
