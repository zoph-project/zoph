<?php
/**
 * photo\relation\controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use photo\relation\controller;
use photo\relation\model as photoRelation;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the photo\relation\controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class relationControllerTest extends TestCase {

    public static function tearDownAfterClass() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("relation/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        // The actual relation in the DB is the other way arround id_1 = 2, id_2 = 1
        // so we also test reversing the order
        $app = new app(mockupRequest::createRequest("relation/display", get: array(
            "photo_id_1"  => 1,
            "photo_id_2"  => 2
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(photo\relation\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/relation/edit?photo_id_1=2&photo_id_2=1", (string) $template);
        $this->assertStringContainsString("/image/thumb?photo_id=1", (string) $template);
        $this->assertStringContainsString("/image/thumb?photo_id=2", (string) $template);
    }

    /**
     * Create photo\relation
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("relation/insert", post: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9",
            "desc_1"        => "Original photo",
            "desc_2"        => "Changed the colours"
        )));
        $app->run();

        $view = $app->view;

        $relation=$app->controller->getObject();

        $this->assertInstanceOf(photo\relation\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Original photo", (string) $template);
        $this->assertStringContainsString("Changed the colours", (string) $template);
        $this->assertStringContainsString("/relation/edit?photo_id_1=8&photo_id_2=9", (string) $template);
        $this->assertStringContainsString("/image/thumb?photo_id=8", (string) $template);
        $this->assertStringContainsString("/image/thumb?photo_id=9", (string) $template);

        return $relation;

        return $relation;
    }

    /**
     * Test the "delete" action
     * @depends testInsertAction
     */
    public function testDeleteAction(photoRelation $relation) {
        $app = new app(mockupRequest::createRequest("relation/delete", get: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(photo\relation\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete relationship", (string) $template);
        $this->assertStringContainsString("/relation/confirm?photo_id_1=8&photo_id_2=9", (string) $template);
        return $relation;
    }

    /**
     * Update photo\relation in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(photoRelation $relation) {
        $app = new app(mockupRequest::createRequest("relation/update", post: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9",
            "desc_2"        => "Used GIMP to change colours"
        )));
        $app->run();

        $view = $app->view;

        $relation=$app->controller->getObject();

        $this->assertInstanceOf(photo\relation\view\update::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Used GIMP to change colours", (string) $template);
        $this->assertStringNotContainsString("Changed the colours", (string) $template);
        $this->assertStringContainsString("/relation/edit?photo_id_1=8&photo_id_2=9", (string) $template);
        $this->assertStringContainsString("/image/thumb?photo_id=8", (string) $template);
        $this->assertStringContainsString("/image/thumb?photo_id=9", (string) $template);

        return $relation;
    }

    /**
     * Delete photo relation
     * @depends testUpdateAction
     */
    public function testConfirmAction(photoRelation $relation) {
        $app = new app(mockupRequest::createRequest("relation/confirm", get: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /photo?photo_id=8"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        $this->assertEmpty((new photo(8))->getRelated());
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("relation/new", get: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9"
        )));
        $app->run();

        $view = $app->view;

        $relation=$app->controller->getObject();
        $this->assertInstanceOf(photo\relation\model::class, $relation);

        $this->assertInstanceOf(photo\relation\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<input type=\"hidden\" name=\"photo_id_1\" value=\"8\">", (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"photo_id_2\" value=\"9\">", (string) $template);
    }

    /**
     * Test create relation by unauthorised user
     */
    public function testRelationNotAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();
        user::setCurrent($unauthUser);

        $app = new app(mockupRequest::createRequest("relation/insert", post: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9",
            "desc_1"        => "Some photo",
            "desc_2"        => "Another photo"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }

    /**
     * Test update relation by unauthorised user
     */
    public function testUpdateNotAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();
        user::setCurrent($unauthUser);

        $app = new app(mockupRequest::createRequest("relation/update", post: array(
            "photo_id_1"    => "8",
            "photo_id_2"    => "9",
            "desc_1"        => "Some photo",
            "desc_2"        => "Another photo"
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }

    public function getActions() {
        return array(
            array("new", photo\relation\view\update::class),
            array("edit", photo\relation\view\update::class),
            array("delete", photo\relation\view\confirm::class),
            array("display", photo\relation\view\display::class),
            array("nonexistant", photo\relation\view\display::class)
        );
    }
}
