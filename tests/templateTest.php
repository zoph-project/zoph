<?php
/**
 * Template test
 * Test the working of the template\template class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
require_once "testSetup.php";

use template\template;
use PHPUnit\Framework\TestCase;

/**
 * Test the template class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class termplateTest extends TestCase {
    /**
     * Test create date input
     */
    public function testCreateInputDate() {
        $tpl = template::createInputDate("date", "2021-12-03", "today");

        $this->assertInstanceOf("template\block", $tpl);
        $exp = "<label for=\"date\">today</label><input id=\"date\" type=\"date\" name=\"date\" maxlength=\"32\" size=\"32\" value=\"2021-12-03\">";

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        $this->assertEquals($exp, $act);
    }

    /**
     * Test create time input
     */
    public function testCreateInputTime() {
        $tpl = template::createInputTime("time", "23:59:59", "late");

        $this->assertInstanceOf("template\block", $tpl);
        $exp = "<label for=\"time\">late</label><input id=\"time\" type=\"time\" name=\"time\" maxlength=\"32\" size=\"32\" value=\"23:59:59\">";

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        $this->assertEquals($exp, $act);
    }

    /**
     * Test create e-mail input
     */
    public function testCreateInputEmail() {
        $tpl = template::createInputEmail("mail", "test@zoph.org", "mail address");

        $this->assertInstanceOf("template\block", $tpl);
        $exp = "<label for=\"mail\">mail address</label><input id=\"mail\" type=\"email\" name=\"mail\" maxlength=\"64\" size=\"32\" value=\"test@zoph.org\">";

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        $this->assertEquals($exp, $act);
    }

    /**
     * Test create number input
     */
    public function testCreateInputNumber() {
        $tpl = template::createInputNumber("count", 7, "number of tests", 3, 11);

        $this->assertInstanceOf("template\block", $tpl);
        $exp = "<label for=\"count\">number of tests</label><input id=\"count\" type=\"number\" name=\"count\" maxlength=\"32\" size=\"6\" value=\"7\" min=\"3\" max=\"11\">";

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        $this->assertEquals($exp, $act);
    }

    /**
     * Test create input
     */
    public function testCreateInput() {
        $tpl = template::createInput("name", "Zoph", 10, "Name of program", 15, "the name");

        $this->assertInstanceOf("template\block", $tpl);
        $exp = "<label for=\"name\">Name of program</label><input id=\"name\" type=\"text\" name=\"name\" maxlength=\"10\" size=\"15\" value=\"Zoph\"><span class=\"inputhint\">the name</span>";

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        $this->assertEquals($exp, $act);
    }
}
