<?php
/**
 * Test requirements
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use requirements\check;
use requirements\view\failed;
use web\request;

use PHPUnit\Framework\TestCase;

/**
 * Test migrations
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class requirementsTest extends TestCase {
    public function testCheck() {
        $check = new check();
        $check->doChecks();

        $this->assertEquals(true, $check->getStatus());

        return $check;
    }

    /**
     * @depends testCheck
     */
    public function testShow(check $check) {
        $output = $check->show();

        // Check some strings to verify the template has generated correct output
        $this->assertStringContainsString("<table class=\"requirements\">", (string) $output);
        $this->assertStringContainsString("class=\"reqpass\"", (string) $output);
        $this->assertStringContainsString("Check for PHP FileInfo Extension", (string) $output);
        $this->assertStringContainsString("PHP version supported", (string) $output);
    }

    public function testFailed() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $check = new check();

        $magic = conf::get("path.magic");
        conf::set("path.magic", "/non/existant.file");

        $check->doChecks();

        conf::set("path.magic", $magic);

        $this->assertEquals(false, $check->getStatus());

        $view = new failed($request);
        $view->addRequirements($check);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"alert error\">", (string) $output);
        $this->assertStringContainsString("class=\"reqfail\"", (string) $output);
        $this->assertStringContainsString("The file for path.magic does not exist.", (string) $output);
    }

}

?>
