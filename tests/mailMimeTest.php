<?php
/**
 * Test mail/mime
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;

use conf\conf;
use mail\mime;

/**
 * Test mail/mime
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class mailMimeTest extends TestCase {

    public static function setUpBeforeClass(): void {
    }

    public function testTextMail() {
        $mime = new mime();

        $mime->setTXTBody("Hello from Zoph");
        $mime->setFrom("test@zoph.org");

        $body = $mime->get();

        $expBody = "Hello from Zoph\n";

        $this->assertEquals($expBody, $body);
    }

    public function testHTMLMail() {
        $mime = new mime();

        $mime->setHTMLBody("Hello from <b>Zoph</b>");
        $mime->setFrom("test@zoph.org");

        $body = $mime->get();

        $expBody = "Hello from <b>Zoph</b>\n";

        $this->assertEquals($expBody, $body);
    }

    public function testHTMLandTextMail() {
        $mime = new mime();

        $mime->setHTMLBody("Hello from <b>Zoph</b>");
        $mime->setTXTBody("Hello from Zoph");
        $mime->setFrom("test@zoph.org");

        $body = $mime->get();

        $expBody = "Hello from <b>Zoph</b>\n";
        $this->assertStringContainsString($expBody, $body);

        $expBody = "Hello from Zoph\n";
        $this->assertStringContainsString($expBody, $body);
        
        $this->assertStringContainsString("Content-Type: text/html; charset=\"utf-8\"\n", $body);
    }

    public function testHTMImage() {
        $photo = new photo(1);
        $photo->lookup();

        $dir = conf::get("path.images") . "/" . $photo->get("path") . "/" .  MID_PREFIX . "/";
        $filename = MID_PREFIX . "_" . $photo->get("name");
        $file=new file($dir . DIRECTORY_SEPARATOR . $filename);

        $mime = new mime();
        $mime->setHTMLBody("Photo from <b>Zoph</b>");
        $mime->addHTMLImageFromFile($dir . "/" . $filename, $file->getMime());
        
        $expBody = "Photo from <b>Zoph</b>\n";

        $body = $mime->get();
        $this->assertStringContainsString($expBody, $body);
        $this->assertStringContainsString("Content-Type: text/html; charset=\"utf-8\"\n", $body);
        $this->assertStringContainsString("Content-Type: image/jpeg\n", $body);
        $this->assertStringContainsString("Content-Disposition: inline; filename=\"mid_TEST_0001.JPG\"\n", $body);
    }

    public function testHTMImageAttachment() {
        $photo = new photo(1);
        $photo->lookup();

        $dir = conf::get("path.images") . "/" . $photo->get("path") . "/" .  MID_PREFIX . "/";
        $filename = MID_PREFIX . "_" . $photo->get("name");
        $file=new file($dir . DIRECTORY_SEPARATOR . $filename);

        $mime = new mime();
        $mime->setTXTBody("Photo from Zoph");
        $mime->addAttachmentFromFile($dir . "/" . $filename, $file->getMime());
        
        $expBody = "Photo from Zoph\n";

        $body = $mime->get();
        $this->assertStringContainsString($expBody, $body);
        $this->assertStringContainsString("Content-Type: text/plain; charset=\"utf-8\"\n", $body);
        $this->assertStringContainsString("Content-Type: image/jpeg\n", $body);
        $this->assertStringContainsString("Content-Disposition: attachment; filename=\"mid_TEST_0001.JPG\"\n", $body);
    }

}

?>
