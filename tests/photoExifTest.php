<?php
/**
 * Unittests for photo/exif class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photo\exif;
use template\block;
use PHPUnit\Framework\TestCase;

/**
 * Test photo/exif class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photoExifTest extends TestCase {

    /**
     * Test show full exif
     * @dataProvider getPhotos()
     */

    public function testGetFull(file $photo) {
        $exif = new exif($photo);

        $tpl = $exif->getFull();

        $this->assertInstanceOf(block::class, $tpl);
    }

    public function getPhotos() {
        $files = array();

        $photos = glob("./tests/exif/*.JPG");
        foreach ($photos as $photo) {
            $files[] = array(new file($photo));
        }
        return $files;
    }
}
