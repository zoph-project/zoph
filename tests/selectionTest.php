<?php
/**
 * Selection test
 * Test the working of selecting a photo, to use elsewhere
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

use PHPUnit\Framework\TestCase;

use template\actionlink;
require_once "testSetup.php";

/**
 * Test the selection class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class selectionTest extends TestCase {

    public function testCreate() {
        $session = array(
            "selected_photo"    => array(1)
        );

        $links = array(
            new actionlink("relate", "photo/relate", array("photo_id_2" => 2))
        );

        $return = "photo?_qs=photo_id=2";

        $photo = new photo(2);
        $selection = new selection(
            $links,
            $return,
            $photo,
            field: "photo_id_1",
            session: $session
        );

        $output = helpers::whitespaceClean((string) $selection);
        $this->assertStringContainsString("/image/thumb?photo_id=1", $output);
        $this->assertStringContainsString("<a href=\"/photo/relate?photo_id_2=2&photo_id_1=1&_return=photo%3F_qs%3Dphoto_id%3D2\">relate</a>", $output);
        $this->assertStringContainsString("<a href=\"/photo/deselect?photo_id=1&_return=photo%3F_qs%3Dphoto_id%3D2\">x</a>", $output);
    }

    public function testNoPhotos() {
        $this->expectException(photoNoSelectionException::class);
        $session = array(
            "selected_photo"    => array()
        );

        $links = array(
            new actionlink("relate", "photo/relate", array("photo_id_2" => 2))
        );

        $return = "photo?_qs=photo_id=2";

        $photo = new photo(2);
        $selection = new selection(
            $links,
            $return,
            $photo,
            session: $session
        );
    }
}
