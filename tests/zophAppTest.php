<?php
/**
 * Test Zoph App class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use PHPUnit\Framework\TestCase;
use template\block;
use zoph\app;

/**
 * Test Zoph App class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class zophAppTest extends TestCase {
    public function testMenuSelected() {
        $app = new app(mockupRequest::createRequest(""));
        $app->run();

        $tpl = new block("menu", array(
            "tabs"  => app::getMenu()
        ));

        $output = helpers::whitespaceClean((string) $tpl);

        $this->assertStringContainsString("\"/album\"", $output);
        $this->assertStringContainsString("<li class=\"selected\"><a href=\"/\">home</a></li>", $output);
        $this->assertStringNotContainsString("<li class=\"selected\"><a href=\"/album\">albums</a></li>", $output);
    }

    public function testMenuNoPeopleNoAdmin() {
        user::setCurrent(new user(6));
        $app = new app(mockupRequest::createRequest(""));
        $app->run();

        $tpl = new block("menu", array(
            "tabs"  => app::getMenu()
        ));

        $output = helpers::whitespaceClean((string) $tpl);

        $this->assertStringContainsString("\"/category\"", $output);
        $this->assertStringNotContainsString("people", $output);
        $this->assertStringNotContainsString("admin", $output);
        user::setCurrent(new user(1));
    }

    public function testMenuImportNoImport() {
        conf::set("import.enable", true);
        $app = new app(mockupRequest::createRequest(""));
        $app->run();

        $tpl = new block("menu", array(
            "tabs"  => app::getMenu()
        ));

        $output = helpers::whitespaceClean((string) $tpl);
        $this->assertStringContainsString("import", $output);
        $this->assertStringContainsString("/zoph/info", $output);

        conf::set("import.enable", false);

        $output = helpers::whitespaceClean((string) $tpl);
        $this->assertStringNotContainsString("import", $output);
        $this->assertStringContainsString("/photos", $output);
        $this->assertStringContainsString("/admin", $output);
    }
}
