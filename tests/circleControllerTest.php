<?php
/**
 * Circle controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the circle controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class circleControllerTest extends TestCase {

    protected function setUp() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("circle/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("circle/display", get: array(
            "circle_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(circle\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("circle/edit?circle_id=1", (string) $template);
        $this->assertStringContainsString("Queen", (string) $template);
        $this->assertStringContainsString("John Deacon", (string) $template);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("circle/delete", get: array(
            "circle_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(circle\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete circle", (string) $template);
        $this->assertStringContainsString("Confirm deletion of 'Queen'", (string) $template);
        $this->assertStringContainsString("circle/confirm?circle_id=1", (string) $template);
    }

    /**
     * Create circle in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("circle/insert", post: array(
            "circle_name"   => "Guitarists",
            "description"   => "People who play the 5-string",
        )));
        $app->run();

        $view = $app->view;

        $circle=$app->controller->getObject();

        $this->assertInstanceOf(circle\view\update::class, $view);
        $this->assertEquals("Guitarists", $circle->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("circle?circle_id=" . $circle->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"circle_id\" value=\"" . $circle->getId() . "\">", (string) $template);
        $this->assertStringContainsString("Guitarists", (string) $template);
        $this->assertStringContainsString("People who play the 5-string", (string) $template);

        return $circle;
    }

    /**
     * Update circle in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(circle $circle) {
        $app = new app(mockupRequest::createRequest("circle/update", post: array(
            "circle_id"      => $circle->getId(),
            "description"    => "People who play the 6-string",
            "_member"    =>  2
        )));
        $app->run();

        $view = $app->view;

        $circle=$app->controller->getObject();

        $this->assertInstanceOf(circle\view\update::class, $view);

        $this->assertEquals("People who play the 6-string", $circle->get("description"));
        $this->assertEquals(2, $circle->getMembers()[0]->getId());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("circle?circle_id=" . $circle->getId(), (string) $template);
        $this->assertStringContainsString("Guitarists", (string) $template);
        $this->assertStringNotContainsString("People who play the 5-string", (string) $template);
        $this->assertStringContainsString("Brian May", (string) $template);

        return $circle;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("circle/new"));
        $app->run();

        $view = $app->view;
        $circle=$app->controller->getObject();
        $this->assertInstanceOf(circle::class, $circle);
        $this->assertEquals(0, $circle->getId());

        $this->assertInstanceOf(circle\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("person/people", (string) $template); // return link
        $this->assertStringContainsString("New circle", (string) $template);
        $this->assertStringContainsString("New circle", $view->getTitle());
    }

    /**
     * Update circle, remove member
     * @depends testUpdateAction
     */
    public function testUpdateRemoveMemberAction(circle $circle) {
        $app = new app(mockupRequest::createRequest("circle/update", post: array(
            "circle_id" => $circle->getId(),
            "_remove"   =>  array(2)
        )));
        $app->run();

        $view = $app->view;

        $circle=$app->controller->getObject();

        $this->assertInstanceOf(circle\view\update::class, $view);
        $this->assertEmpty($view->getHeaders());
        $this->assertEquals("Guitarists", $circle->getName());
        $this->assertEquals(0, sizeof($circle->getMembers()));

        return $circle;
    }

    /**
     * Test confirm (delete) acrion
     * @depends testUpdateRemoveMemberAction
     */
    public function testConfirmAction(circle $circle) {
        $id=$circle->getId();
        $app = new app(mockupRequest::createRequest("circle/confirm", get: array(
            "circle_id" => $id,
        )));
        $app->run();

        $view = $app->view;

        $circles=circle::getAll();
        $ids=array();
        foreach ($circles as $circle) {
            $ids[]=$circle->getId();
        }
        $this->assertNotContains($id, $ids);

        $circle=$app->controller->getObject();

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /person/people"), $view->getHeaders());
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $circle;
    }

    public function getActions() {
        return array(
            array("display", circle\view\display::class),
            array("new", circle\view\update::class),
            array("edit", circle\view\update::class),
            array("delete", circle\view\confirm::class),
            array("nonexistant", circle\view\display::class)
        );
    }
}
