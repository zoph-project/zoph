<?php
/**
 * Set controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use set\controller;
use set\model as set;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the set controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class setControllerTest extends TestCase {

    protected function setUp() : void {
        conf::set("feature.sets", true)->update();
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */

    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("set/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Create set in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("set/insert", post: array(
            "name"         => "Test set"
        )));
        $app->run();

        $view = $app->view;
        $set=$app->controller->getObject();

        $this->assertInstanceOf(\set\view\update::class, $view);
        $this->assertEquals("Test set", $set->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("set/delete?set_id=" . $set->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"set_id\" value=\"" . $set->getId() . "\">", (string) $template);
        $this->assertStringContainsString("Test set", (string) $template);

        return $set;
    }

    /**
     * Test the "sets" action - displays all sets
     * @depends testInsertAction
     */
    public function testSetsAction(set $set) {
        $app = new app(mockupRequest::createRequest("set/sets"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(\set\view\sets::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("new", (string) $template);
        $this->assertStringContainsString("set/new", (string) $template);
        $this->assertStringContainsString("Test set", (string) $template);
        $this->assertStringContainsString("set?set_id=" . $set->getId(), (string) $template);
        return $set;
    }

    /**
     * Test the "display" action
     * @depends testSetsAction
     */
    public function testDisplayAction(set $set) {
        $setId = $set->getId();

        $app = new app(mockupRequest::createRequest("set", get: array(
            "set_id"  => $setId
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(\set\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete", (string) $template);
        $this->assertStringContainsString("Test set", (string) $template);
        $this->assertStringContainsString("set/delete?set_id=" . $setId, (string) $template);
        return $set;
    }

    /**
     * Test the "addphoto" action
     * @depends testSetsAction
     */
    public function testAddphotoAction(set $set) {
        $setId = $set->getId();
        $app = new app(mockupRequest::createRequest("set/addphoto", get: array(
            "set_id"    => $setId,
            "_photo_id" => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(\set\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals([1], $photoIds);

        return $set;
    }

    /**
     * Test the "display" action - unauthorised
     * @depends testDisplayAction
     */
    public function testUnauthorisedDisplayAction(set $set) {
        $setId = $set->getId();
        user::setCurrent(new user(6));
        $app = new app(mockupRequest::createRequest("set", get: array(
            "set_id"  => $setId
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(\web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /zoph/welcome"), $view->getHeaders());
    }

    /**
     * Test the "delete" action
     * @depends testUpdateAction
     */
    public function testDeleteAction(set $set) {
        $setId = $set->getId();
        $app = new app(mockupRequest::createRequest("set/delete", get: array(
            "set_id"  => $setId
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(\set\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete set", (string) $template);
        $this->assertStringContainsString("set/confirm?set_id=" . $setId, (string) $template);
    }

    /**
     * Update set in the db
     * @depends testDisplayAction
     */
    public function testUpdateAction(set $set) {
        $setId = $set->getId();
        $app = new app(mockupRequest::createRequest("set/update", post: array(
            "set_id"  => $setId,
            "name"    => "Updated Set",
        )));
        $app->run();

        $view = $app->view;

        $set=$app->controller->getObject();

        $this->assertInstanceOf(\set\view\update::class, $view);

        $this->assertEquals("Updated Set", $set->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/set?set_id=" . $setId, (string) $template);
        $this->assertStringContainsString("Updated Set", (string) $template);
        $this->assertStringNotContainsString("Test set", (string) $template);

        return $set;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("set/new"));
        $app->run();

        $view = $app->view;

        $set=$app->controller->getObject();
        $this->assertInstanceOf(set::class, $set);
        $this->assertEquals(0, $set->getId());

        $this->assertInstanceOf(\set\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("set/sets", (string) $template);
        $this->assertStringContainsString("new set", (string) $template);
        $this->assertStringContainsString("new set", $view->getTitle());
    }


    /**
     * Test create set coverphoto
     * @depends testInsertAction
     */
    public function testCoverPhotoAction(set $set) {
        $set->lookup();
        $setId = $set->getId();
        $this->assertEquals($set->get("coverphoto"), null);
        unset($set);

        $app = new app(mockupRequest::createRequest("set/coverphoto", get: array(
            "set_id"        => $setId,
            "coverphoto"    => "1",
        )));
        $app->run();

        $view = $app->view;

        $set=$app->controller->getObject();
        $this->assertInstanceOf(set::class, $set);
        $this->assertEquals($set->get("coverphoto"), 1);

        $this->assertInstanceOf(\set\view\display::class, $view);

        return $set;
    }

    /**
     * Test unset coverphoto action
     * @depends testCoverPhotoAction
     */
    public function testUnsetCoverphotoAction(set $set) {
        $setId=$set->getId();
        $app = new app(mockupRequest::createRequest("set/unsetcoverphoto", get: array(
            "set_id"        => $setId
        )));
        $app->run();

        $view = $app->view;
        $set=$app->controller->getObject();
        $this->assertInstanceOf(set::class, $set);
        $this->assertEquals($set->get("coverphoto"), null);
        return $set;
    }

    /**
     * Test choose action
     * @depends testUnsetCoverphotoAction
     */
    public function testChooseAction(set $set) {
        $app = new app(mockupRequest::createRequest("set/choose", get: array(
            "album_id"        => 5
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(\set\view\choose::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<input type=\"hidden\" name=\"album_id\" value=\"5\">", (string) $template);
        $this->assertStringContainsString("Add photos to set", $view->getTitle());

        return $set;
    }

    /**
     * Test addphotos action
     * @depends testChooseAction
     */
    public function testAddPhotosAction(set $set) {
        $setId=$set->getId();
        $app = new app(mockupRequest::createRequest("set/addphotos", get: array(
            "_set_id"   => $setId,
            "album_id"  => 5
        )));
        $app->run();

        $view = $app->view;

        $set=$app->controller->getObject();

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        foreach ([3,4,5] as $id) {
            $this->assertContains($id, $photoIds);
        }

        return $set;
    }

    /**
     * Test addphotos action
     * @depends testAddPhotosAction
     */
    public function testRemovePhotoAction(set $set) {
        $setId=$set->getId();
        $app = new app(mockupRequest::createRequest("set/removephoto", get: array(
            "set_id"   => $setId,
            "photo_id"      => 3,
            "_qs"           => "set_id=" . $setId . "&amp;_order=set_order"
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /photos?set_id=" . $setId . "&_order=set_order"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $set;
    }

    /**
     * Test confirm (delete) action
     * @depends testRemovePhotoAction
     */
    public function testConfirmAction(set $set) {
        $setId=$set->getId();
        $app = new app(mockupRequest::createRequest("set/confirm", get: array(
            "set_id"   => $setId,
        )));
        $app->run();

        $sets=set::getAll();
        $ids=array();
        foreach ($sets as $set) {
            $ids[]=$set->getId();
        }
        $this->assertNotContains($setId, $ids);

        $set=$app->controller->getObject();

        $view=$app->view;
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /set/sets"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());
    }

    public function testMoveAction() {
        $set=new set();
        $set->set("name", "testMoveAction");
        $set->insert();

        $photoIds = array(6,7,8);

        foreach ($photoIds as $id) {
            $set->addPhoto(new photo($id));
        }

        $setId=$set->getId();

        $app = new app(mockupRequest::createRequest("set/move", post: array(
            "set_id"   => $setId,
            "_photoId"      => 6,
            "_targetId"     => 8,
        )));
        $app->run();

        $set=$app->controller->getObject();

        $view=$app->view;
        $this->assertInstanceOf(\photos\view\json::class, $view);
        $json = $view->view();
        $this->assertEquals(array(7,6,8), json_decode($json));

        $set->delete();
    }
    public function getActions() {
        return array(
            array("display", \set\view\notfound::class),
            array("new", \set\view\update::class),
            array("sets", \set\view\sets::class),
            array("nonexistant", \set\view\notfound::class)
        );
    }
}
