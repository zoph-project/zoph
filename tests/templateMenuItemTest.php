<?php
/**
 * Test menu items - building block to build a menu
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use PHPUnit\Framework\TestCase;
use template\menu\item as menuItem;

/**
 * Test menu items
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class templateMenuItem extends TestCase {
    /**
     * Test simple menu item
     */
    public function testSimpleMenuItem() {

        $menuItem = new menuItem("home", "zoph/welcome");

        $this->assertTrue($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/zoph/welcome\"", $output);
        $this->assertStringContainsString("home", $output);
    }

    /**
     * Test admin only menu item - negated
     */
    public function testAdminMenuItemAdminNegated() {
        $menuItem = new menuItem("user", "zoph/user", "isAdmin", negate: true);
        $this->assertFalse($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertEmpty($output);
    }

    /**
     * Test admin only menu item - non Admin user - negated
     */
    public function testAdminMenuItemNoAdminNegated() {
        user::setCurrent(new user(3));

        $menuItem = new menuItem("user menu", "zoph/user", "isAdmin", negate: true);
        $this->assertTrue($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/zoph/user\"", $output);
        $this->assertStringContainsString("user menu", $output);
        user::setCurrent(new user(1));
    }

    /**
     * Test admin only menu item
     */
    public function testAdminMenuItemAdmin() {
        $menuItem = new menuItem("admin", "zoph/admin", "isAdmin");
        $this->assertTrue($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/zoph/admin\"", $output);
        $this->assertStringContainsString("admin", $output);
    }

    /**
     * Test admin only menu item - non Admin user
     */
    public function testAdminMenuItemNoAdmin() {
        user::setCurrent(new user(3));

        $menuItem = new menuItem("admin", "zoph/admin", "isAdmin");
        $this->assertFalse($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertEmpty($output);
        user::setCurrent(new user(1));
    }

    /**
     * Test menu item that depends on a config being set
     */
    public function testMenuDependingOnConfig() {
        $conf = conf::get("feature.sets");
        conf::set("feature.sets", true)->update();

        $menuItem = new menuItem("sets", "set", config: "feature.sets");
        $this->assertTrue($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/set\"", $output);
        $this->assertStringContainsString("sets", $output);

        conf::set("feature.sets", $conf)->update();
    }

    /**
     * Test menu item that depends on a config being set
     * test with the configuration disabled
     */
    public function testMenuDependingOnConfigNoDisplay() {
        $conf = conf::get("feature.sets");
        conf::set("feature.sets", false)->update();

        $menuItem = new menuItem("sets", "set", config: "feature.sets");
        $this->assertFalse($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertEmpty($output);
        conf::set("feature.sets", $conf)->update();
    }

    /**
     * Test menu item that depends on a config NOT being set
     * test with the configuration disabled (thus: should display)
     */
    public function testMenuDependingOnNotConfig() {
        $conf = conf::get("feature.sets");
        conf::set("feature.sets", false)->update();

        $menuItem = new menuItem("sets", "set", config: "feature.sets", negate: true);
        $this->assertTrue($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/set\"", $output);
        $this->assertStringContainsString("sets", $output);

        conf::set("feature.sets", $conf)->update();
    }

    /**
     * Test menu item that depends on a config NOT being set
     * test with the configuration enabled (thus: should NOT display)
     */
    public function testMenuDependingOnNotConfigNoDisplay() {
        $conf = conf::get("feature.sets");
        conf::set("feature.sets", true)->update();

        $menuItem = new menuItem("sets", "set", config: "feature.sets", negate: true);
        $this->assertFalse($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertEmpty($output);
        conf::set("feature.sets", $conf)->update();
    }

    /**
     * Test menu item that contains a parameter
     */
    public function testMenuWithParameter() {
        $user = user::getCurrent();
        $lightbox = $user->getLightBox();
        $user->set("lightbox_id", 5);
        $user->update();

        $menuItem = new menuItem("lightbox", "album", "getLightBox", parameter: array("album_id" => "getLightBox"));
        $this->assertTrue($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/album?album_id=5\"", $output);
        $this->assertStringContainsString("lightbox", $output);

        $user->set("lightbox_id", null);
        $user->update();
        $this->assertFalse($menuItem->shouldBeDisplayed());

        $output = (string) $menuItem->display();
        $this->assertEmpty($output);

        $user->set("lightbox_id", $lightbox);
        $user->update();
    }

    /**
     * Test translation of a menu item
     */
    public function testTranslation() {
        $lang=new language("nl");
        $lang->read();
        language::setCurrent($lang);

        $menuItem = new menuItem("places", "place");

        $output = (string) $menuItem->display();
        $this->assertStringContainsString("<a href=\"/place\"", $output);
        $this->assertStringContainsString("plaatsen", $output);
        $lang=new language("en");
        $lang->read();
        language::setCurrent($lang);
    }
}
