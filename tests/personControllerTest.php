<?php
/**
 * Person controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use person\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the person controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class personControllerTest extends TestCase {

    protected function setUp() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("person/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test the "people" action
     */
    public function testPeopleAction() {
        $app = new app(mockupRequest::createRequest("person/people"));
        $app->run();

        $view=$app->view;
        $this->assertInstanceOf(person\view\people::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("people", (string) $output);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"_action\" value=\"people\">", (string) $output);
        $this->assertStringContainsString("/person/people?circle_id=3", (string) $output);
        $this->assertStringContainsString("/person?person_id=3", (string) $output);
        $this->assertStringContainsString("/person/people?_showhidden=1", (string) $output);
        $this->assertStringNotContainsString("/person/people?circle_id=2", (string) $output); // hidden circle
    }

    /**
     * Test the "people" action - not allwed
     */
    public function testPeopleActionUnauthorised() {
        $user=new user(6);
        $origUser = clone $user;

        $user->set("browse_people", 0);
        $user->set("detailed_people", 0);
        $user->update();
        user::setCurrent($user);

        $app = new app(mockupRequest::createRequest("person/people"));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(\web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /zoph/welcome"), $view->getHeaders());

        $origUser->update();
    }

    /**
     * Test the "people" action with a circle selected
     */
    public function testPeopleActionWithCircle() {
        $app = new app(mockupRequest::createRequest("person/people", get: array(
            "circle_id" => 3
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\people::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("bass players", (string) $output);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"circle_id\" value=\"3\">", (string) $output);
        $this->assertStringContainsString("/person?person_id=4", (string) $output);
        $this->assertStringContainsString("/person?person_id=9", (string) $output);
        $this->assertStringContainsString("/person/people?_showhidden=1", (string) $output);
        $this->assertStringNotContainsString("/person/people?circle_id=2", (string) $output); // hidden circle
    }

    /**
     * Test the "people" action with hidden circles visible
     */
    public function testPeopleActionWithHiddenCircles() {
        $app = new app(mockupRequest::createRequest("person/people", get: array(
            "_showhidden"   => 1
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\people::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("people", (string) $output);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"_showhidden\" value=\"1\">", (string) $output);
        $this->assertStringContainsString("/person?person_id=1", (string) $output);
        $this->assertStringContainsString("/person/people?_showhidden=0", (string) $output);
        $this->assertStringContainsString("/person/people?circle_id=2", (string) $output); // hidden circle
    }

    /**
     * Test the "people" action with a first letter
     */
    public function testPeopleActionWithFirstLetter() {
        $app = new app(mockupRequest::createRequest("person/people", get: array(
            "_l"        => "d"
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\people::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("people", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=1", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=3", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=7", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=10", (string) $output);
        $this->assertStringContainsString("<a href=\"/person/people?_l=w\">w</a>", (string) $output);
        $this->assertStringContainsString("<a href=\"/person/people?_l=d\" class=\"selected\">d</a>", (string) $output);
        $this->assertStringContainsString("/person?person_id=9", (string) $output);
    }

    /**
     * Test the "people" action with a first letter - nothing found
     */
    public function testPeopleActionWithFirstLetterNotFound() {
        $app = new app(mockupRequest::createRequest("person/people", get: array(
            "_l"        => "w"
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\people::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("people", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=1", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=3", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=7", (string) $output);
        $this->assertStringNotContainsString("/person?person_id=10", (string) $output);
        $this->assertStringContainsString("<a href=\"/person/people?_l=d\">d</a>", (string) $output);
        $this->assertStringContainsString("<a href=\"/person/people?_l=w\" class=\"selected\">w</a>", (string) $output);
        $this->assertStringContainsString("No people were found with a last name beginning with 'w'.", (string) $output);
    }

    /**
     * Test the "people" action with a circle selected - with coverphoto
     */
    public function testPeopleActionWithCoverPhoto() {
        $circle = new circle(3);
        $circle->lookup();
        $orig = clone $circle;

        $circle->set("coverphoto", 1);
        $circle->update();

        $app = new app(mockupRequest::createRequest("person/people", get: array(
            "circle_id" => 3
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\people::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("/circle/update?circle_id=3&coverphoto=NULL", (string) $output);

        $orig->update();
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("person/display", get: array(
            "person_id"  => 2
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Brian May", (string) $output);
        $this->assertStringContainsString("/person/edit?person_id=2", (string) $output);
        $this->assertStringContainsString("/person/delete?person_id=2", (string) $output);
        $this->assertStringContainsString("/person/new", (string) $output);
        $this->assertStringContainsString("/photos?person_id=2", (string) $output);
        $this->assertStringContainsString("/photos?photographer_id=2", (string) $output);
    }

    /**
     * Test the "display" action - unauthorised
     * A user who can neither browse or see people details will be redirected to the main page
     */
    public function testUnauthorisedDisplayAction() {
        $user=new user(6);
        $origUser = clone $user;

        $user->set("browse_people", 0);
        $user->set("detailed_people", 0);
        $user->update();
        user::setCurrent($user);

        $app = new app(mockupRequest::createRequest("person/display", get: array(
            "person_id"  => 1
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(\web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /zoph/welcome"), $view->getHeaders());

        $origUser->update();
    }

    /**
     * Test the "display" action - unauthorised
     * A user who can not browse people but can see people details will be
     * see a "not found" error when they go to a person they cannot see
     */
    public function testUnauthorisedDisplayActionCanSeeDetails() {
        $user=new user(6);
        $origUser = clone $user;

        $user->set("browse_people", 0);
        $user->set("detailed_people", 1);
        $user->update();
        user::setCurrent($user);

        $app = new app(mockupRequest::createRequest("person/display", get: array(
            "person_id"  => 1
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\notfound::class, $view);

        $this->assertEquals("Person not found", $view->getTitle());

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Person not found", (string) $output);
        $this->assertStringContainsString("/person/people", (string) $output);

        $origUser->update();
    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("person/delete", get: array(
            "person_id"  => 1
        )));
        $app->run();

        $view=$app->view;

        $this->assertInstanceOf(person\view\confirm::class, $view);

        $this->assertEmpty($view->getHeaders());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Confirm deletion of 'Unknown Person'", (string) $template);
        $this->assertStringContainsString("person/confirm?person_id=1", (string) $template);
    }

    /**
     * Create person in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("person/insert", post: array(
            "first_name"        => "Ringo",
            "last_name"         => "Stark",
            "dob"               => "1947-07-07"
        )));
        $app->run();

        $view=$app->view;

        $person=$app->controller->getObject();

        $this->assertInstanceOf(person\view\display::class, $view);

        $this->assertEquals("Ringo Stark", $person->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("person/edit?person_id=" . $person->getId(), (string) $template);

        return $person;
    }

    /**
     * Update person in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(person $person) {
        $app = new app(mockupRequest::createRequest("person/update", post: array(
            "person_id" => $person->getId(),
            "last_name" => "Starr",
        )));
        $app->run();

        $view=$app->view;

        $person=$app->controller->getObject();

        $this->assertInstanceOf(person\view\display::class, $view);

        $this->assertEquals("Ringo Starr", $person->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/person/edit?person_id=" . $person->getId(), (string) $template);
        $this->assertStringContainsString("Ringo Starr", (string) $template);
        $this->assertStringNotContainsString("Ringo Stark", (string) $template);

        return $person;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("person/new"));
        $app->run();

        $view=$app->view;


        $person=$app->controller->getObject();
        $this->assertInstanceOf(person::class, $person);
        $this->assertEquals(0, $person->getId());

        $this->assertInstanceOf(person\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("person/people", (string) $template);
        $this->assertStringContainsString("new person", (string) $template);
        $this->assertStringContainsString("new person", $view->getTitle());
    }

    /**
     * Test confirm (delete) action
     * @depends testUpdateAction
     */
    public function testConfirmAction(person $person) {
        $id=$person->getId();
        $app = new app(mockupRequest::createRequest("person/confirm", get: array(
            "person_id"      => $id,
        )));
        $app->run();

        $view=$app->view;

        $people=person::getAll();
        $ids=array();
        foreach ($people as $person) {
            $ids[]=$person->getId();
        }
        $this->assertNotContains($id, $ids);

        $person=$app->controller->getObject();

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /person/people"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $person;
    }
   /**
     * Test set coverphoto
     */
    public function testCoverPhotoAction() {
        $person = new person(9);
        $person->lookup();
        $this->assertEquals($person->get("coverphoto"), null);

        unset($person);

        $app = new app(mockupRequest::createRequest("person/coverphoto", get: array(
            "person_id"   => "9",
            "coverphoto" => "1",
        )));
        $app->run();

        $view=$app->view;

        $person=$app->controller->getObject();
        $this->assertInstanceOf(person::class, $person);
        $this->assertEquals($person->get("coverphoto"), 1);

        $this->assertInstanceOf(person\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("John Deacon", (string) $output);
        $this->assertStringContainsString("<img src=\"/image/thumb?photo_id=1\" class=\"thumb\" width=\"120\" height=\"80\"", (string) $output);
        return $person;
    }

    /**
     * Test unset coverphoto action
     * @depends testCoverPhotoAction
     */
    public function testUnsetCoverphotoAction(person $person) {
        $id=$person->getId();
        $app = new app(mockupRequest::createRequest("person/unsetcoverphoto", get: array(
            "person_id"      => $id,
        )));
        $app->run();

        $view=$app->view;

        $person=$app->controller->getObject();
        $this->assertInstanceOf(person::class, $person);
        $this->assertEquals($person->get("coverphoto"), null);

        $this->assertInstanceOf(person\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("John Deacon", (string) $output);
        $this->assertStringNotContainsString("<img src=\"image/thumb?photo_id=1\" class=\"thumb\" width=\"120\" height=\"80\"", (string) $output);
    }

    public function getActions() {
        return array(
            array("display", person\view\display::class),
            array("people", person\view\people::class),
            array("new", person\view\update::class),
            array("edit", person\view\update::class),
            array("delete", person\view\confirm::class),
            array("nonexistant", person\view\display::class)
        );
    }
}
