<?php
/**
 * Place controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use place\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the place controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class placeControllerTest extends TestCase {

    protected function setUp() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("place/" . $action));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf($expView, $view);
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("place/display", get: array(
            "place_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(place\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Places", (string) $output);
        $this->assertStringContainsString("/place/edit?place_id=1", (string) $output);
        $this->assertStringContainsString("<a href=\"/photos?location_id=1,18,2,6,7,3,5,4,8,9,10,11,14,15,12,13,16,17\">" .
            "<img alt=\"icon\" src=\"/templates/default/images/icons/folderphoto.png\">" .
            "<span class=\"photocount\">12 photos</span></a>", (string) $output);
        $this->assertStringContainsString("/place?place_id=18", (string) $output);
    }

    /**
     * Test the "display" action - unauthorised
     */
    public function testUnauthorisedDisplayAction() {
        user::setCurrent(new user(6));
        $app = new app(mockupRequest::createRequest("place/display", get: array(
            "place_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(place\view\notfound::class, $view);
    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("place/delete", get: array(
            "place_id"  => 1
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(place\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete place", (string) $template);
        $this->assertStringContainsString("place/confirm?place_id=1", (string) $template);
    }

    /**
     * Create place in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("place/insert", post: array(
            "parent_place_id"   => "18",
            "title"         => "New Souht Wales",
            "country"       => "Australia"),
        ));
        $app->run();

        $view = $app->view;
        $place=$app->controller->getObject();

        $this->assertInstanceOf(place\view\update::class, $view);
        $this->assertEquals("New Souht Wales", $place->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("place?place_id=" . $place->getId() , (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"place_id\" value=\"" . $place->getId() . "\">", (string) $template);
        $this->assertStringContainsString("New Souht Wales", (string) $template);
        $this->assertStringContainsString("Australia", (string) $template);

        return $place;
    }

    /**
     * Update place in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(place $place) {
        $app = new app(mockupRequest::createRequest("place/update", post: array(
            "place_id"      => $place->getId(),
            "title"         => "New South Wales",
        )));
        $app->run();

        $view = $app->view;

        $place=$app->controller->getObject();

        $this->assertInstanceOf(place\view\update::class, $view);

        $this->assertEquals("New South Wales", $place->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("place?place_id=" . $place->getId(), (string) $template);
        $this->assertStringContainsString("New South Wales", (string) $template);
        $this->assertStringNotContainsString("New Souht Wales", (string) $template);

        return $place;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("place/new", get: array(
            "parent_place_id"   => "18",
        )));
        $app->run();

        $view = $app->view;

        $place=$app->controller->getObject();
        $this->assertInstanceOf(place::class, $place);
        $this->assertEquals(0, $place->getId());

        $this->assertInstanceOf(place\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("place?place_id=18", (string) $template);
        $this->assertStringContainsString("new place", (string) $template);
        $this->assertStringContainsString("new place", $view->getTitle());
    }

    /**
     * Test confirm (delete) action
     * @depends testUpdateAction
     */
    public function testConfirmAction(place $place) {
        $id=$place->getId();
        $app = new app(mockupRequest::createRequest("place/confirm", get: array(
            "place_id"   => $id,
        )));
        $app->run();

        $view = $app->view;

        $places=place::getAll();
        $ids=array();
        foreach ($places as $place) {
            $ids[]=$place->getId();
        }
        $this->assertNotContains($id, $ids);

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /place?place_id=18"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());
    }

    /**
     * Test create set coverphoto
     */
    public function testCoverPhotoAction() {
        $place = new place(18);
        $place->lookup();
        $this->assertEquals($place->get("coverphoto"), null);

        unset($place);

        $app = new app(mockupRequest::createRequest("place/coverphoto", get: array(
            "place_id"   => "18",
            "coverphoto" => "1",
            "_return"    => "place?place_id=18"
        )));
        $app->run();

        $view = $app->view;
        $place=$app->controller->getObject();

        $this->assertInstanceOf(place::class, $place);
        $this->assertEquals($place->get("coverphoto"), 1);

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /place?place_id=18"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        $template = $view->view();
        return $place;
    }

    /**
     * Test unset coverphoto action
     * @depends testCoverPhotoAction
     */
    public function testUnsetCoverphotoAction(place $place) {
        $id=$place->getId();
        $app = new app(mockupRequest::createRequest("place/unsetcoverphoto", get: array(
            "place_id"   => $id
        )));
        $app->run();

        $view = $app->view;
        $place=$app->controller->getObject();

        $this->assertInstanceOf(place::class, $place);
        $this->assertEquals($place->get("coverphoto"), null);

        $template = $view->view();

        $this->assertInstanceOf(place\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Australia", (string) $output);
        $this->assertStringContainsString("place/edit?place_id=" . $id, (string) $output);
    }

    /**
     * Test set timezone for children
     */
    public function testSetTZforChildrenAction() {
        $place = new place(6);
        $place->lookup();
        $place->set("timezone", "Europe/Berlin");
        $place->update();

        $app = new app(mockupRequest::createRequest("place/settzchildren", get: array(
            "place_id"   => 6
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(place\view\update::class, $view);

        foreach ([6,7] as $id) {
            $place = new place($id);
            $place->lookup();
            $this->assertEquals($place->get("timezone"), "Europe/Berlin");
            $place->set("timezone", null);
            $place->update();
        }
    }

    public function getActions() {
        return array(
            array("new", place\view\update::class),
            array("edit", place\view\update::class),
            array("delete", place\view\confirm::class),
            array("display", place\view\display::class),
            array("nonexistant", place\view\display::class)
        );
    }
}
