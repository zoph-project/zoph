<?php
/**
 * Photos View Display test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photo\view\display;
use PHPUnit\Framework\TestCase;
use web\request;
use conf\conf;

/**
 * Test the photo view display class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photoViewDisplayTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=mockupRequest::createRequest("photo/display", get: array(
            "album_id"  => 2,
            "_off"      => 1
        ));

        $photo = new photo(7);
        $photo->lookup();

        $display = new display($request, $photo);
        $this->assertInstanceOf(photo\view\display::class, $display);
        return $display;
    }

    /**
     * Test view
     *
     * @depends testCreate
     */
    public function testView(display $display) {
        $template=$display->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/photo?album_id=2&amp;_off=0", (string) $template);
        $this->assertStringContainsString("/photos?album_id=2&_off=0", (string) $template);
        $this->assertStringContainsString("/image?photo_id=7", (string) $template);

        // Check if scripts are included correctly
        $scripts = $display->getScripts();

        $this->assertContains("js/photoPeople.js", $scripts);
        $this->assertContains("js/rating.js", $scripts);
    }

}
