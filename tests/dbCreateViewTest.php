<?php
/**
 * Test creating a view via the db\view class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use db\view;
use db\select;

use db\exception as dbException;

use PHPUnit\Framework\TestCase;

/**
 * Test class that tests CREATE VIEW queries
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class dbCreateViewTest extends TestCase {

    /**
     * Test creating a view as a simple SELECT query
     */
    public function testCreateQuery() {
        $view = new view("test_view");

        $view->setQuery(new select("test_table"));

        $sql = (string) $view;

        $this->assertEquals("CREATE VIEW zoph_test_view AS\n" .
            "SELECT * FROM zoph_test_table;", $sql);
    }

    /**
     * Test IF NOT EXISTS
     */
    public function testCreateQueryIfNotExists() {
        $view = new view("test_view");
        $view->ifNotExists();

        $view->setQuery(new select("test_table"));

        $sql = (string) $view;

        $this->assertEquals("CREATE VIEW IF NOT EXISTS zoph_test_view AS\n" .
            "SELECT * FROM zoph_test_table;", $sql);
    }
}
