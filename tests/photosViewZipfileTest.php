<?php
/**
 * Photos View Prepare test (download feature)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use photos\view\zipfile;
use photos\params;
use photo\collection;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the photos view zipfile class (download feature)
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photosViewZipfileTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $zipfile = new zipfile($request, $params);
        $this->assertInstanceOf('\photos\view\zipfile', $zipfile);
        return $zipfile;
    }

    /**
     * Test view
     *
     * @depends testCreate
     * this will throw an exception, because the zipfile is not created
     */
    public function testView(zipfile $zipfile) {
        $this->expectException(fileNotFoundException::class);
        $tpl=$zipfile->view();
    }

    /**
     * Test create view with photos
     */
    public function testCreateWithZipfile() {
        $file = fopen("/tmp/zoph_1_test_1.zip", "w");
        fwrite($file, "Ceci n'est pas une ZIP");
        fclose($file);


        $request=new request(array(
            "GET"   => array(
                "_action"   => "zipfile",
                "_filename" => "test",
                "_filenum"  => "1"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $collection = collection::createFromRequest($request);

        $zipfile = new zipfile($request, $params);
        $this->assertInstanceOf('\photos\view\zipfile', $zipfile);

        $headers = $zipfile->getHeaders();

        ob_start();
            $zipfile->view();
        $output=ob_get_contents();
        ob_end_clean();

        $this->assertIsArray($headers);
        $this->assertContains("Content-Description: File Transfer", $headers);
        $this->assertContains("Content-Disposition: attachment; filename=\"test_1.zip\"", $headers);
        $this->assertContains("Content-Length: 22", $headers);

        $this->assertStringContainsString("Ceci n'est pas une ZIP", $output);
        // make sure file is gone
        $this->assertFileDoesNotExist("/tmp/zoph_1_test_1.zip");
    }
}
