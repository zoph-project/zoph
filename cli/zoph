#!/usr/bin/php
<?php
/**
 * Executable for CLI functions of Zoph
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

/**
 * Prepares the environment before switching to the CLI functions inside the webroot
 */

 /**
  * Change this if your zoph.ini is in another place. Just never, EVER put it inside your webroot,
  * or the entire Internet will know your database passwords
  */
define("INI_FILE", "/etc/zoph.ini");

define("CLI", true);
define("CLI_API", 9);


define("EXIT_NO_PROBLEM", 0);
define("EXIT_NO_ARGUMENTS", 1);
define("EXIT_NO_FILES", 2);

define("EXIT_IMAGE_NOT_FOUND", 10);
define("EXIT_PERSON_NOT_FOUND", 20);
define("EXIT_PLACE_NOT_FOUND", 30);
define("EXIT_ALBUM_NOT_FOUND", 40);
define("EXIT_CATEGORY_NOT_FOUND", 50);
define("EXIT_SET_NOT_FOUND", 60);
define("EXIT_NOT_IN_CWD", 61);
define("EXIT_ILLEGAL_DIRPATTERN", 62);
define("EXIT_NO_PARENT", 80);

define("EXIT_INI_NOT_FOUND", 90);
define("EXIT_INSTANCE_NOT_FOUND", 91);

define("EXIT_CLI_USER_NOT_ADMIN", 95);
define("EXIT_CLI_USER_NOT_VALID", 96);

define("EXIT_API_NOT_COMPATIBLE", 99);

define("EXIT_CLI_CONFIG_ERROR", 100);

define("EXIT_CANNOT_ACCESS_ARGUMENTS", 250);
define("EXIT_OTHER_ERROR", 253);
define("EXIT_UNKNOWN_ERROR", 254);

use set\notFoundException as setNotFoundException;
init();

/**
 * Initialize and handover to scripts inside webroot
 */
function init() {
    global $argv;

    $instance=getInstance();
    try {
        $ini=loadINI($instance);
    } catch (zophINIInstanceNotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit(EXIT_INSTANCE_NOT_FOUND);
    } catch (zophININotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_INI_NOT_FOUND);
    } catch (zophINIMissingSettingException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_CLI_CONFIG_ERROR);
    }

    require_once("classes/autoloader.inc.php");
    $autoloader=new autoloader();
    $autoloader->register();
    require_once("classes/settings.inc.php");
    settings::parseINI($ini);
    require_once("include.inc.php");

    
    try {
        $auth = new auth\cli();
        $user = $auth->getUser();
    } catch (cliUserNotValidException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_CLI_USER_NOT_VALID);
    }

    try {
        $cli=new cli\cli($user, CLI_API, $argv);
        $cli->run();
    } catch (cliAPINotCompatibleException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_API_NOT_COMPATIBLE);
    } catch (cliUserNotAdminException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_CLI_USER_NOT_ADMIN);
    } catch (cliNoFilesException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_NO_FILES);
    } catch (cliUnknownErrorException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_UNKNOWN_ERROR);
    } catch (cliNotInCWDException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_PATH_NOT_IN_CWD);
    } catch (cliNotInCWDException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_PATH_NOT_IN_CWD);
    } catch (albumNotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_ALBUM_NOT_FOUND);
    } catch (categoryNotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_CATEGORY_NOT_FOUND);
    } catch (personNotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_PERSON_NOT_FOUND);
    } catch (placeNotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_PLACE_NOT_FOUND);
    } catch (setNotFoundException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_SET_NOT_FOUND);
    } catch (cliNoParentException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_NO_PARENT);
    } catch (cliIllegalDirpatternException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_ILLEGAL_DIRPATTERN);
    } catch (configurationException $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_CLI_CONFIG_ERROR);
    } catch (Exception $e) {
        echo $e->getMessage() . "\n";
        exit (EXIT_OTHER_ERROR);
    }


}

/**
 * See if the user specified an instance on the CLI or just pick the first one
 */
function getInstance() {
    global $argv;

    if ($argv) {
        $size=sizeof($argv);
        for ($i=0; $i<$size; $i++) {
            if ($argv[$i] == "-i" || $argv[$i] == "--instance") {
                return($argv[++$i]);
            }
        }
    }
    return null;
}

/**
 * Load the right instance from the INI file
 * @param string Name of the Zoph instance to be used
 */
function loadINI($instance) {
    if (!defined("INI_FILE")) {
        define("INI_FILE", "/etc/zoph.ini");
    }
    if (file_exists(INI_FILE)) {
        $ini=parse_ini_file(INI_FILE, true);
        if (!empty($instance)) {
            if (isset($ini[$instance])) {
                $ini=$ini[$instance];
            } else {
                throw new zophINIInstanceNotFoundException("Instance " . $instance . " not found in " . INI_FILE);
            }
        } else {
            // No instance given, taking the first
            $ini=array_shift($ini);
        }
    } else {
        throw new zophININotFoundException(INI_FILE . " not found.");
    }
    set_include_path(get_include_path() . PATH_SEPARATOR . $ini["php_location"]);
    return $ini;
}
?>
